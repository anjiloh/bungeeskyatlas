/**
 * 
 */
package net.anc.atlas.guice;

import java.util.List;

import net.anc.atlas.Content;
import net.anc.atlas.Page;

/**
 * Mock concrete Page, it actually does nothing
 * 
 * @author Angelo Nicolini
 *
 */
public class GiuicePage implements Page {

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {
		// TODO Auto-generated method stub

	}

}
