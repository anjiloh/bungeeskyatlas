/**
 * 
 */
package net.anc.atlas;

import jparsec.io.HTMLReport.SIZE;
import jparsec.util.JPARSECException;

/**
 * @author Angelo Nicolini
 *
 */
public interface AtlasGenerator {

	/**
	 * ID constant for a black text color.
	 */
	public static final String COLOR_BLACK = "Black";
	/**
	 * ID constant for a white text color.
	 */
	public static final String COLOR_WHITE = "White";
	/**
	 * ID constant for a red text color.
	 */
	public static final String COLOR_RED = "Red";
	/**
	 * ID constant for a green text color.
	 */
	public static final String COLOR_GREEN = "Green";
	/**
	 * ID constant for a blue text color.
	 */
	public static final String COLOR_BLUE = "Blue";
	/**
	 * ID constant for a yellow text color.
	 */
	public static final String COLOR_YELLOW = "Yellow";
	
	/**
	 * Writes the header of the Latex file for a presentation.
	 * Not compatible with HTML.
	 * @param theme The theme name, or null to use Warsaw.
	 * @param title Title to be seen in the document.
	 * @param author Document author.
	 * @param institute The institute.
	 * @param date Date.
	 * @param packages A set of package names to be imported using usepackage Latex command.
	 * Can be null to import no additional one. Default imported packages are graphicx, txfonts,
	 * verbatim, hyperref, spanish {babel}, longtable, usenames {color}, natbib.
	 * @param hideNavigationControls True to hide navigation controls.
	 * @param showTitlePage True to include the first frame showing a default presentation title.
	 * @param forceSmallNavigationBar True to force the navigation bar to show just one section and
	 * subsection (the current one).
	 * @param leftMargin Left margin as a value in cm for the text location. Null for default value.
	 * @param rightMargin Right margin as a value in cm for the text location. Null for default value.
	 */
	void writeHeader(String theme, String title, String author, String institute, String date,
			String packages[], boolean hideNavigationControls, boolean showTitlePage, boolean forceSmallNavigationBar,
			String leftMargin, String rightMargin);

	String getCode();

	void setCode(String code);

	void endBody();

	void endDocument();

	/**
	 * Begins a frame for a presentation.
	 * @param plain True to begin a plain frame without decorations.
	 * @param addTitlePage True to add \\titlepage to the frame.
	 */
	void beginFrame(boolean plain, boolean addTitlePage);

	void endFrame();
	
	/**
	 * Returns the code for an image tag. None of the parameters can be null.
	 * @param width Width. As a number for pixels or as percentage.
	 * @param height Height. As a number for pixels or as percentage.
	 * @param align Align.
	 * @param border Border. Ignored
	 * @param alt Alt (tooltip text). Ignored.
	 * @param src Link to the image.
	 * @return Latex code.
	 */
	String writeImage(String width, String height, String align, String border,
			String alt, String src);

	void writeRawText(String text);

	void setTextSize(SIZE verySmall);

	String getAvoidFormatting();

	void beginSection(String string);

	void setAvoidFormatting(String string);

	void beginSubSection(String title);

	/**
	 * Writes the code for an image tag. Not compatible with HTML.
	 * @param width Width.
	 * @param height Height.
	 * @param align Align.
	 * @param src Link to the image.
	 * @param caption Caption for the image. Can be null.
	 * @param label Label as an id for the image. Can be null.
	 */
	void writeImageWithCaption(String width, String height, String align,
			String src, String caption, String label) ;

	void writeNavigator(int i, int j, int k, int l, int m);

	void beginColumns();

	void beginColumn(String string);

	void endColumn();

	void endColumns();

	void writeTableHeader(String header);

	void writeHeaderRowInTable(String[] columns, String bgcolor, String align, String colspan);

	void writeRowInTable(String[] columns, String bgcolor, String align, String colspan);

	void writeHorizontalLine();

	void endTable();

	void beginFrame(String title);
	
	void writeSeparator();

	void writeLongTableHeader(String caption, String columnAligment);
	void endLongTable(String label);
	
	/**
	 * write a paragraph
	 * @param text
	 */
	void writeParagraph(String text);
	
	/**
	 * Creates a link.
	 * @param href Link destination.
	 * @param highlighted Text to be highlighted.
	 * @return Code for the link.
	 */
	String writeLink(String href, String highlighted);
	
	/**
	 * Writes a list of items.
	 * @param lines Lines to be formatted as items.
	 * @throws JPARSECException If the text style is invalid.
	 */
	void writeList(String[] lines) throws JPARSECException;

	void forceNewPage();

	void writeListUsingNumbers(String[] lines);
	
}
