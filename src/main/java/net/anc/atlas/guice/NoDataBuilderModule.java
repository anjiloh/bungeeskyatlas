/**
 * 
 */
package net.anc.atlas.guice;

import static org.mockito.Mockito.mock;

import net.anc.atlas.AtlasProperties;
import net.anc.atlas.BungeeSkyAtlas;
import net.anc.atlas.MultiVolumeNoDataBungeeSkyAtlas;
import net.anc.atlas.NoDataBungeeSkyAtlas;
import net.anc.atlas.Recover;
import net.anc.atlas.Startup;

/**
 * Guice Builder for a generation of the index part only
 * 
 * @author Angelo Nicolini
 *
 */
public class NoDataBuilderModule extends AbstractAtlasBuilderModule {

	@Override
	protected void configure() {
		
		super.configure();
		bind(Startup.class).toInstance(mock(Recover.class));

		
		if( AtlasProperties.getProperties().isMultiVolume() ) {
			bind(BungeeSkyAtlas.class).to(MultiVolumeNoDataBungeeSkyAtlas.class);
		}
		else {
			bind(BungeeSkyAtlas.class).to(NoDataBungeeSkyAtlas.class);
		}
		
	}
}
