/**
 * 
 */
package net.anc.atlas;

import java.util.logging.Logger;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import jparsec.observer.LocationElement;

/**
 * @author Angelo Nicolini
 *
 */
public class AstroItemDetail implements Content {

	private static final Logger LOGGER = Logger.getLogger( MySkyAtlas.class.getName() );

	
	private String name;
	private String otherNames;
	
	private LocationElement location;
	private float magnitude;
	private String angle;
	private String type;
	private String constellation;
	private int chartId;
	private AstroItemType itemType;
	private String extraInfo;
	
	public enum AstroItemType {
		DSO,
		STAR,
		NOVA,
		SUPERNOVA
		
		
	} ;

	/**
	 * Construct  an AstroItemDetail from a CSV line.
	 * Format: name, longitude, latitude, magnitude, angle, type,  constellation, chartId
	 * @param line
	 */
	public AstroItemDetail(String line) {
		
		String datai[]  = line.split(",", -1);
		
		setName(datai[0]);
		setLocation(new LocationElement(Double.parseDouble(datai[1]), Double.parseDouble(datai[2]), Double.parseDouble(datai[3])));
		setMagnitude(Float.parseFloat(datai[4]));
		setAngle(datai[5]);
		setType(datai[6]);
		setConstellation(datai[7]);
		this.itemType = AstroItemType.valueOf(datai[8]);
		try {
			setChartId(Integer.parseInt(datai[9]));
		}
		catch( NumberFormatException e) {
			LOGGER.warning("problem while reading source " + e.getMessage() + ",  recovering");
		}
		setExtraInfo("");
		
	}

	/**
	 * 
	 * @param locationElement
	 */
	public final void setLocation(LocationElement locationElement) {
		this.location = locationElement;
		
	}

	/**
	 * 	// 188 CALDWELL
	 * // 1,0.21575668454170227,1.4897487163543701,8.1,4,0.125x0.125,Cepheus,0
	*
	 * @param name
	 * @param location
	 * @param magnitude
	 * @param angle
	 * @param type
	 * @param constellation
	 * @param chartId
	 */
	public AstroItemDetail(String name, LocationElement location, float magnitude, String angle, String type,
			String constellation, AstroItemType itemType, int chartId) {
		super();
		this.name = name;
		this.location = location;
		this.magnitude = magnitude;
		this.angle = angle;
		this.setType(type);
		this.constellation = constellation;
		this.chartId = chartId;
		this.itemType = itemType;
		this.extraInfo = "";
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public final void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return location.getLongitude();
	}

//	/**
//	 * @param longitude
//	 *            the longitude to set
//	 */
//	public void setLongitude(double longitude) {
//		this.longitude = longitude;
//	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return location.getLatitude();
	}

//	/**
//	 * @param dec
//	 *            the dec to set
//	 */
//	public void setLatitude(double latitude) {
//		this.latitude = latitude;
//	}

	/**
	 * @return the magnitude
	 */
	public float getMagnitude() {
		return magnitude;
	}

	/**
	 * @param magnitude
	 *            the magnitude to set
	 */
	public final void setMagnitude(float magnitude) {
		this.magnitude = magnitude;
	}

	/**
	 * @return the angle
	 */
	public String getAngle() {
		return angle;
	}

	/**
	 * @param angle
	 *            the angle to set
	 */
	public final void setAngle(String angle) {
		this.angle = angle;
	}

	/**
	 * @return the constellation
	 */
	public String getConstellation() {
		return constellation;
	}

	/**
	 * @param constellation
	 *            the constellation to set
	 */
	public final void setConstellation(String constellation) {
		this.constellation = constellation;
	}

	/**
	 * @return the chartId
	 */
	public int getChartId() {
		return chartId;
	}

	/**
	 * @param chartId
	 *            the chartId to set
	 */
	public final void setChartId(int chartId) {
		this.chartId = chartId;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public final void setType(String type) {
		this.type = type;
	}

	public String asCSVString() {
		

		StringBuilder csvString = new StringBuilder();
		csvString.append(getName()).append(',');
		csvString.append(getLongitude()).append(',').append(getLatitude()).append(',').append(getLocation().getRadius()).append(',');
		csvString.append(getMagnitude()).append(',');
		csvString.append(getAngle()).append(',');
		csvString.append(getType()).append(',');
		csvString.append(getConstellation()).append(',');
		csvString.append(itemType).append(',');
		csvString.append(getChartId());
		
		return csvString.toString();
//		return ReflectionToStringBuilder.toString(this, ToStringStyle.SIMPLE_STYLE);
	}

	/**
	 * this return 8 (chartId was excluded)
	 */
	@Override
	public int size() {
		return 8;
	}
	
	/**
	 * 
	 * @return
	 */
	public LocationElement getLocation() {
		return location;
	}
	

	/**
	 * utility to trasform string to {@link AstroItemDetail} via Lambda map
	 * @param line
	 * @return
	 */
	public static AstroItemDetail valueOf(String line) {
		return new AstroItemDetail(line);
	}

	
	public AstroItemType getItemType() {
		return itemType;
	}

	/**
	 * @return the extraInfo
	 */
	public String getExtraInfo() {
		return extraInfo;
	}

	/**
	 * @param extraInfo the extraInfo to set
	 */
	public final void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}
	
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SIMPLE_STYLE);

	}

	/**
	 * This value cannot be serialized into a CSV file!!!
	 * 
	 * @return the otherNames
	 */
	public String getOtherNames() {
		return otherNames;
	}

	/**
	 * This value cannot be serialized into a CSV file!!!
	 * 
	 * @param otherNames the otherNames to set
	 */
	public void setOtherNames(String otherNames) {
		this.otherNames = otherNames;
	}
}
