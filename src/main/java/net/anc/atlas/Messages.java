package net.anc.atlas;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;

import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;

public class Messages {
	
	private static final String BUNDLE_NAME = "messages"; //$NON-NLS-1$

	private static final String START_LINK = "<link>";
	private static final String END_LINK = "</link>";
	private static final int START_LINK_LEN = 6;
	private static final int END_LINK_LEN = 7;
	
	private static volatile ResourceBundle RESOURCE_BUNDLE = null;
	private static volatile Map<String, String> OBJECT_DESCRIPTIONS = null;
	
	private Messages() {
	}

	public static String getString(String key) {
		try {
			
			String msg = getResourceBundle().getString(key);
			
			if( msg.indexOf("<link>") != -1 ) {
				
				String value = msg;
				StringBuilder retValue = new StringBuilder();
				do {
					int start = value.indexOf(START_LINK);
					int end = value.indexOf(END_LINK);
					if( start == -1 ){
						retValue.append(value);
						break;
					}
					else{
						retValue.append(value.substring(0, start - 1));
						retValue.append( " \\href{"+value.substring(start + START_LINK_LEN, end -1 )+"}{"+value.substring(start + START_LINK_LEN, end )+"}");
						value = value.substring(end + END_LINK_LEN);
					}
						
				}
				while( true );

				msg = retValue.toString();
			}

			return msg;
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

	private static ResourceBundle getResourceBundle() {
		if( RESOURCE_BUNDLE == null ){
			if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH) {
				RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, Locale.ENGLISH);
			}
			else {
				// it_IT is defined by Locale.ITALY, not Locale.ITALIAN !!!
				RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, Locale.ITALY);

			}
		}
		return RESOURCE_BUNDLE;
	}
	
	private static Map<String, String> getObjectDescriptions(){
		if( OBJECT_DESCRIPTIONS == null ) {
			OBJECT_DESCRIPTIONS = new HashMap<String, String>();
			try (Stream<String> indexEntries = Files.lines(Paths.get(AtlasProperties.getProperties().getClass().getClassLoader().getResource("commonnames_it.properties").toURI()))) {
				indexEntries.forEach( line -> {
					String[] values = line.split("=");
					OBJECT_DESCRIPTIONS.put(values[0].toLowerCase(), values[1]);
				});
			} catch (IOException  | URISyntaxException e) {
				throw new RuntimeException("cannot get message", e);			}
		}
		return OBJECT_DESCRIPTIONS;
	}
	
	public static String getObjectDescriptionTransation(String extraInfo) {
		if( Translate.getDefaultLanguage() == Translate.LANGUAGE.ENGLISH ) {
			return extraInfo;
		}
		else {
			if( StringUtils.isNotBlank(extraInfo)) {
				String[] parts = extraInfo.split(",");
				StringBuilder retValue = new StringBuilder();
				for (String part : parts) {
					String translated = getObjectDescriptions().get(part.trim().toLowerCase());
					if(translated == null ) {
						retValue.append(", ").append(part);
					}
					else {
						retValue.append(", ").append(translated);
					}
				}
				if( retValue.toString().startsWith(", ")) {
					retValue.delete(0, 2);	
				}
				return retValue.toString();
			}
			return "";
 		}
	}
	
}
