package net.anc.atlas.index;

import jparsec.graph.chartRendering.RenderSky;
import jparsec.graph.chartRendering.SkyRenderElement;
import net.anc.atlas.AstroItemDetail;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public interface IndexConverter extends IndexComposer {
	
	String STAR = "star";
	String OBJECT = "dso";
	
	/**
	 * This converts the JPARSEC object to a BungeeSkyAtlas one.
	 * The output of a concrete object implementing this must be serialized in a CSV file 
	 * @param data the data to be converted
	 * @param renderSky the {@link SkyRenderElement} JPARSEC item owning this data
	 * @return
	 */
	AstroItemDetail convert(Object data, RenderSky renderSky);

}
