/**
 * 
 */
package net.anc.atlas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author nicolinia
 *
 */
public abstract class EmisphereMinimap  {
	
	/*
	protected static final Map<Zoom, double[]> strechFactorNWT = Collections.unmodifiableMap(new HashMap<Zoom, double[]>() 
	{ 
		private static final long serialVersionUID = 4924918631910248509L;

	{
		put(Zoom.VIEW_OF_25_DEG_NWT, new double[]{1.2, 1.4, 1.5, 2.2, 2.3});
	}
	{
		put(Zoom.VIEW_OF_18_DEG_NWT, new double[] {1.4, 1.4, 1.8, 1.9, 2.1, 2.1, 2.1});
	}
	{
		put(Zoom.VIEW_OF_15_DEG_NWT, new double[]{1.4, 1.4, 1.5, 1.6, 1.6, 2.0, 2.1});
	}
	});

	protected static final Map<Zoom, double[]> strechFactorDEF = Collections.unmodifiableMap(new HashMap<Zoom, double[]>() 
	{ 
		private static final long serialVersionUID = 4924918631910248509L;

	{
		put(Zoom.VIEW_OF_25_DEG_DEF, new double[]{1.2, 1.4, 1.5, 2.2, 2.3}); //{2.3, 2.3, 2.3, 2.9, 3.0});		// A4??
	}
	{
		put(Zoom.VIEW_OF_18_DEG_DEF, new double[] {1.4, 1.4, 1.8, 1.9, 2.1, 2.1, 2.1});
	}
	{
		put(Zoom.VIEW_OF_15_DEG_DEF, new double[]{1.4, 1.4, 1.5, 1.6, 1.6, 2.0, 2.1});
	}
	});
*/
	
	protected static final Map<PaperSize , Map<Zoom, double[]>> strechFactor = Collections.unmodifiableMap(new HashMap<PaperSize, Map<Zoom, double[]>>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(new A3(), new HashMap<Zoom, double[]>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				{
					put(Zoom.VIEW_OF_25_DEG_DEF, new double[]{2.3, 2.3, 2.3, 2.9, 2.9});		
				}
				{
					put(Zoom.VIEW_OF_25_DEG_NWT, new double[]{2.3, 2.3, 2.3, 2.9, 2.9});
				}

				
				{
					put(Zoom.VIEW_OF_18_DEG_DEF, new double[] {1.4, 1.4, 1.8, 1.9, 2.1, 2.1, 2.1});
				}
				{
					put(Zoom.VIEW_OF_15_DEG_DEF, new double[]{1.4, 1.4, 1.5, 1.6, 1.6, 2.0, 2.1});
				}
				{
					put(Zoom.VIEW_OF_18_DEG_NWT, new double[] {1.4, 1.4, 1.8, 1.9, 2.1, 2.1, 2.1});
				}
				{
					put(Zoom.VIEW_OF_15_DEG_NWT, new double[]{1.4, 1.4, 1.5, 1.6, 1.6, 2.0, 2.1});
				}
			});

		}

		{
			put(new A4(), new HashMap<Zoom, double[]>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				{
					put(Zoom.VIEW_OF_25_DEG_DEF, new double[]{1.2, 1.4, 1.5, 2.2, 2.3});	
				}
				{
					put(Zoom.VIEW_OF_25_DEG_NWT, new double[]{1.2, 1.4, 1.5, 2.2, 2.3});
				}
				
				{
					put(Zoom.VIEW_OF_18_DEG_DEF, new double[] {1.0, 1.0, 1.2, 1.3, 1.5, 1.5, 1.6});
				}
				{
					put(Zoom.VIEW_OF_18_DEG_NWT, new double[] {1.0, 1.0, 1.2, 1.3, 1.5, 1.5, 1.6});
				}

				{
					put(Zoom.VIEW_OF_15_DEG_DEF, new double[]{0.9, 1.0, 1.0, 1.3, 1.2, 1.6, 1.9});
				}
				
				{
					put(Zoom.VIEW_OF_15_DEG_NWT, new double[]{0.9, 1.0, 1.0, 1.3, 1.2, 1.6, 1.9});
				}
			});

		}



	});

	
	
	
	protected List<Content> coordinates = new ArrayList<Content>();
	
	public final List<Content> getCoordinates() {
		return coordinates;
	}


	public static class Entry implements Content {
		public double ro;
		public double theta;
		public int chartId;
		
		@Override
		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	
	public abstract void compile();
	
}
