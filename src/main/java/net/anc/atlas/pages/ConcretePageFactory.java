/**
 * 
 */
package net.anc.atlas.pages;

import java.util.Map;

import com.google.inject.Inject;

import net.anc.atlas.Page;

/**
 * @author Angelo Nicolini
 *
 */
public class ConcretePageFactory implements PageFactory<Page> {

	private Map<String, PageFactory<?>> factories;

	@Inject
	public ConcretePageFactory(Map<String, PageFactory<?>> factories) {
		this.factories = factories;
	}
	
	@Override
	public Page create(String name) {
		return factories.get(name).create("name");
	}

}

