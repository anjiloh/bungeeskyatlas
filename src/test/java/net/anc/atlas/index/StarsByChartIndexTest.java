package net.anc.atlas.index;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import jparsec.observer.LocationElement;
import jparsec.util.Translate;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AstroItemDetail.AstroItemType;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.TestModule;
import net.anc.atlas.pages.PagesModule;

public class StarsByChartIndexTest {

	protected Injector injector = Guice.createInjector(
			new TestModule(),
			new PagesModule(),
			new AbstractModule() {
		        @Override
		        protected void configure() {
		        }
			}
			
			);

	@Inject
	protected AtlasGenerator generator;

	
	@Before
	public void setUp() throws Exception {
		injector.injectMembers(this);
	}

	@Test
	public void testWriteContents()  {
		List<Content> contents = new ArrayList<Content>();
		AstroItemDetail detail = new AstroItemDetail(
				"Cursa", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				"Taurus",
				AstroItemType.STAR,
				-1
				);
		detail.setExtraInfo(Translate.translate("Variable Type") + ":Pulsating");
				
				
		contents.add( detail );
		
		StarsByChartIndex subject = injector.getInstance(StarsByChartIndex.class);

		subject.writeContents(contents, 0, 1);
		
		String code = generator.getCode();
		assertTrue(code.contains(Translate.translate("Pulsating")));
	}

	@Test
	public void testWriteContentsComplexVariableType()  {
		List<Content> contents = new ArrayList<Content>();
		AstroItemDetail detail = new AstroItemDetail(
				"Cursa", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				"Taurus",
				AstroItemType.STAR,
				-1
				);
		detail.setExtraInfo(Translate.translate("Variable Type") + ":Pulsating (Tau Alp)");
				
				
		contents.add( detail );
		
		StarsByChartIndex subject = injector.getInstance(StarsByChartIndex.class);
		
		
		subject.writeContents(contents, 0, 1);
		
		String code = generator.getCode();
		assertTrue(code.contains(Translate.translate("Pulsating")));
		assertTrue(code.contains(Translate.translate("Pulsating") + "\\\\(Tau Alp)"));
		
	}

	@Test
	public void testWriteContentsWithExtraName()  {
		List<Content> contents = new ArrayList<Content>();
		AstroItemDetail detail = new AstroItemDetail(
				"Cursa", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				"Taurus",
				AstroItemType.STAR,
				-1
				);
		detail.setExtraInfo(Translate.translate("Variable Type") + ":Pulsating (Tau Alp)");
				
		detail.setOtherNames("alp nam");
				
		contents.add( detail );
		
		StarsByChartIndex subject = injector.getInstance(StarsByChartIndex.class);
		
		subject.writeContents(contents, 0, 1);
		
		String code = generator.getCode();
		assertTrue(code.contains("alp nam"));
	}

	public void testWriteContentsWithExtraNames()  {
		List<Content> contents = new ArrayList<Content>();
		AstroItemDetail detail = new AstroItemDetail(
				"Cursa", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				"Taurus",
				AstroItemType.STAR,
				-1
				);
		detail.setExtraInfo(Translate.translate("Variable Type") + ":Pulsating (Tau Alp)");
				
		detail.setOtherNames("alp nam;second name");
				
		contents.add( detail );
		
		StarsByChartIndex subject = injector.getInstance(StarsByChartIndex.class);
		
		subject.writeContents(contents, 0, 1);
		
		String code = generator.getCode();
		assertTrue(code.contains("\\makecell[l]{alp nam\\second name}"));
	}

	
//	@Test 
//	public void testGetColumnsAlign() {
//		fail("Not yet implemented"); // TODO
//	}
//
//	@Test
//	public void testGetColumnsName() {
//		fail("Not yet implemented"); // TODO
//	}
//
//	@Test
//	public void testGetId() {
//		fail("Not yet implemented"); // TODO
//	}

}
