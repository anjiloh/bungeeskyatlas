package net.anc.atlas.index;

import org.junit.Before;
import static org.mockito.Mockito.mock;

import org.junit.Test;

import jparsec.graph.chartRendering.RenderSky;
import jparsec.graph.chartRendering.SkyRenderElement;
import net.anc.atlas.AstroItemDetail;

public class StarTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testConvert() {
		Star subject = new Star();
		
		RenderSky renderer = mock(RenderSky.class);
		renderer.render = new SkyRenderElement();
		
		AstroItemDetail result = subject.convert("Sirius", renderer);
		System.out.println(result);
		//6450045
//		System.out.println(StarElement.getVariableType(1));
		
//		result = subject.convert("RR Lyrae");
//		System.out.println(result);

//		result = subject.convert("Delta Scuti");
//		System.out.println(result);
		
		
//		Almak, 2030134 (Gam1 And) (3-1)
//		result = subject.convert("Almak");
//		System.out.println(result);
//		System.out.println(StarElement.getVariableType(1));
		
		
		// Toliman 14400004
		result = subject.convert("Toliman", renderer);
		System.out.println(result);

	}

}
