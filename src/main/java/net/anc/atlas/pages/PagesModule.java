/**
 * 
 */
package net.anc.atlas.pages;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;

import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Page;

/**
 * @author Angelo Nicolini
 *
 */
public class PagesModule extends AbstractModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {

		MapBinder<String, PageFactory<?>> mapbinder = MapBinder.newMapBinder(binder(), new TypeLiteral<String>() {
		}, new TypeLiteral<PageFactory<?>>() {
		});

		install(new FactoryModuleBuilder().build(HeaderPageFactory.class));
		mapbinder.addBinding("header").to(HeaderPageFactory.class);
		install(new FactoryModuleBuilder().build(CreditsPageFactory.class));
		mapbinder.addBinding("credits").to(CreditsPageFactory.class);
		install(new FactoryModuleBuilder().build(PresentationPageFactory.class));
		mapbinder.addBinding("presentation").to(PresentationPageFactory.class);
		install(new FactoryModuleBuilder().build(GenerationOptionsPageFactory.class));
		mapbinder.addBinding("generationOptions").to(GenerationOptionsPageFactory.class);
		install(new FactoryModuleBuilder().build(LegendPageFactory.class));
		mapbinder.addBinding("legend").to(LegendPageFactory.class);
		install(new FactoryModuleBuilder().build(LicensePageFactory.class));
		mapbinder.addBinding("license").to(LicensePageFactory.class);
		install(new FactoryModuleBuilder().build(HistoryPageFactory.class));
		mapbinder.addBinding("history").to(HistoryPageFactory.class);
		install(new FactoryModuleBuilder().build(ChartPageFactory.class));
		mapbinder.addBinding("chartPage").to(ChartPageFactory.class);
		install(new FactoryModuleBuilder().build(MinimapsChartsPageFactory.class));
		mapbinder.addBinding("minimaps").to(MinimapsChartsPageFactory.class);
		
		install(new FactoryModuleBuilder().build(DSOsByChartPageFactory.class));
		mapbinder.addBinding("DSOsByChart").to(DSOsByChartPageFactory.class);
		install(new FactoryModuleBuilder().build(StarsByChartPageFactory.class));
		mapbinder.addBinding("starsByChart").to(StarsByChartPageFactory.class);

		install(new FactoryModuleBuilder().build(NovaeByChartPageFactory.class));
		install(new FactoryModuleBuilder().build(MultiVolumeNovaeByChartPageFactory.class));
		install(new FactoryModuleBuilder().build(ConstellationsByChartPageFactory.class));
		install(new FactoryModuleBuilder().build(MultiVolumeConstellationsByChartPageFactory.class));

		if (AtlasProperties.getProperties().isMultiVolume()) {
			mapbinder.addBinding("novaeByChart").to(MultiVolumeNovaeByChartPageFactory.class);
			mapbinder.addBinding("constellationsByChart").to(MultiVolumeConstellationsByChartPageFactory.class);
		} else {
			mapbinder.addBinding("novaeByChart").to(NovaeByChartPageFactory.class);
			mapbinder.addBinding("constellationsByChart").to(ConstellationsByChartPageFactory.class);
		}

		bind(new TypeLiteral<PageFactory<Page>>() {
		}).to(ConcretePageFactory.class);

	}
}
