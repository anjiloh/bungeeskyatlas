package com.guice.test;

import com.google.inject.assistedinject.Assisted;

public interface MercedesFactory extends CarFactory<Mercedes> {
	
	Mercedes create(@Assisted("part") String part);

}
