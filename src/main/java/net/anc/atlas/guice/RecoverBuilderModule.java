/**
 * 
 */
package net.anc.atlas.guice;

import net.anc.atlas.AtlasProperties;
import net.anc.atlas.BungeeSkyAtlas;
import net.anc.atlas.CompleteBungeeSkyAtlas;
import net.anc.atlas.MultiVolumeCompleteBungeeSkyAtlas;
import net.anc.atlas.Recover;
import net.anc.atlas.Startup;

/**
 * Guice Recover Builder to restart an atlas build after a crash
 * 
 * @author Angelo Nicolini
 *
 */
public class RecoverBuilderModule extends AbstractAtlasBuilderModule {
	@Override
	protected void configure() {
		
		super.configure();
		bind(Startup.class).to(Recover.class);
		if( AtlasProperties.getProperties().isMultiVolume() ) {
			bind(BungeeSkyAtlas.class).to(MultiVolumeCompleteBungeeSkyAtlas.class);
		}
		else {
			bind(BungeeSkyAtlas.class).to(CompleteBungeeSkyAtlas.class);
		}

	}
}
