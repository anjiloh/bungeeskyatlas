/**
 * 
 */
package net.anc.atlas.serializer;

import java.awt.Dimension;

import jparsec.graph.chartRendering.frame.SkyRendering;

/**
 * @author nicolinia
 *
 */
public interface ExportChart {

	/**
	 * Initialize the chart export process
	 * @param marginX
	 * @param marginY
	 */
	void initialize(int marginX, int marginY);
	
	/**
	 * 
	 * @param skyRender
	 * @param size
	 * @param plotFile
	 * @param image
	 */
	void export(SkyRendering skyRender, Dimension size, String plotFile);

}
