/*
 * This file is part of JPARSEC library.
 *
 * (C) Copyright 2006-2017 by T. Alonso Albi - OAN (Spain).
 *
 * Project Info:  http://conga.oan.es/~alonso/jparsec/jparsec.html
 *
 * JPARSEC library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * JPARSEC library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package net.anc.atlas.serializer;

import java.io.Serializable;
import java.text.DateFormat;

import com.google.inject.Singleton;

import jparsec.ephem.Functions;
import jparsec.time.TimeFormat;
import jparsec.util.JPARSECException;
import jparsec.util.Translate;
import jparsec.util.Version;
import net.anc.atlas.A3;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;

/**
 * A class to generate reports for Latex.<P>
 *
 * The basic process is to write the header, to start the body, to end the body, and
 * to end the document. Any text to write should be located inside the body. Anyway,
 * the header/body and document end are handled or called automatically in case the
 * user don't call them, so it is possible to concentrate just in the content. <P>
 *
 * Latex report has been designed to be compatible with html reports. If you create
 * an html report, you can also report to latex by just changing the object instance.
 * Some methods are not completely compatible, though.
 *
 * This is directly derived from the {@link jparsec.io.LATEXReport} from T. Alonso Albi
 *
 * @author Angelo Nicolini
 * @author T. Alonso Albi - OAN (Spain)
 * @version 1.0
 */
@Singleton
public class LaTeXBeamerGenerator extends  AbstractLaTeXGenerator implements Serializable, AtlasGenerator
{
	private static final long serialVersionUID = 1L;

	private String msg = Translate.translate(257)+" "+Translate.translate(161)+" "+Version.PACKAGE_NAME+" v"+Version.VERSION_ID+
	" "+Translate.translate(259) + " ";

	private boolean firstJustify = true;


	/**
	 * Empty constructor.
	 */
	public LaTeXBeamerGenerator() {
		super();
	}
	/**
	 * Not so empty constructor...
	 * @param fixedFigures True to add adequate code to the figures to fix
	 * their positions in the document.
	 */
	public LaTeXBeamerGenerator(boolean fixedFigures) {
//		fixFigures = true; 
		super(fixedFigures);
	}


	/**
	 * Writes the header of the Latex file.
	 * @param title Title to be seen in the document.
	 */
	public void writeHeader(String title)
	{
		latexCode.append("% "+msg + TimeFormat.dateNow(DateFormat.FULL) + sep);
		latexCode.append("\\documentclass{article}" + sep);
		latexCode.append("\\usepackage[T1]{fontenc}" + sep);

		latexCode.append("\\usepackage{graphicx}" + sep);
		latexCode.append("\\usepackage{ulem}" + sep);
		//latexCode.append("\\usepackage{txfonts}" + sep);
		latexCode.append("\\usepackage[table]{xcolor}" + sep);
		latexCode.append("\\usepackage{verbatim}" + sep);
		latexCode.append("\\usepackage{hyperref}" + sep);
//		if (Translate.getDefaultLanguage() == LANGUAGE.SPANISH)
//			latexCode.append("\\usepackage[spanish]{babel}" + sep);
//		if (Translate.getDefaultLanguage() == LANGUAGE.ITALIAN)
//			latexCode.append("\\usepackage[italian]{babel}" + sep);
		latexCode.append("\\usepackage[" + Translate.getDefaultLanguage().toString().toLowerCase() +"]{babel}" + sep);
		
		latexCode.append("\\usepackage{longtable}" + sep);
		latexCode.append("\\usepackage{makecell}" + sep);
		latexCode.append("\\usepackage{ragged2e}" + sep);
		latexCode.append("\\usepackage{setspace}" + sep);
		latexCode.append("\\usepackage{pdfpages}" + sep);
		latexCode.append("\\usepackage{bookmark}" + sep);
		latexCode.append("\\usepackage[space]{grffile}" + sep);
		//latexCode.append("\\usepackage{blindtext}" + sep);
		latexCode.append("\\usepackage[usenames]{color}" + sep);
		latexCode.append("\\usepackage{natbib}" + sep);
		latexCode.append(" \\bibpunct{(}{)}{;}{a}{}{,} % to follow the A&A style" + sep);
		latexCode.append(sep);
		latexCode.append("\\pdfpagewidth 8.5in" + sep);
		latexCode.append("\\pdfpageheight 11in " + sep);
		latexCode.append("\\setlength\\topmargin{0in}" + sep);
		latexCode.append("\\setlength\\headheight{0in}" + sep);
		latexCode.append("\\setlength\\headsep{0in}" + sep);
		latexCode.append("\\setlength\\textheight{8.7in}" + sep);
		latexCode.append("\\setlength\\textwidth{6.5in}" + sep);
		latexCode.append("\\setlength\\oddsidemargin{0in}" + sep);
		latexCode.append("\\setlength\\evensidemargin{0in}" + sep);
		latexCode.append("\\setlength\\parindent{0.25in}" + sep);
		latexCode.append("\\setlength\\parskip{0.25in}" + sep);
		latexCode.append(sep);
		latexCode.append("\\begin{document}" + sep);

		if( AtlasProperties.getProperties().isWhite()) {
			latexCode.append("\\rowcolors{2}{gray!25}{white}" + sep);
		}
		else {
			latexCode.append("\\rowcolors{2}{gray!85}{black}" + sep);
		}
		latexCode.append("\\setcounter{secnumdepth}{4}" + sep); 		// titles with up to 4 levels

		header = true;
		if (title != null) {
			latexCode.append("\\title{");
			this.writeTextWithStyleAndColor(title, false);
			latexCode.append("}" + sep);
			latexCode.append("\\maketitle" + sep);
		}
	}

	/**
	 * Writes the header of the Latex file.
	 * @param title Title to be seen in the document.
	 * @param textWidthPercentage Percentage of the page width ocupped by text, from 0 to 100.
	 * @param textHeightPercentage Percentage of the page height ocupped by text, from 0 to 100.
	 */
	public void writeHeader(String title, int textWidthPercentage, int textHeightPercentage)
	{
		double w = (textWidthPercentage / 100.0) * 8.5;
		double h = (textHeightPercentage / 100.0) * 11;

		latexCode.append("% "+msg + TimeFormat.dateNow(DateFormat.FULL) + sep);
		latexCode.append("\\documentclass{article}" + sep);
		latexCode.append("\\usepackage[T1]{fontenc}" + sep);

		latexCode.append("\\usepackage{graphicx}" + sep);
		latexCode.append("\\usepackage{ulem}" + sep);
		//latexCode.append("\\usepackage{txfonts}" + sep);
		latexCode.append("\\usepackage[table]{xcolor}" + sep);
		latexCode.append("\\usepackage{verbatim}" + sep);
		latexCode.append("\\usepackage{hyperref}" + sep);
		latexCode.append("\\usepackage[" + Translate.getDefaultLanguage().toString().toLowerCase() +"]{babel}" + sep);

//		if (Translate.getDefaultLanguage() == LANGUAGE.SPANISH)
//			latexCode.append("\\usepackage[spanish]{babel}" + sep);
//		if (Translate.getDefaultLanguage() == LANGUAGE.ITALIAN)
//			latexCode.append("\\usepackage[italian]{babel}" + sep);

		latexCode.append("\\usepackage{longtable}" + sep);
		latexCode.append("\\usepackage{makecell}" + sep);
		latexCode.append("\\usepackage{ragged2e}" + sep);
		latexCode.append("\\usepackage{setspace}" + sep);
		latexCode.append("\\usepackage{pdfpages}" + sep);
		latexCode.append("\\usepackage{bookmark}" + sep);
		latexCode.append("\\usepackage[space]{grffile}" + sep);
		//latexCode.append("\\usepackage{blindtext}" + sep);
		latexCode.append("\\usepackage[usenames]{color}" + sep);
		latexCode.append("\\usepackage{natbib}" + sep);
		latexCode.append(" \\bibpunct{(}{)}{;}{a}{}{,} % to follow the A&A style" + sep);
		latexCode.append(sep);
		latexCode.append("\\pdfpagewidth 8.5in" + sep);
		latexCode.append("\\pdfpageheight 11in " + sep);
		latexCode.append("\\setlength\\topmargin{0in}" + sep);
		latexCode.append("\\setlength\\headheight{0in}" + sep);
		latexCode.append("\\setlength\\headsep{0in}" + sep);
		latexCode.append("\\setlength\\textheight{"+Functions.formatValue(h, 1)+"in}" + sep);
		latexCode.append("\\setlength\\textwidth{"+Functions.formatValue(w, 1)+"in}" + sep);
		latexCode.append("\\setlength\\oddsidemargin{0in}" + sep);
		latexCode.append("\\setlength\\evensidemargin{0in}" + sep);
		latexCode.append("\\setlength\\parindent{0.25in}" + sep);
		latexCode.append("\\setlength\\parskip{0.25in}" + sep);
		latexCode.append(sep);
		latexCode.append("\\begin{document}" + sep);
		if( AtlasProperties.getProperties().isWhite()) {
			latexCode.append("\\rowcolors{2}{gray!25}{white}" + sep);
		}
		else {
			latexCode.append("\\rowcolors{2}{gray!85}{black}" + sep);
		}
		latexCode.append("\\setcounter{secnumdepth}{4}" + sep); 		// titles with up to 4 levels

		header = true;
		if (title != null) {
			latexCode.append("\\title{");
			this.writeTextWithStyleAndColor(title, false);
			latexCode.append("}" + sep);
			latexCode.append("\\maketitle" + sep);
		}
	}

	/**
	 * Writes the header of the Latex file with an author.
	 * Not compatible with HTML.
	 * @param documentType Document type. Set to null to use default article.
	 * @param title Title to be seen in the document.
	 * @param author Document author.
	 * @param date Date.
	 * @param packages A set of package names to be imported using usepackage Latex command.
	 * Can be null to import no additional one. Default imported packages are graphicx, txfonts,
	 * verbatim, hyperref, spanish {babel}, longtable, usenames {color}, natbib.
	 */
	public void writeHeader(String documentType, String title, String author, String date, String packages[])
	{
		if (documentType == null) documentType = "article";
		latexCode.append("% "+msg + TimeFormat.dateNow(DateFormat.FULL) + sep);
		latexCode.append("\\documentclass{"+documentType+"}" + sep);
		latexCode.append("\\usepackage[T1]{fontenc}" + sep);

		latexCode.append("\\usepackage{graphicx}" + sep);
		latexCode.append("\\usepackage{ulem}" + sep);
		latexCode.append("\\usepackage{txfonts}" + sep);
		latexCode.append("\\usepackage[table]{xcolor}" + sep);
		latexCode.append("\\usepackage{verbatim}" + sep);
		latexCode.append("\\usepackage{hyperref}" + sep);
//		if (Translate.getDefaultLanguage() == LANGUAGE.SPANISH)
//			latexCode.append("\\usepackage[spanish]{babel}" + sep);
//		if (Translate.getDefaultLanguage() == LANGUAGE.ITALIAN)
//			latexCode.append("\\usepackage[italian]{babel}" + sep);
		latexCode.append("\\usepackage[" + Translate.getDefaultLanguage().toString().toLowerCase() +"]{babel}" + sep);
		
		latexCode.append("\\usepackage{longtable}" + sep);
		latexCode.append("\\usepackage{makecell}" + sep);
		latexCode.append("\\usepackage{ragged2e}" + sep);
		latexCode.append("\\usepackage{setspace}" + sep);
		latexCode.append("\\usepackage{pdfpages}" + sep);
		latexCode.append("\\usepackage{bookmark}" + sep);
		latexCode.append("\\usepackage{natbib}" + sep);
		if (packages != null && packages.length > 0) {
			for (int i=0; i<packages.length; i++) {
				latexCode.append("\\usepackage{"+packages[i]+"}" + sep);
			}
		}
		latexCode.append("\\usepackage[space]{grffile}" + sep);
		//latexCode.append("\\usepackage{blindtext}" + sep);
		latexCode.append("\\usepackage[usenames]{color}" + sep);
		latexCode.append(" \\bibpunct{(}{)}{;}{a}{}{,} % to follow the A&A style" + sep);
		latexCode.append(sep);
		latexCode.append("\\pdfpagewidth 8.5in" + sep);
		latexCode.append("\\pdfpageheight 11in " + sep);
		latexCode.append("\\setlength\\topmargin{0in}" + sep);
		latexCode.append("\\setlength\\headheight{0in}" + sep);
		latexCode.append("\\setlength\\headsep{0.5in}" + sep);
		latexCode.append("\\setlength\\textheight{7.7in}" + sep);
		latexCode.append("\\setlength\\textwidth{6.5in}" + sep);
		latexCode.append("\\setlength\\oddsidemargin{0in}" + sep);
		latexCode.append("\\setlength\\evensidemargin{0in}" + sep);
		latexCode.append("\\setlength\\parindent{0.25in}" + sep);
		latexCode.append("\\setlength\\parskip{0.25in}" + sep);
		latexCode.append(sep);
		latexCode.append("\\begin{document}" + sep);
		if( AtlasProperties.getProperties().isWhite()) {
			latexCode.append("\\rowcolors{2}{gray!25}{white}" + sep);
		}
		else {
			latexCode.append("\\rowcolors{2}{gray!85}{black}" + sep);
		}
		latexCode.append("\\setcounter{secnumdepth}{4}" + sep); 		// titles with up to 4 levels

		header = true;
		if (author != null) {
			latexCode.append("\\author{");
			this.writeTextWithStyleAndColor(author, false);
			latexCode.append("}" + sep);
		}
		if (date != null) {
			latexCode.append("\\date{");
			this.writeTextWithStyleAndColor(date, false);
			latexCode.append("}" + sep);
		}
		if (title != null) {
			latexCode.append("\\title{");
			this.writeTextWithStyleAndColor(title, false);
			latexCode.append("}" + sep);
			latexCode.append("\\maketitle" + sep);
		}
	}

	/**
	 * Writes the header of the Latex file for a presentation.
	 * Not compatible with HTML.
	 * @param theme The theme name, or null to use Warsaw.
	 * @param title Title to be seen in the document.
	 * @param author Document author.
	 * @param institute The institute.
	 * @param date Date.
	 * @param packages A set of package names to be imported using usepackage Latex command.
	 * Can be null to import no additional one. Default imported packages are graphicx, txfonts,
	 * verbatim, hyperref, spanish {babel}, longtable, usenames {color}, natbib.
	 * @param hideNavigationControls True to hide navigation controls.
	 * @param showTitlePage True to include the first frame showing a default presentation title.
	 * @param forceSmallNavigationBar True to force the navigation bar to show just one section and
	 * subsection (the current one).
	 * @param leftMargin Left margin as a value in cm for the text location. Null for default value.
	 * @param rightMargin Right margin as a value in cm for the text location. Null for default value.
	 */
	public void writeHeaderForPresentationUsingBeamer(String theme, String title, String author, String institute, String date,
			String packages[], boolean hideNavigationControls, boolean showTitlePage, boolean forceSmallNavigationBar,
			String leftMargin, String rightMargin)
	{
		if (theme == null) theme = "Warsaw";
		header = true;
		latexCode.append("% "+msg + TimeFormat.dateNow(DateFormat.FULL) + sep);
		latexCode.append("\\documentclass{beamer}" + sep);
		latexCode.append("\\usepackage[T1]{fontenc}" + sep);

		latexCode.append("\\usepackage[size=a4]{beamerposter}" + sep);
//		if (Translate.getDefaultLanguage() == LANGUAGE.ITALIAN)
//			latexCode.append("\\usepackage[italian]{babel}" + sep);
		latexCode.append("\\usepackage[" + Translate.getDefaultLanguage().toString().toLowerCase() +"]{babel}" + sep);

		latexCode.append("\\usepackage[table]{xcolor}" + sep);
		latexCode.append("\\usepackage{bookmark}" + sep);
		/// navigatore
		latexCode.append("\\usepackage{tikz}" + sep);
		latexCode.append("\\usepackage[pscoord]{eso-pic}% The zero point of the coordinate systemis the lower left corner of the page (the default)." + sep);
		
		latexCode.append("\\usetikzlibrary{arrows, positioning, trees, mindmap, calc}" + sep);
		latexCode.append("\\newcommand*\\circled[1]{\\tikz[baseline=(char.base)]{" + sep);
		latexCode.append("         \\node[shape=circle,fill=green!20,draw,inner sep=4pt] (char) {#1};}}" + sep);
		latexCode.append("\\newcommand*\\mmcircled[1]{\\tikz[baseline=(char.base)]{").append(sep);
		latexCode.append("     \\node[shape=circle,draw,inner sep=4pt] (char) {#1};}}").append(sep);

		latexCode.append("\\newcommand{\\windrose}[5]{%" + sep);
		latexCode.append("\\setbox0=\\hbox{" + sep);
		latexCode.append("\\AddToShipoutPictureFG*{% Add <stuff> to current page foreground" + sep);
		if( AtlasProperties.getProperties().getPaperSize() instanceof A3) {
			latexCode.append("    \\put(\\LenToUnit{0.05\\paperwidth},\\LenToUnit{0.15\\paperheight}){\\vtop{{\\null}\\makebox[0pt][c]{%" + sep);
		}
		else {
			latexCode.append("    \\put(\\LenToUnit{0.071\\paperwidth},\\LenToUnit{0.22\\paperheight}){\\vtop{{\\null}\\makebox[0pt][c]{%" + sep);
		}
		latexCode.append("\\begin{tikzpicture}[" + sep);
		latexCode.append("rotate=90" + sep);
		latexCode.append(','+ sep);
		latexCode.append(" scale=0.35," + sep);
		latexCode.append("%           every node/.style={scale=0.6}" + sep);
		latexCode.append("%remember picture,overlay" + sep);
		latexCode.append(" ] " + sep);
		latexCode.append("\\coordinate (c1) at (0,0);" + sep);
		latexCode.append("\\draw (c1) circle(45 mm);" + sep);
		latexCode.append("\\node (a) at (0,0) {\\circled{#1}};%" + sep);
		latexCode.append("\\node (b) at (4.5,0) {\\circled{#2}};%" + sep);
		latexCode.append("\\node (c) at (0,4.5) {\\circled{#3}};%" + sep);
		latexCode.append("\\node (d) at (-4.5,0) {\\circled{#4}};%" + sep);
		latexCode.append("\\node (e) at (0, -4.5) {\\circled{#5}};%" + sep);
		latexCode.append("\\path[draw=black!60,solid,line width=0.8mm,fill=black!60,preaction={-triangle 90,thin,draw,shorten >=-1mm}] (a) -- (b);%" + sep);
		latexCode.append("\\path[draw=black!60,solid,line width=0.8mm,fill=black!60,preaction={-triangle 90,thin,draw,shorten >=-1mm}] (a) -- (c);%" + sep);
		latexCode.append("\\path[draw=black!60,solid,line width=0.8mm,fill=black!60,preaction={-triangle 90,thin,draw,shorten >=-1mm}] (a) -- (d);%" + sep);
		latexCode.append("\\path[draw=black!60,solid,line width=0.8mm,fill=black!60,preaction={-triangle 90,thin,draw,shorten >=-1mm}] (a) -- (e);%" + sep);
		latexCode.append("\\end{tikzpicture}%" + sep);
		latexCode.append("}}}%" + sep);
		latexCode.append("}%\\AddToShipoutPictureFG" + sep);
		latexCode.append("}%hbox" + sep);
		latexCode.append("}" + sep);
		//////////////
		
		latexCode.append("\\usepackage{booktabs}" + sep);		// for constellations list 
		latexCode.append("\\usepackage{pgfplotstable}" + sep);  // for constellations list
		latexCode.append("\\pgfplotsset{compat=1.15}" + sep);   // for constellations list
		latexCode.append("\\usepackage{adjustbox}" + sep);   // for constellations list
		

		
		

		if (packages != null && packages.length > 0) {
			for (int i=0; i<packages.length; i++) {
				latexCode.append("\\usepackage{"+packages[i]+"}" + sep);
			}
		}
		if (author != null) {
			latexCode.append("\\author{");
			this.writeTextWithStyleAndColor(author, false);
			latexCode.append("}" + sep);
		}
		if (institute != null) {
			latexCode.append("\\institute{");
			this.writeTextWithStyleAndColor(institute, false);
			latexCode.append("}" + sep);
		}
		if (date != null) {
			latexCode.append("\\date{");
			this.writeTextWithStyleAndColor(date, false);
			latexCode.append("}" + sep);
		}
		if (title != null) {
			latexCode.append("\\title{");
			this.writeTextWithStyleAndColor(title, false);
			latexCode.append("}" + sep);
		}
		if (hideNavigationControls) latexCode.append("\\usenavigationsymbolstemplate{}" + sep);
		if (leftMargin != null) latexCode.append("\\setbeamersize{text margin left="+leftMargin+"cm}" + sep);
		if (rightMargin != null) latexCode.append("\\setbeamersize{text margin right="+rightMargin+"cm}" + sep);

//		if (forceSmallNavigationBar) {
//			latexCode.append("\\setbeamertemplate{headline}" + sep);
//			latexCode.append("{%" + sep);
//			latexCode.append("\\leavevmode%" + sep);
//			latexCode.append("\\begin{beamercolorbox}[wd=.5\\paperwidth,ht=2.5ex,dp=1.125ex]{section in head/foot}%" + sep);
//			latexCode.append("\\hbox to .5\\paperwidth{\\hfil\\insertsectionhead\\hfil}" + sep);
//			latexCode.append("\\end{beamercolorbox}%" + sep);
//			latexCode.append("\\begin{beamercolorbox}[wd=.5\\paperwidth,ht=2.5ex,dp=1.125ex]{subsection in head/foot}%" + sep);
//			latexCode.append("\\hbox to .5\\paperwidth{\\hfil\\insertsubsectionhead\\hfil}" + sep);
//			latexCode.append("\\end{beamercolorbox}%" + sep);
//			latexCode.append("}" + sep);
//		}

		latexCode.append("\\setbeamertemplate{footline}[frame number]" + sep);
		
		latexCode.append(sep + "\\begin{document}" + sep);
		if( AtlasProperties.getProperties().isWhite()) {
			latexCode.append("\\rowcolors{2}{gray!25}{white}" + sep);
		}
		else {
			latexCode.append("\\rowcolors{2}{gray!85}{black}" + sep);
		}

		if (showTitlePage) {
			latexCode.append("\\begin{frame}[plain]" + sep);
			latexCode.append("\\titlepage" + sep);
			latexCode.append("\\end{frame}" + sep);
		}
	}


	/**
	 * Begins a frame for a presentation.
	 * @param plain True to begin a plain frame without decorations.
	 * @param addTitlePage True to add \\titlepage to the frame.
	 */
	public void beginFrame(boolean plain, boolean addTitlePage) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		if (!plain) {
			latexCode.append(sep + "\\begin{frame}" + sep);
		} else {
			latexCode.append(sep + "\\begin{frame}[plain]" + sep);
		}
		if (addTitlePage) latexCode.append("\\titlepage" + sep);
	}

	/**
	 * Begins a frame for a presentation.
	 * @param name Name.
	 */
	public void beginFrame(String name)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		if (name == null) {
			latexCode.append(sep + "\\begin{frame}" + sep);
		} else {
			latexCode.append(sep + "\\begin{frame}{"+name+"}" + sep);
		}
	}

	/**
	 * Ends a frame of a presentation.
	 */
	public void endFrame()
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\end{frame}" + sep);
	}

	/**
	 * Begins a section.
	 * @param name Name.
	 */
	public void beginSection(String name)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\section{");
		this.writeTextWithStyleAndColor(name, false);
		latexCode.append("}" + sep + sep);
	}
	/**
	 * Begins a sub-section.
	 * @param name Name.
	 */
	public void beginSubSection(String name)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\subsection{");
		this.writeTextWithStyleAndColor(name, false);
		latexCode.append("}" + sep + sep);
	}

	/**
	 * Begins a columns block.
	 */
	public void beginColumns() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{columns}");
	}
	/**
	 * Ends a columns block.
	 */
	public void endColumns() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\end{columns}");
	}

	/**
	 * Begins a column block.
	 * @param widthFraction Fraction of the width as a value, from 0 to 1.
	 */
	public void beginColumn(String widthFraction) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{column}{"+widthFraction+" \\textwidth}");
	}
	/**
	 * Ends a column block.
	 */
	public void endColumn() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\end{column}");
	}

	/**
	 * Writes a list of items.
	 * @param lines Lines to be formatted as items.
	 * @throws JPARSECException If the text style is invalid.
	 */
	public void writeList(String[] lines)
	throws JPARSECException{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		if (justify) {
			if (firstJustify) {
				latexCode.append("\\let\\olditem\\item" + sep);
				firstJustify = false;
			}
			latexCode.append("\\renewcommand\\item{\\olditem\\justifying}" + sep);
		} else {
			if (firstJustify) {
				latexCode.append("\\let\\olditem\\item" + sep);
				firstJustify = false;
			}
			latexCode.append("\\renewcommand\\item{\\olditem\\raggedright}" + sep);
		}
		latexCode.append(sep + "\\begin{itemize}" + sep);
		for (int i=0; i<lines.length; i++)
		{
			latexCode.append("\\item ");
			this.writeParagraph(lines[i]);
		}
		latexCode.append("\\end{itemize}" + sep + sep);
	}

	/**
	 * Writes a paragraph.
	 * @param text Text to write.
	 * @param indent The indentation in unit of cm of the first line.
	 */
	public void writeParagraph(String text, double indent)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		if (!latexCode.toString().endsWith(sep)) latexCode.append(sep);
		//\hspace{0.5cm}
		this.writeTextWithStyleAndColor(text, true);
		String beg = this.getBeginOfCurrentStyle(true);
		String s = latexCode.toString();
		String rest = s.substring(s.lastIndexOf(beg) + beg.length());
		setCode(s.substring(0, s.lastIndexOf(beg)) + beg + "\\hspace{"+Functions.formatValue(indent, 3)+"cm} " + rest);
		String ends = this.getEndOfCurrentStyle(true);
		setCode(latexCode.toString().substring(0, latexCode.toString().lastIndexOf(ends)) + "\\\\" + ends);
		latexCode.append(sep + sep);
	}

	/**
	 * Writes a paragraph.
	 * @param text Text to write.
	 */
	public void writeParagraph(String text)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep);
		this.writeTextWithStyleAndColor(text, true);
		String ends = this.getEndOfCurrentStyle(true);
		setCode(latexCode.toString().substring(0, latexCode.toString().lastIndexOf(ends)) + "\\\\" + ends);
		latexCode.append(sep + sep);
	}

	/**
	 * Writes the header of a table.
	 * @param caption Caption for the table.
	 */
	public void writeTableHeader(String caption)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{table}[h]" + sep);
		if (caption != null) {
			latexCode.append("\\caption{");
			this.writeTextWithStyleAndColor(caption, false);
			latexCode.append("}" + sep);
		}
		latexCode.append("\\begin{tabular*}{1.0\\textwidth}" + sep);
		insideTable = true;
		tableFirstLine = false;
	}
	/**
	 * Writes the header of a long table.
	 * @param caption Caption for the table.
	 * @param columnAligment Aligment command (l, r, c) for each column in the table;
	 */
	public void writeLongTableHeader(String caption, String columnAligment)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "% *** For decimal point alignment in numbers use the siunitx package and the S column alignment type ***");
		latexCode.append(sep + "\\begin{longtable}{"+columnAligment+"}" + sep);
		if (caption != null) {
			latexCode.append("\\caption{");
			this.writeTextWithStyleAndColor(caption, false);
			latexCode.append("} \\\\" + sep);
		}
		latexCode.append("\\hline\\noalign{\\smallskip}" + sep);
		insideTable = true;
		tableFirstLine = true;
	}
	/**
	 * Ends a table tag.
	 */
	public void endTable()
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\noalign{\\smallskip}\\hline" + sep);
		latexCode.append("\\end{tabular*}" + sep);
		latexCode.append("\\end{table}" + sep + sep);
		insideTable = false;
		tableFirstLine = false;
	}
	/**
	 * Ends a table tag adding a label.
	 * Not compatible with HTML.
	 * @param label The label.
	 */
	public void endTable(String label)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\noalign{\\smallskip}\\hline" + sep);
		latexCode.append("\\end{tabular*}" + sep);
		if (label != null) latexCode.append("\\label{"+label+"}" + sep);
		latexCode.append("\\end{table}" + sep + sep);
		insideTable = false;
		tableFirstLine = false;
	}

	/**
	 * Write a row in a table.<P>
	 *
	 * Align commands are right, left, center.<P>
	 *
	 * Column span is the number of columns to be agruped by a given text. If a table has 6 columns
	 * (length of columns array), then a column span of 6 alows to write text ocuping the whole row,
	 * as a title for example. In this case the columns array should have length 1 to write only that
	 * title.
	 *
	 * @param columns Columns to be written to complete the row.
	 * @param bgcolor Background color. Ignored, only for compatibility with HTML report.
	 * @param align Align command. Set to null to use default left align.
	 * @param colspan Column span command. Set to null to skip the command.
	 */
	public void writeRowInTable(String columns[], String bgcolor, String align, String colspan)
	{
		this.writeRowInTable(columns, bgcolor, align, colspan, true);
	}

	/**
	 * Write a row in a table.<P>
	 *
	 * Align commands are right, left, center.<P>
	 *
	 * Column span is the number of columns to be agruped by a given text. If a table has 6 columns
	 * (length of columns array), then a column span of 6 alows to write text ocuping the whole row,
	 * as a title for example. In this case the columns array should have length 1 to write only that
	 * title.
	 *
	 * @param columns Columns to be written to complete the row.
	 * @param bgcolor Background color. Ignored, only for compatibility with HTML report.
	 * @param align Align command. Set to null to use default left align.
	 * @param colspan Column span command. Set to null to skip the command.
	 * @param format True to format text with the current style and color.
	 */
	public void writeRowInTable(String columns[], String bgcolor, String align, String colspan, boolean format)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		if (align == null) {
			align = "l";
		} else {
			align = align.substring(0, 1).toLowerCase();
			if (!align.equals("c") && !align.equals("l") && !align.equals("r")) align = "l";
		}
		String cols = "";
		int howMany = columns.length;
		if (colspan != null) howMany *= Integer.parseInt(colspan);
		for (int i=0; i<howMany; i++)
		{
			cols += align;
		}
		if (!tableFirstLine) {
			latexCode.append("{@{\\extracolsep{\\fill}}"+cols+"}" + sep);
			latexCode.append("\\hline\\noalign{\\smallskip}" + sep);
			tableFirstLine = true;
		}
//		int begin = latexCode.lastIndexOf("begin{tabular*}");
//		int end = latexCode.lastIndexOf("\\\\");
//		if (end < begin) {
//			latexCode.append("{@{\\extracolsep{\\fill}}"+cols+"}" + sep);
//			latexCode.append("\\hline\\noalign{\\smallskip}" + sep);
//		}

		for (int i=0; i<columns.length; i++)
		{
			if (!columns[i].trim().equals("")) {
				String line = "";
				if (colspan == null) {
					line = "";
				} else {
					line = "\\multicolumn{"+colspan+"}{c}{";
				}

				if (!line.equals(""))
				{
					latexCode.append(line);
					if (format) {
						this.writeTextWithStyleAndColor(columns[i], false);
					} else {
						latexCode.append(columns[i]);
					}
					latexCode.append('}');
					i = i + Integer.parseInt(colspan);
				} else {
					if (format) {
						this.writeTextWithStyleAndColor(columns[i], false);
					} else {
						latexCode.append(columns[i]);
					}
				}
			}
			if (i < (columns.length - 1)) latexCode.append(" & ");
		}
		latexCode.append(" \\\\"+sep);
	}

	/**
	 * Returns the code for an image tag. None of the parameters can be null.
	 * @param width Width. As a number for pixels or as percentage.
	 * @param height Height. As a number for pixels or as percentage.
	 * @param align Align.
	 * @param border Border. Ignored
	 * @param alt Alt (tooltip text). Ignored.
	 * @param src Link to the image.
	 * @return Latex code.
	 */
	public String writeImage(String width, String height, String align, String border,
			String alt, String src)
	{
		String code = this.beginAlignment(align);
		if (width != null) {
			if (width.endsWith("%")) {
				double scale = Double.parseDouble(width.substring(0, width.length() - 1)) / 100.0;
				width = "" + scale + "\\textwidth";
			} else {
				width += "pt";
			}
		}
		if (height != null) {
			if (height.endsWith("%")) {
				double scale = Double.parseDouble(height.substring(0, height.length() - 1)) / 100.0;
				height = "" + scale + "\\textheight";
			} else {
				height += "pt";
			}
		}

		String fix = "";
		if (isFixFigures()) fix = "[!ht]";
		code += sep + "\\begin{figure}" + fix + sep;
		code += "\\includegraphics[";
		if (width != null) {
			code +="width="+width;
			if (height != null) code+=", ";
		}
		if (height != null) code += "height="+height;
		code +="]{"+src+"}"+sep;
		if (align != null) {
			if (align.toLowerCase().equals("left")) code += "\\hspace*{15cm}"+sep;
			if (align.toLowerCase().equals("right")) code += "\\hspace*{-15cm}"+sep;
		}

		code += "\\end{figure}"+sep + sep;
		code += this.endAlignment(align);
		return code;
	}

	/**
	 * Returns the code for an image tag. None of the parameters can be null.
	 * @param width Width. As a number for pixels or as percentage.
	 * @param height Height. As a number for pixels or as percentage.
	 * @param align Align.
	 * @param border Border. Ignored
	 * @param alt Alt (tooltip text). Ignored.
	 * @param src Link to the image.
	 * @return Latex code.
	 */
	public String writeImages(String width, String height, String align, String border,
			String alt, String src[])
	{
		String code = this.beginAlignment(align);
		if (width != null) {
			if (width.endsWith("%")) {
				double scale = Double.parseDouble(width.substring(0, width.length() - 1)) / 100.0;
				width = "" + scale + "\\textwidth";
			} else {
				width += "pt";
			}
		}
		if (height != null) {
			if (height.endsWith("%")) {
				double scale = Double.parseDouble(height.substring(0, height.length() - 1)) / 100.0;
				height = "" + scale + "\\textheight";
			} else {
				height += "pt";
			}
		}

		String fix = "";
		if (isFixFigures()) fix = "[!ht]";
		code += sep + "\\begin{figure}" + fix + sep;
		for (int i=0; i<src.length; i++) {
			code += "\\includegraphics[";
			if (width != null) {
				code +="width="+width;
				if (height != null) code+=", ";
			}
			if (height != null) code += "height="+height;
			code +="]{"+src[i]+"}"+sep;
		}
		if (align != null) {
			if (align.toLowerCase().equals("left")) code += "\\hspace*{15cm}"+sep;
			if (align.toLowerCase().equals("right")) code += "\\hspace*{-15cm}"+sep;
		}

		code += "\\end{figure}"+sep + sep;
		code += this.endAlignment(align);
		return code;
	}

	/**
	 * Writes the code for an image tag. Not compatible with HTML.
	 * @param width Width.
	 * @param height Height.
	 * @param angle Rotation angle of the image in degrees, or null.
	 * @param align Align.
	 * @param src Link to the image.
	 * @param caption Caption for the image. Can be null.
	 * @param label Label as an id for the image. Can be null.
	 */
	public void writeImageWithCaption(String width, String height, String angle, String align,
			String src, String caption, String label)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		String code = this.beginAlignment(align);
		if (width != null) {
			if (width.endsWith("%")) {
				double scale = Double.parseDouble(width.substring(0, width.length() - 1)) / 100.0;
				width = "" + Functions.formatValue(scale, 5) + "\\textwidth";
			} else {
				width += "pt";
			}
		}
		if (height != null) {
			if (height.endsWith("%")) {
				double scale = Double.parseDouble(height.substring(0, height.length() - 1)) / 100.0;
				height = "" + Functions.formatValue(scale, 5) + "\\textheight";
			} else {
				height += "pt";
			}
		}

		String fix = "";
		if (isFixFigures()) fix = "[!ht]";
		code += sep + "\\begin{figure}" + fix + sep;
		code += "\\includegraphics[";
		if (angle != null) {
			code += "angle="+angle;
			if (width != null || height != null) code += ", ";
		}
		if (width != null) {
			code +="width="+width;
			if (height != null) code+=", ";
		}
		if (height != null) code += "height="+height;
		code +="]{"+src+"}"+sep;
		if (align != null) {
			if (align.toLowerCase().equals("left")) code += "\\hspace*{15cm}"+sep;
			if (align.toLowerCase().equals("right")) code += "\\hspace*{-15cm}"+sep;
		}
//		if (caption != null) code += "\\smallskip"+sep;
		if (caption != null) {
			code += "\\caption{";
			code += this.getTextWithStyleAndColor(caption, false);
			code += "}"+sep;
		}
		if (label != null && !label.equals("")) code += "\\label{"+label+"}"+sep;
		code += "\\end{figure}"+sep + sep;
		code += this.endAlignment(align);

		this.latexCode.append(code);
	}
	
	public void writeNavigator(int center, int north, int east, int south , int west) {
		StringBuilder navigator = new StringBuilder("\\windrose");
		navigator.append('{').append(center).append('}');
		navigator.append('{').append(north).append('}');
		navigator.append('{').append(east).append('}');
		navigator.append('{').append(south).append('}');
		navigator.append('{').append(west).append('}');
		navigator.append(sep);
		
		this.latexCode.append(navigator.toString());
	}
	
	/**
	 * Writes the code for a set of images. Not compatible with HTML.
	 * @param width Width. Can be null.
	 * @param height Height. Can be null.
	 * @param angle Rotation angle of the image in degrees, or null.
	 * @param align Align.
	 * @param src Links to the images.
	 * @param caption Caption for the image. Can be null.
	 * @param label Label as an id for the image. Can be null.
	 */
	public void writeImagesWithCaption(String width, String height, String angle, String align,
			String[] src, String caption, String label)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		String code = this.beginAlignment(align);
		if (width != null) {
			if (width.endsWith("%")) {
				double scale = Double.parseDouble(width.substring(0, width.length() - 1)) / 100.0;
				width = "" + Functions.formatValue(scale, 5) + "\\textwidth";
			} else {
				width += "pt";
			}
		}
		if (height != null) {
			if (height.endsWith("%")) {
				double scale = Double.parseDouble(height.substring(0, height.length() - 1)) / 100.0;
				height = "" + Functions.formatValue(scale, 5) + "\\textheight";
			} else {
				height += "pt";
			}
		}

		String fix = "";
		if (isFixFigures()) fix = "[!ht]";
		code += sep + "\\begin{figure}" + fix + sep;
		for (int i=0;i<src.length;i++) {
			code += "\\includegraphics[";
			if (angle != null) {
				code += "angle="+angle;
				if (width != null || height != null) code += ", ";
			}
			if (width != null) {
				code +="width="+width;
				if (height != null) code+=", ";
			}
			if (height != null) code += "height="+height;
			code +="]{"+src[i]+"}"+sep;
		}

		if (caption != null) code += "\\smallskip"+sep;
		if (caption != null) {
			code += "\\caption{";
			code += this.getTextWithStyleAndColor(caption, false);
			code += "}"+sep;
		}
		if (label != null && !label.equals("")) code += "\\label{"+label+"}"+sep;
		code += "\\end{figure}"+sep + sep;
		code += this.endAlignment(align);

		this.latexCode.append(code);
	}

//	/**
//	 * Returns the Latex code. The document IS closed
//	 * automatically in case it is still opened.
//	 * @return Latex code.
//	 */
////	public String getCode()
////	{
////		if (header && document) {
////			LaTeXBeamerGenerator lr = new LaTeXBeamerGenerator();
////			lr.header = true;
////			lr.setCode(this.latexCode.toString());
////			lr.endDocument();
////			return lr.latexCode.toString();
////		}
////		return this.latexCode.toString();
////	}
//	/**
//	 * Returns the Latex code. The document is NOT closed
//	 * automatically in case it is still opened.
//	 * @return Latex code.
//	 */
//	public String getCode()
//	{
//		return this.latexCode.toString();
//	}





	@Override
	public void writeHeader(String theme, String title, String author, String institute, String date, String[] packages,
			boolean hideNavigationControls, boolean showTitlePage, boolean forceSmallNavigationBar, String leftMargin,
			String rightMargin) {
		writeHeaderForPresentationUsingBeamer(null, title, author, institute, date, null, true, false, true, "0.25", "0.25");

		
	}
//	@Override
//	public void setMapIndexTitle(int chartId, String constel, String text) {
//		
//		latexCode.append("\\frametitle{\\LARGE{\\textbf{Chart ").append(chartId + 1).append("}}, ");
//		latexCode.append(text);
//		latexCode.append('(').append(constel).append(")}").append(sep);
//	}
	
	@Override
	public void writeSeparator() {
		endColumn();
		beginColumn("0.5");		
	}
	@Override
	public void writeHeaderRowInTable(String[] columns, String bgcolor, String align, String colspan) {
		this.writeRowInTable(columns, bgcolor, "l", colspan, true);		
	}
}
