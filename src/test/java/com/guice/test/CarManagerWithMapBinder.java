package com.guice.test;

import java.util.Map;

import com.google.inject.Inject;

public class CarManagerWithMapBinder {

	private final Map<String, CarFactory<?>> factories;

	@Inject
	public CarManagerWithMapBinder(Map<String, CarFactory<?>> factories) {
		this.factories = factories;
	}
	
	public void createAllCars() {
		System.out.println("start createCars()");
		for(Map.Entry<String, CarFactory<?>> entry :factories.entrySet()) {
			String part ="assisted part";
			entry.getValue().create(part);
		}	
	}

	public void createCarByType(String type) {
		System.out.println("start createCarByType()");
//		for(Map.Entry<String, CarFactory<?>> entry :factories.entrySet()) {
//			if(entry.getKey().equals(type)) {
//				String part ="assisted part";
//				entry.getValue().create(part);
//				System.out.println(entry.toString());
//				
//			}
//		}	
		
		Car car = factories.get(type).create("engine");
		System.out.println(car);
	}
	
}