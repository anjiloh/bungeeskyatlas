/**
 * 
 */
package net.anc.atlas.pages;

import com.google.inject.assistedinject.Assisted;

import net.anc.atlas.Page;
import net.anc.atlas.index.MultiVolumeNovaeAndSupernovaeIndex;
import net.anc.atlas.index.NovaeAndSupernovaeIndex;
import net.anc.atlas.index.StarsByChartIndex;

/**
 * Factory (factories) for the pages
 * Used by Guice to auto-inject pages
 *  
 * @author Angelo Nicolini
 *
 */
public interface PageFactory<T extends Page> {

	T create(@Assisted String name);
}

interface HeaderPageFactory extends PageFactory<HeaderPage> { }
interface CreditsPageFactory extends PageFactory<CreditsPage> { }
interface PresentationPageFactory extends PageFactory<PresentationPage> { }
interface GenerationOptionsPageFactory extends PageFactory<GenerationOptionsPage> { }
interface LegendPageFactory extends PageFactory<LegendPage> { }
interface CatalogLegendPageFactory extends PageFactory<CatalogLegendPage> { }

interface LicensePageFactory extends PageFactory<GFDLLicensePage> { }
interface HistoryPageFactory extends PageFactory<HistoryPage> { }

interface ChartPageFactory extends PageFactory<ChartPage> { }
interface CatalogChartPageFactory extends PageFactory<CatalogChartPage> {}
interface MinimapsChartsPageFactory extends PageFactory<MinimapsChartsPage> {}

interface ConstellationsByChartPageFactory extends PageFactory<ConstellationsByChartIndex> { }
interface DSOsByChartPageFactory extends PageFactory<ObjectsByChartIndex> { }
interface StarsByChartPageFactory extends PageFactory<StarsByChartIndex> { }
interface NovaeByChartPageFactory extends PageFactory<NovaeAndSupernovaeIndex> { }
interface MultiVolumeNovaeByChartPageFactory extends PageFactory<MultiVolumeNovaeAndSupernovaeIndex> { }
interface MultiVolumeConstellationsByChartPageFactory extends PageFactory<MultiVolumeConstellationsByChartIndex> {}



