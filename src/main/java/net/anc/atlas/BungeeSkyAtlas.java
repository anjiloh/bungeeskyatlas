/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;

import jparsec.util.JPARSECException;

/**
 * Implementations on this interface defines the full atlas creation
 * 
 * @author Angelo Nicolini
 *
 */
public interface BungeeSkyAtlas {

	
	String OBJECTS_INDEX_FILE = AtlasProperties.getProperties().getOutputPath() + "/objects_index.csv";
	String STARS_INDEX_FILE = AtlasProperties.getProperties().getOutputPath() + "/stars_index.csv";
	String NOVAE_INDEX_FILE = AtlasProperties.getProperties().getOutputPath() + "/novae_index.csv";

	public void create() throws JPARSECException, IOException;
}
