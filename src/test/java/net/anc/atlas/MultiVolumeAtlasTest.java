package net.anc.atlas;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import net.anc.atlas.pages.PagesModule;

public class MultiVolumeAtlasTest {

	protected Injector injector = Guice.createInjector(
			new TestModule(),
			new PagesModule(),
			new AbstractModule() {
		        @Override
		        protected void configure() {
		            bind(Atlas.class).to(MultiVolumeAtlas.class);
		        	
		        }
			}
			
			);

	@Inject
	protected AtlasGenerator generator;

	
	@Before
	public void setUp() throws Exception {
		injector.injectMembers(this);
	}

	

	@Test
	public void testStartAtlas() {
		Atlas subject = injector.getInstance(MultiVolumeAtlas.class);
		subject.startAtlas();
		
		assertTrue(generator.getCode().contains("Volume I"));

		subject.startAtlas();
		
		assertTrue(generator.getCode().contains("Volume II"));
	}

}
