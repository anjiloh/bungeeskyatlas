/**
 * 
 */
package net.anc.atlas;


/**
 * Thes sky must be divided in sections
 * 
 * @author Angelo Nicolini
 *
 */
public class SkyMap  implements Content {
	
	public SkyMap(double ra, double dec, int chartId) {
		this.ra = ra;
		this.dec = dec;
		this.chartId = chartId;
	}
	public double ra;
	public double dec;
	
	private int chartId;
	
	public SkyMap north;
	public SkyMap south;
	/**
	 * right neighbour
	 */
	public SkyMap east;
	/**
	 * left neighbour
	 */
	public SkyMap west;
	
    @Override
	public String toString() {
		return "(" + ra + ", " + dec + ")";
		
	}
    
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + Double.valueOf(ra).hashCode();
        result = 31 * result + Double.valueOf(dec).hashCode();
        result = 31 * result + chartId;
        result = 31 * result + north.hashCode();
        result = 31 * result + south.hashCode();
        result = 31 * result + west.hashCode();
        result = 31 * result + east.hashCode();

        return result;

    }
	
    @Override
	public boolean equals(Object other) {
		if( other instanceof SkyMap) {
			return ((SkyMap)other).ra == this.ra && ((SkyMap)other).dec == this.dec;
		}
		else  {
			return false;
		}
		
	}

	/**
	 * no real use here...
	 */
	@Override
	public int size() {
		return 0;
	}


	public int getChartId() {
		return chartId;
	}
	
	/**
	 * This returns the angular position of the center of this map.
	 * @return
	 */
	public double getTetha() {
		double theta = 360 * ra / 24 - 90.0;
		if( !AtlasProperties.getProperties().isReflector() ) {
			theta += 180.0;
		}
		return theta;
	}
	
}
