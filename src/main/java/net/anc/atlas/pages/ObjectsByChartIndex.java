package net.anc.atlas.pages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;

import jparsec.ephem.Functions;
import jparsec.graph.DataSet;
import jparsec.io.FileIO;
import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.Index;
import net.anc.atlas.Table;
import net.anc.atlas.serializer.GeneratorFormat;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public class ObjectsByChartIndex extends BaseIndex implements Index, Table {

	private static final Character DEG = Character.valueOf((char)176);
	
	@Inject
	private AtlasGenerator generator;

	
	@Override
	public void setHeader(String caption, String[] header) {
		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BEAMER ) {
			generator.writeTableHeader(caption);
		}
		else {
			generator.writeLongTableHeader(
					caption, 
					getColumnsAlign(header.length)
					);
			
			generator.writeHeaderRowInTable(header, null, null, null);
			
		}
		generator.writeHorizontalLine();
		this.caption = caption;
	}
	
	protected String getColumnsAlign(int headerLen) {
		return String.format("%0" + (headerLen -1 ) + "d", 0).replace("0","l")+"p{0.2\\linewidth}p{0.6\\linewidth}";
	}

	
	private static Map<String, String> typeDesc = new HashMap<String, String>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7060074555636995361L;

		{
			put("1", "unknown");
			put("1", "Galaxy");
			put("2", "Nebula");
			put("3", "Planetary Nebula");
			put("4", "open cluster");
			put("5", "globular cluster");
			put("6", "galpart");
			put("7", "qua");
			put("8", "duplicate");
			put("9", "duplicateInNGC");
			put("10", "star/s");
			put("11", "notFound");
		}
	};
	
	private String caption;
	


	private static String[] columnNames = {
			Translate.translate("Name"), 
			Translate.translate("Right ascension"), // Translate.translate("Right ascension") +  " (J" + AtlasProperties.getProperties().getYear() + ".0)",
			Translate.translate("Declination"), 
			"Mag.", 
			Translate.translate("Type"),
			"\\makecell[l]{" + Translate.translate("Angular radius")  + "\\ (" + DEG + ")}", 
			Translate.translate("Constellation"), 
			Translate.translate("Note")
	
	};
	
	

	@Override
	public void writeContent(List<Content> content) {
			
			// generator.setTextStyle(STYLE.PLAIN);
			
			if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BEAMER) {
				int nmax = content.size();
				
				boolean twoColumn = false;
				if (nmax > 70) {
					twoColumn = true;
					generator.beginColumns();
					generator.beginColumn("0.5");
					nmax = 70;
				}
				// colum one
				writeContents(content, 0, nmax);
				// colum two, if needed
				if (twoColumn) {
					if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH) {
						generator.writeTableHeader(caption + " (continuation)");
					}
					else {
						generator.writeTableHeader(caption + " (continuazione)");
					}
				
					generator.writeHorizontalLine();
					generator.writeSeparator( );
					writeContents(content, 70, Math.min(content.size(), 140));
					generator.endColumn();
					generator.endColumns();
				}	
				
				generator.endTable();
				generator.endFrame();
			}
			else {
				writeContents(content, 0, content.size());
//				((LaTeXBookGenerator)LaTexGenerator.getGenerator()).
				generator.endLongTable(null); 
			}
			
		
	}
	
	/**
	 * add data to the table
	 * @param data
	 * @param from
	 * @param to
	 */
	protected void writeContents(List<Content> data, int from, int to) {
		for (int i = from ; i < to; i++) {
			AstroItemDetail next = (AstroItemDetail)data.get(i);
			
			String dec = DataSet.replaceOne(Functions.formatDEC(next.getLatitude(), 0), "\"", "$\"$", 1);
			if( !dec.startsWith("-")) {
				dec = "+" + dec;
			}
			String magnitude =  Functions.formatValue(next.getMagnitude(), 1);
			if (magnitude.equals("100.0")) {
				magnitude = "-";
			}
			String angle = next.getAngle();
			if (angle.length() > 11) {
				String f1 = FileIO.addSpacesAfterAString(FileIO.getField(1, angle, "x", false), 5);
				String f2 = FileIO.addSpacesAfterAString(FileIO.getField(2, angle, "x", false), 5);
				angle = f1.substring(0, 5).trim() + "x" + f2.substring(0, 5).trim();
			}
			String type = DataSet.capitalize(typeDesc.get(next.getType()), false);
			if( type == null) {
				type = "";
			}
			
			String[] datai = new String[] {
					next.getName(), 
					Functions.formatRA(next.getLongitude(), 1),  
					dec,
					magnitude, 
					Translate.translate(type),
					angle, 
					next.getConstellation(),
					next.getExtraInfo()
			};
			generator.writeRowInTable(datai, null, null, null);
		}
	}

	@Override
	public String[] getColumnsName() {
		return columnNames;
	}

	@Override
	public String getId() {
		return "Objects.Index.title";
	}
	


}
