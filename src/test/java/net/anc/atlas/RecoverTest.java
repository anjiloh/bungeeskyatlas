package net.anc.atlas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jparsec.io.FileIO;

public class RecoverTest {

	@Before
	public void setUp() throws Exception {

		Field index = RecoveryPoint.class.getDeclaredField("MAP_INDEX_FILE");
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(index, index.getModifiers() & ~Modifier.FINAL);

		System.out.println(RecoveryPoint.MAP_INDEX_FILE);

		Method setOutputPath = AtlasProperties.class.getDeclaredMethod("setOutputPath", String.class);
		setOutputPath.setAccessible(true);
		setOutputPath.invoke(AtlasProperties.getProperties(), "d:/");

	}

	@After
	public void tearDown() {
		try {
			Files.delete(Paths.get(RecoveryPoint.MAP_INDEX_FILE));
		} catch (IOException e) {
			// ignore
		}
		try {
			Files.delete(Paths.get(BungeeSkyAtlas.OBJECTS_INDEX_FILE));
		} catch (IOException e) {
			// ignore
		}
		try {
			Files.delete(Paths.get(BungeeSkyAtlas.STARS_INDEX_FILE));
		} catch (IOException e) {
			// ignore
		}
		try {
			Files.delete(Paths.get(BungeeSkyAtlas.NOVAE_INDEX_FILE));
		} catch (IOException e) {
			// ignore
		}
	}

	@Test
	public void testGetChartId() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		Recover subject = new Recover();

		Method method = subject.getClass().getDeclaredMethod("getChartId", List.class);
		method.setAccessible(true);

		List<String> testable = new ArrayList<>();

		testable.add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart1.pdf,0.5235987755982988,1.3439035240356338");

		Object r = method.invoke(subject, testable);
		assertTrue(r instanceof Integer);
		assertEquals((int) r, 1);

		testable.add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart3.pdf,0.5235987755982988,0.8901179185171081");

		r = method.invoke(subject, testable);
		assertTrue(r instanceof Integer);
		assertEquals((int) r, 3);

		testable.add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart4.pdf,0.5");

		r = method.invoke(subject, testable);
		assertTrue(r instanceof Integer);
		// the last line is not complete! let's ignore it!
		assertEquals((int) r, 3);

	}

	@Test
	public void testPurgeRecoveryFromChartId() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2188954849720566917L;

			{
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart0.pdf,0.5235987755982988,1.5707963267948966");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart1.pdf,0.5235987755982988,1.3439035240356338");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart2.pdf,0.5235987755982988,1.117010721276371");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart3.pdf,0.5235987755982988,0.8901179185171081");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart4.pdf,0.5235987755982988,0.6632251157578453");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart5.pdf,0.5235987755982988,0.4363323129985824");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart6.pdf,0.5235987755982988,0.20943951023931956");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart7.pdf,0.5235987755982988,-0.017453292519943295");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart8.pdf,0.5235987755982988,-0.24434609527920614");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart9.pdf,0.5235987755982988,-0.47123889803846897");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart10.pdf,0.5235987755982988,-0.6981317007977318");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart11.pdf,0.5235987755982988,-0.9250245035569946");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart12.pdf,0.5235987755982988,-1.1519173063162575");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart13.pdf,0.5235987755982988,-1.3788101090755203");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart14.pdf,0.5235987755982988,-1.5707963267948966");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(RecoveryPoint.MAP_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected exception " + e.getMessage());
			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeRecoveryFromChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(RecoveryPoint.MAP_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(2, results.size());
	}

	@Test
	public void testPurgeRecoveryFromChartIdBrokenFile() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -1271882617460514798L;

			{
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart0.pdf,0.5235987755982988,1.5707963267948966");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart1.pdf,0.5235987755982988,1.3439035240356338");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart2.pdf,0.5235987755982988,1.117010721276371");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart3.pdf,0.5235987755982988,0.8901179185171081");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart4.pdf,0.5235987755982988,0.6632251157578453");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart5.pdf,0.5235987755982988,0.4363323129985824");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart6.pdf,0.5235987755982988,0.20943951023931956");
				add("d:\\tmp\\atlas\\BungeeSkyAtlas_18d_A318_chart");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(RecoveryPoint.MAP_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected IOException " + e.getMessage());

			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeRecoveryFromChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(RecoveryPoint.MAP_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(2, results.size());
	}

	@Test
	public void testPurgeObjectsChartId() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2188954849720566917L;

			{
				add("188 CALDWELL 1,0.21575668454170227,1.4897487163543701,1.0,8.1,0.125x0.125,4,Cepheus,DSO,0");
				add("2655,2.347141742706299,1.3640402555465698,1.0,10.1,0.04083333x0.03416667,1,Camelopardalis,DSO,0");
				add("2336,1.964031457901001,1.3987067937850952,1.0,10.4,0.05916667x0.0325,1,Camelopardalis,DSO,0");
				add("2146,1.6645985841751099,1.3674143552780151,1.0,10.6,0.045x0.02416667,1,Camelopardalis,DSO,1");
				add("I.3568,3.2881789207458496,1.4392859935760498,1.0,10.6,0.00141667x0.00141667,3,Camelopardalis,DSO,1");
				add("2300,1.9989513158798218,1.4952011108398438,1.0,11.0,0.02333333x0.01666667,1,Cepheus,DSO,2");
				add("2715,2.4011592864990234,1.3615694046020508,1.0,11.2,0.04x0.01333333,1,Camelopardalis,DSO,2");
				add("2276,1.9770854711532593,1.4960365295410156,1.0,11.4,0.01916667x0.01583333,1,Cepheus,DSO,3");
				add("2268,1.915726661682129,1.4721732139587402,1.0,11.5,0.0225x0.0125,1,Camelopardalis,DSO,3");
				add("2732,2.424791097640991,1.3807746171951294,1.0,11.9,0.0175x0.0075,1,Camelopardalis,DSO,4");
				add("I.512,2.392052412033081,1.4910167455673218,1.0,12.2,0.015x0.01083333,1,Camelopardalis,DSO,5");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(BungeeSkyAtlas.OBJECTS_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected exception " + e.getMessage());

			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeObjectsChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(BungeeSkyAtlas.OBJECTS_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(5, results.size());

		results.forEach(line -> {
			String[] data = line.split(",");
			assertEquals(10, data.length);
			assertTrue(Integer.valueOf(data[9]) < 2);
		});
	}

	@Test
	public void testPurgeObjectsChartIdBrokenFile() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -1271882617460514798L;

			{
				add("188 CALDWELL 1,0.21575668454170227,1.4897487163543701,1.0,8.1,0.125x0.125,4,Cepheus,DSO,0");
				add("2655,2.347141742706299,1.3640402555465698,1.0,10.1,0.04083333x0.03416667,1,Camelopardalis,DSO,0");
				add("2336,1.964031457901001,1.3987067937850952,1.0,10.4,0.05916667x0.0325,1,Camelopardalis,DSO,0");
				add("2146,1.6645985841751099,1.3674143552780151,1.0,10.6,0.045x0.02416667,1,Camelopardalis,DSO,1");
				add("I.3568,3.2881789207458496,1.4392859935760498,1.0,10.6,0.00141667x0.00141667,3,Camelopardalis,DSO,1");
				add("2300,1.9989513158798218,1.4952011108398438,1.0,11.0,0.02333333x0.01666667,1,Cepheus,DSO,2");
				add("2715,2.4011592864990234,1.3615694046020508,1.0,11.2,0.04x0.01333333,1,Camelopardalis,DSO,2");
				add("2276,1.9770854711532593,1.4960365295410156,1.0,11.4,0.01916667x0.01583333,1,Cepheus,DSO,3");
				add("2268,1.915726661682129,1.4721732139587402,1.0,11.5,0.0225x0.0125,1,Camelopardalis,DSO,3");
				add("2732,2.424791097640991,1.3807746171951294,1.0,11.9,0.0175x0.0075,1,Camelopardalis,DSO,4");
				add("I.512,2.392052412033081,1.4910167455673218,1.0,12.2,0.015x0.01083333,1,Ca");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(BungeeSkyAtlas.OBJECTS_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected exception " + e.getMessage());

			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeObjectsChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(BungeeSkyAtlas.OBJECTS_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(5, results.size());
		results.forEach(line -> {
			String[] data = line.split(",");
			assertEquals(10, data.length);
			assertTrue(Integer.valueOf(data[9]) < 2);
		});
	}

	@Test
	public void purgeStarFromChartId() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2188954849720566917L;

			{
				add("Sirius,1.7677943505397833,-0.29175125991929673,2.63706131835522,0.0,2.63706131835522,,Canis Major,STAR,0");
				add("Canopus,1.675305906381518,-0.9197127926668318,94.78672814415795,0.0,94.78672814415795,,Carina,STAR,0");
				add("Arcturus,3.7335263105401464,0.3347961983416263,11.257457833918485,0.0,11.257457833918485,,Bootes,STAR,0");
				add("Toliman,3.8380149498060296,-1.0617534695397555,1.3248367181601524,0.0,1.3248367181601524,,Centaurus,STAR,1");
				add("Capella,1.3818177990915128,0.8028164004557665,13.123360105634077,0.0,13.123360105634077,,Auriga,STAR,2");
				add("Vega,4.873565507966297,0.6769030683515622,7.678722512531997,0.0,7.678722512531997,,Lyra,STAR,3");
				add("Rigel,1.3724303558701985,-0.14314559352713446,264.55026655260417,0.0,264.55026655260417,,Orion,STAR,3");
				add("Procyon,2.0040830037364605,0.0911932362453849,3.5141973874739367,0.0,3.5141973874739367,,Canis Minor,STAR,4");
				add("Achernar,0.42635815483446293,-0.9989697896148032,42.75331449741566,0.0,42.75331449741566,,Eridanus,STAR,5");
				add("Acamar,0.7778143447690485,-0.7034502530993098,29.76190611348407,0.0,29.76190611348407,,Eridanus,STAR,5");
				add("Betelgeuse,1.5497291379154017,0.12927765461867483,152.67175127941593,0.0,152.67175127941593,,Orion,STAR,6");
				add("Agena,3.681874256983143,-1.0537082429992082,120.19231210093542,0.0,120.19231210093542,,Centaurus,STAR,6");
				add("Altair,5.195772355763607,0.15478140399460139,5.1295204701413395,0.0,5.1295204701413395,,Aquila,STAR,7");
				add("Aldebaran,1.203930959247178,0.2881416832321906,20.433184063357338,0.0,20.433184063357338,,Taurus,STAR,8");
				add("Spica,3.5133171949608895,-0.19480177388024786,76.56967594717345,0.0,76.56967594717345,,Virgo,STAR,8");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(BungeeSkyAtlas.STARS_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected exception " + e.getMessage());

			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeStarFromChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(BungeeSkyAtlas.STARS_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(4, results.size());

		results.forEach(line -> {
			String[] data = line.split(",");
			assertEquals(10, data.length);
			assertTrue(Integer.valueOf(data[9]) < 2);
		});
	}

	@Test
	public void purgeStarFromChartIdBrokenFile() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -1271882617460514798L;

			{
				add("Sirius,1.7677943505397833,-0.29175125991929673,2.63706131835522,0.0,2.63706131835522,,Canis Major,STAR,0");
				add("Canopus,1.675305906381518,-0.9197127926668318,94.78672814415795,0.0,94.78672814415795,,Carina,STAR,0");
				add("Arcturus,3.7335263105401464,0.3347961983416263,11.257457833918485,0.0,11.257457833918485,,Bootes,STAR,0");
				add("Toliman,3.8380149498060296,-1.0617534695397555,1.3248367181601524,0.0,1.3248367181601524,,Centaurus,STAR,1");
				add("Capella,1.3818177990915128,0.8028164004557665,13.123360105634077,0.0,13.123360105634077,,Auriga,STAR,2");
				add("Vega,4.873565507966297,0.6769030683515622,7.678722512531997,0.0,7.678722512531997,,Lyra,STAR,3");
				add("Rigel,1.3724303558701985,-0.14314559352713446,264.55026655260417,0.0,264.55026655260417,,Orion,STAR,3");
				add("Procyon,2.0040830037364605,0.0911932362453849,3.5141973874739367,0.0,3.5141973874739367,,Canis Minor,STAR,4");
				add("Achernar,0.42635815483446293,-0.9989697896148032,42.75331449741566,0.0,42.75331449741566,,Eridanus,STAR,5");
				add("Acamar,0.7778143447690485,-0.7034502530993098,29.76190611348407,0.0,29.76190611348407,,Eridanus,STAR,5");
				add("Betelgeuse,1.5497291379154017,0.12927765461867483,152.67175127941593,0.0,152.67175127941593,,Orion,STAR,6");
				add("Agena,3.681874256983143,-1.0537082429992082,120.19231210093542,0.0,120.19231210093542,,Centaurus,STAR,6");
				add("Altair,5.195772355763607,0.15478140399460139,5.1295204701413395,0.0,5.1295204701413395,,Aquila,STAR,7");
				add("Aldebaran,1.203930959247178,0.2881416832321906,20.433184063357338,0.0,20.433184063357338,,Taurus,STAR,8");
				add("Spica,3.5133171949608895,-0.19480177388024786,7");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(BungeeSkyAtlas.STARS_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected exception " + e.getMessage());

			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeStarFromChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(BungeeSkyAtlas.STARS_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(4, results.size());
		results.forEach(line -> {
			String[] data = line.split(",");
			assertEquals(10, data.length);
			assertTrue(Integer.valueOf(data[9]) < 2);
		});
	}

	@Test
	public void purgeNovaeFromChartId() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2188954849720566917L;

			{
				add("SN015 am,0.5682009453086211,0.5599405302539754,0.9999999999999999,18.7,Aug13,null,Triangulum,SUPERNOVA,0");
				add("SN015 ar,0.2981745094718588,0.5671586888150656,0.9999999999999999,18.8,Nov11,null,Pisces,SUPERNOVA,0");
				add("SN015 bg,0.7360283811783354,0.6105819251431982,0.9999999999999999,17.8,Dec16,null,Perseus,SUPERNOVA,1");
				add("SN015 ab,0.6525791468403236,0.35544026687216795,1.0,17.3,Jul10,null,Aries,SUPERNOVA,1");
				add("SN015 am,0.5682009453086211,0.5599405302539754,0.9999999999999999,18.7,Aug13,null,Triangulum,SUPERNOVA,2");
				add("SN015 ap,0.5505076246454758,0.10799832566111527,0.9999999999999999,17.3,Sep 8,null,Pisces,SUPERNOVA,6");
				add("SN015 ax,0.3456585539948291,0.2651857279210388,1.0,17.2,Oct30,null,Pisces,SUPERNOVA,6");
				add("SN015 ao,0.48729360806356264,-0.6269209922020904,0.9999999999999999,17.7,Oct 6,null,Fornax,SUPERNOVA,10");
				add("SN015 aw,0.5542677451997056,-0.9065030510135135,1.0,15.9,Jul12,null,Eridanus,SUPERNOVA,11");
				add("SN015 aj,1.7034355443418892,1.2967210082733385,0.9999999999999999,18.3,Sep 2,null,Camelopardalis,SUPERNOVA,16");
				add("SN015 bk,1.8545035347183378,1.0155977722492697,1.0,20.6,Dec31,null,Lynx,SUPERNOVA,17");
				add("SN015 bl,2.3613401784357406,0.9284138714634805,1.0,20.0,Dec30,null,Ursa Major,SUPERNOVA,18");
				add("SN015 U,1.9637944780027536,0.5895978572022003,1.0,16.4,Feb13,null,Gemini,SUPERNOVA,19");
				add("SN015 X,1.9104775834877612,0.5205121542100954,1.0,17.2,Feb13,null,Gemini,SUPERNOVA,19");
				add("SN015 I,1.914831666500457,0.4068542706078124,1.0,15.7,May 2,null,Gemini,SUPERNOVA,20");
				add("SN015 X,1.9104775834877612,0.5205121542100954,1.0,17.2,Feb13,null,Gemini,SUPERNOVA,20");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(BungeeSkyAtlas.NOVAE_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected exception " + e.getMessage());

			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeNovaeFromChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(BungeeSkyAtlas.NOVAE_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(4, results.size());

		results.forEach(line -> {
			String[] data = line.split(",");
			assertEquals(10, data.length);
			assertTrue(Integer.valueOf(data[9]) < 2);
		});
	}

	@Test
	public void purgeNovaeFromChartIdBrokenFile() throws NoSuchMethodException, SecurityException {

		List<String> fileContent = new ArrayList<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -1271882617460514798L;

			{
				add("SN015 am,0.5682009453086211,0.5599405302539754,0.9999999999999999,18.7,Aug13,null,Triangulum,SUPERNOVA,0");
				add("SN015 ar,0.2981745094718588,0.5671586888150656,0.9999999999999999,18.8,Nov11,null,Pisces,SUPERNOVA,0");
				add("SN015 bg,0.7360283811783354,0.6105819251431982,0.9999999999999999,17.8,Dec16,null,Perseus,SUPERNOVA,1");
				add("SN015 ab,0.6525791468403236,0.35544026687216795,1.0,17.3,Jul10,null,Aries,SUPERNOVA,1");
				add("SN015 am,0.5682009453086211,0.5599405302539754,0.9999999999999999,18.7,Aug13,null,Triangulum,SUPERNOVA,5");
				add("SN015 ap,0.5505076246454758,0.10799832566111527,0.9999999999999999,17.3,Sep 8,null,Pisces,SUPERNOVA,6");
				add("SN015 ax,0.3456585539948291,0.2651857279210388,1.0,17.2,Oct30,null,Pisces,SUPERNOVA,6");
				add("SN015 ao,0.48729360806356264,-0.6269209922020904,0.9999999999999999,17.7,Oct 6,null,Fornax,SUPERNOVA,10");
				add("SN015 aw,0.5542677451997056,-0.9065030510135135,1.0,15.9,Jul12,null,Eridanus,SUPERNOVA,11");
				add("SN015 aj,1.7034355443418892,1.2967210082733385,0.9999999999999999,18.3,Sep 2,null,Camelopardalis,SUPERNOVA,16");
				add("SN015 bk,1.8545035347183378,1.0155977722492697,1.0,20.6,Dec31,null,Lynx,SUPERNOVA,17");
				add("SN015 bl,2.3613401784357406,0.9284138714634805,1.0,20.0,Dec30,null,Ursa Major,SUPERNOVA,18");
				add("SN015 U,1.9637944780027536,0.5895978572022003,1.0,16.4,Feb13,null,Gemini,SUPERNOVA,19");
				add("SN015 X,1.9104775834877612,0.5205121542100954,1.0,17.2,Feb13,null,Gemini,SUPERNOVA,19");
				add("SN015 I,1.914831666500457,0.4068542706078124,1.0,15.7,May 2,null,Gemini,SUPERNOVA,20");
				add("SN015 X,1.9104775834877612,0.5205121542100954,1.0,17.2,F");

			}
		};

		fileContent.forEach(line -> {
			try {
				Files.write(Paths.get(BungeeSkyAtlas.NOVAE_INDEX_FILE), (line + FileIO.getLineSeparator()).getBytes(),
						StandardOpenOption.APPEND, StandardOpenOption.CREATE);
			} catch (IOException e) {
				fail("Unexpected exception " + e.getMessage());

			}
		});

		Recover subject = new Recover();
		Method method = subject.getClass().getDeclaredMethod("purgeNovaeFromChartId", int.class);
		method.setAccessible(true);

		try {
			method.invoke(subject, Integer.valueOf(2));
		} catch (Exception e) {
			fail("Unexpected exception " + e.getMessage());
		}

		List<String> results = null;
		try {
			results = Files.readAllLines(Paths.get(BungeeSkyAtlas.NOVAE_INDEX_FILE));
		} catch (IOException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(results);
		assertFalse(results.isEmpty());
		assertEquals(4, results.size());
		results.forEach(line -> {
			String[] data = line.split(",");
			assertEquals(10, data.length);
			assertTrue(Integer.valueOf(data[9]) < 2);
		});
	}
}
