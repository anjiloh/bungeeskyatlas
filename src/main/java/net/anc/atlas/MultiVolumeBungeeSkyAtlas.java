/**
 * 
 */
package net.anc.atlas;

import jparsec.util.JPARSECException;

/**
 * MultiVolume common implementation.
 * Must be specialized by concrete ones
 * 
 * @author Angelo Nicolini
 *
 */
public abstract class MultiVolumeBungeeSkyAtlas extends AbstractBungeeSkyAtlas {


	private int volumeCounter = 1;
	
	
	
	protected void initializeVolumeOne() throws JPARSECException{
		initialize();
		
	}
	
	protected void initializeVolumeTwo() throws JPARSECException{
		initialize();
		// TODO: how to force Volume II???
	}

	/**
	 * Atlas file name
	 * @return
	 */
	protected String getAtlasFileName() {
		int len =  AtlasProperties.getProperties().getLaTeXAtlasFileName().indexOf('.');
		
		String out =  AtlasProperties.getProperties().getLaTeXAtlasFileName().substring(0, len)
		+ "_Vol_" + Integer.toString(volumeCounter) + AtlasProperties.getProperties().getLaTeXAtlasFileName().substring(len);
		volumeCounter++;
		return out;
	}
}
