/**
 * 
 */
package net.anc.atlas.index;

import java.util.Map;

import com.google.inject.Inject;


/**
 * Concrete factory for IndexComposer classes
 * 
 * @author Angelo Nicolini
 *
 */
public class ConcreteIndexFactory implements IndexComposerFactory<IndexComposer> {

	
	private Map<String, IndexComposerFactory<?>> factories;

	@Inject
	public ConcreteIndexFactory(Map<String, IndexComposerFactory<?>> factories) {
		this.factories = factories;
	}

	@Override
	public IndexComposer create(String name) {
		return factories.get(name).create("name");
	}

}
