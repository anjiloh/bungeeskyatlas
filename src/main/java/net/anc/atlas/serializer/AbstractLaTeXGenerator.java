package net.anc.atlas.serializer;

import jparsec.astrophysics.MeasureElement;
import jparsec.astrophysics.Table;
import jparsec.ephem.Functions;
import jparsec.graph.DataSet;
import jparsec.io.FileIO;
import jparsec.io.HTMLReport;
import jparsec.io.HTMLReport.SIZE;
import jparsec.io.LATEXReport;
import jparsec.util.JPARSECException;
import jparsec.vo.ADSElement;
import net.anc.atlas.AtlasGenerator;

/**
 * Default (and abstract) LaTeX generator.
 * The mean scope of this class is to avoid code duplication
 * 
 * This is directly derived from the {@link jparsec.io.LATEXReport} from T. Alonso Albi
 * 
 * @author Angelo Nicolini
 * @author T. Alonso Albi - OAN (Spain)
 *
 */
public abstract class AbstractLaTeXGenerator implements AtlasGenerator {



	private boolean fixFigures = false;
	private String textColor = COLOR_BLACK;
	private SIZE textSize = SIZE.NORMAL;
	private HTMLReport.STYLE textStyle = HTMLReport.STYLE.PLAIN;
	private String avoidFormatting = "\\{}~";

	protected boolean justify = false;

	protected boolean header = false;
	protected boolean document = true;
			
	protected StringBuffer latexCode = new StringBuffer(1000);
	protected StringBuffer bibTexCode = new StringBuffer(1000);
//
	protected static final String sep = FileIO.getLineSeparator();

	protected boolean insideTable = false;
	protected boolean tableFirstLine = false;

	
	public AbstractLaTeXGenerator() {
		setFixFiguresFlag(false);
	}
	
	/**
	 * Not so empty constructor...
	 * @param fixedFigures True to add adequate code to the figures to fix
	 * their positions in the document.
	 */
	public AbstractLaTeXGenerator(boolean fixFigures) {
		setFixFiguresFlag(fixFigures);
	}
	
	
	/**
	 * Selects to add or not relevant code to fix the positions of the figures in the output pdf.
	 * @param b True or false.
	 */
	public void setFixFiguresFlag(boolean b) {
		fixFigures = b;		
	}


	/**
	 * @return the fixFigures
	 */
	public boolean isFixFigures() {
		return fixFigures;
	}

	/**
	 * Begins the body. Does nothing, only for compatibility with HTML reports.
	 */
	public void beginBody() {}

	/**
	 * Ends the body tag. Does nothing, only for compatibility with HTML reports.
	 */
	public void endBody() {}

	
	/**
	 * Writes the header of the Latex file.
	 * @param title Title to be seen in the document.
	 */
	public abstract void writeHeader(String title);

	/**
	 * Begins a chapter.
	 * @param name Name.
	 */
	public void beginChapter(String name)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\chapter{");
		this.writeTextWithStyleAndColor(name, false);
		latexCode.append("}" + sep + sep);
	}
	
	protected void writeTextWithStyleAndColor(String text, boolean alsoSize)
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(getTextWithStyleAndColor(text, alsoSize));
	}
	
	/**
	 * Returns the LATEX code for a given text using the current style and color.
	 * @param text The text.
	 * @param alsoSize True to get also the code with the current text size.
	 * @return LATEX code.
	 */
	public String getTextWithStyleAndColor(String text, boolean alsoSize)
	{
		String code = this.getBeginOfCurrentStyle(alsoSize);
		if (!this.textColor.equals( AtlasGenerator.COLOR_BLACK)) {
			code += "\\textcolor{"+this.textColor+"}{";
		}
		code += formatSymbols(text);
		if (!this.textColor.equals(AtlasGenerator.COLOR_BLACK)) {
			code += "}";
		}
		code += this.getEndOfCurrentStyle(alsoSize);
		return code;
	}
	
	protected String getBeginOfCurrentStyle(boolean alsoSize)
	{
		String style = "";
		if (alsoSize) {
			switch (textSize) {
			case NORMAL:
				style = "{\\normalsize ";
				break;
			case LARGE:
				style = "{\\large ";
				break;
			case SMALL:
				style = "{\\small ";
				break;
			case VERY_LARGE:
				style = "{\\huge ";
				break;
			case VERY_SMALL:
				style = "{\\tiny ";
				break;
			}
		}

		switch (this.textStyle)
		{
		case PLAIN:
			break;
		case BOLD:
			style += "{\\textbf ";
			break;
		case ITALIC:
			style += "{\\textit ";
			break;
		case UNDERLINE:
			style += "{\\underline ";
			break;
		case UNDERLINE_BOLD:
			style += "{\\textbf{\\underline ";
			break;
		case UNDERLINE_ITALIC:
			style += "{\\textit{\\underline ";
			break;
		}
		return style;
	}
	
	protected String getEndOfCurrentStyle(boolean alsoSize)
	{
		String style = "";
		switch (this.textStyle)
		{
		case PLAIN:
			style = "";
			break;
		case BOLD:
			style = "}";
			break;
		case ITALIC:
			style = "}";
			break;
		case UNDERLINE:
			style = "}";
			break;
		case UNDERLINE_BOLD:
			style = "}}";
			break;
		case UNDERLINE_ITALIC:
			style = "}}";
			break;
		}
		if (alsoSize) return style + "}";
		return style;
	}

	/**
	 * Sets the color of the text to write.
	 * @param color Text color in hex format.
	 * Some constants defined in this class.
	 */
	public void setTextColor(String color)
	{
		this.textColor = color;
	}

	/**
	 * Replaces normal symbols by it's Latex representation. Currently only for &lt;, &gt;,
	 * &aacute;, &eacute;, &iacute;, &oacute;, &uacute;, +/-, {, }, _, ^, ~, \, $, #, %, &amp;, &deg;, &ntilde;. Any of them can be preserved in the
	 * input by adding the character to {@linkplain LATEXReport#setAvoidFormatting(String)}.
	 * Default symbols to avoid formatting are \{}~.
	 *
	 * @param text Input text.
	 * @return Output table.
	 */
	public String formatSymbols(String text)
	{
		if (text == null) return text;
		if (text.equals("")) return text;

		if (this.avoidFormatting.indexOf("\\")<0) text = DataSet.replaceAll(text, "\\", "*JPARSEC_BAR*", true);
		if (this.avoidFormatting.indexOf("$")<0) {
			text = DataSet.replaceAll(text, "$", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\$", true);
		}
		if (this.avoidFormatting.indexOf("\\")<0) text = DataSet.replaceAll(text, "*JPARSEC_BAR*", "$\\backslash$", true);
		if (this.avoidFormatting.indexOf('_')<0) {
			text = DataSet.replaceAll(text, "_", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\_", true);
		}
		if (this.avoidFormatting.indexOf('^')<0) {
			text = DataSet.replaceAll(text, "^", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\^", true);
		}
		if (this.avoidFormatting.indexOf('{')<0) {
			text = DataSet.replaceAll(text, "{", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\{", true);
		}
		if (this.avoidFormatting.indexOf('}')<0) {
			text = DataSet.replaceAll(text, "}", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\}", true);
		}
		if (this.avoidFormatting.indexOf('~')<0) {
			text = DataSet.replaceAll(text, "~", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\~", true);
		}
		if (this.avoidFormatting.indexOf('#')<0) {
			text = DataSet.replaceAll(text, "#", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\#", true);
		}
		if (this.avoidFormatting.indexOf('%')<0) {
			text = DataSet.replaceAll(text, "%", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\%", true);
		}
		if (this.avoidFormatting.indexOf('&')<0) {
			text = DataSet.replaceAll(text, "&", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "\\&", true);
		}
		if (this.avoidFormatting.indexOf('>')<0) {
			text = DataSet.replaceAll(text, ">", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "$>$", true);
		}
		if (this.avoidFormatting.indexOf('<')<0) {
			text = DataSet.replaceAll(text, "<", "*JPARSEC*", true);
			text = DataSet.replaceAll(text, "*JPARSEC*", "$<$", true);
		}
		if (this.avoidFormatting.indexOf("\u00e1")<0) text = DataSet.replaceAll(text, "\u00e1", "\\'{a}", true);
		if (this.avoidFormatting.indexOf("\u00e9")<0) text = DataSet.replaceAll(text, "\u00e9", "\\'{e}", true);
		if (this.avoidFormatting.indexOf("\u00ed")<0) text = DataSet.replaceAll(text, "\u00ed", "\\'{i}", true);
		if (this.avoidFormatting.indexOf("\u00f3")<0) text = DataSet.replaceAll(text, "\u00f3", "\\'{o}", true);
		if (this.avoidFormatting.indexOf("\u00fa")<0) text = DataSet.replaceAll(text, "\u00fa", "\\'{u}", true);
		if (this.avoidFormatting.indexOf("\u00f1")<0) text = DataSet.replaceAll(text, "\u00f1", "\\~n", true);
		if (this.avoidFormatting.indexOf("+/-")<0) text = DataSet.replaceAll(text, "+/-", "$\\pm$", true);
		if (this.avoidFormatting.indexOf("\u00b0")<0) text = DataSet.replaceAll(text, "\u00b0", "$^\\circ$", true);
		if (this.avoidFormatting.indexOf("\u00c1")<0) text = DataSet.replaceAll(text, "\u00c1", "\\'{A}", true);
		if (this.avoidFormatting.indexOf("\u00c9")<0) text = DataSet.replaceAll(text, "\u00c9", "\\'{E}", true);
		if (this.avoidFormatting.indexOf("\u00cd")<0) text = DataSet.replaceAll(text, "\u00cd", "\\'{I}", true);
		if (this.avoidFormatting.indexOf("\u00d3")<0) text = DataSet.replaceAll(text, "\u00d3", "\\'{O}", true);
		if (this.avoidFormatting.indexOf("\u00da")<0) text = DataSet.replaceAll(text, "\u00da", "\\'{U}", true);
		if (this.avoidFormatting.indexOf("\u00d1")<0) text = DataSet.replaceAll(text, "\u00d1", "\\~N", true);

		return text;
	}

	/**
	 * Sets the size of the text to write.
	 * @param size Text size.
	 */
	public void setTextSize(SIZE size)
	{
		this.textSize = size;
	}

	/**
	 * Sets the style of the text to write.
	 * @param style Text style.
	 */
	public void setTextStyle(HTMLReport.STYLE style)
	{
		this.textStyle = style;
	}

	/**
	 * Sets the set of characters to preserve as the user types then without
	 * reformating. See {@linkplain LATEXReport#formatSymbols(String)} for list
	 * of symbols. Default symbols to avoid formatting are \{}~.

	 * @param toAvoid Set of characters to preserve without reformatting.
	 */
	public void setAvoidFormatting(String toAvoid)
	{
		this.avoidFormatting = toAvoid;
	}

	/**
	 * Returns the characters that should not be formatted.
	 * @return Characters to preserve.
	 */
	public String getAvoidFormatting()
	{
		return this.avoidFormatting;
	}

	/**
	 * Sets the Latex code.
	 * @param code Code to be set.
	 */
	public void setCode(String code)
	{
		if( code == null || code.trim().length() == 0 ) {
			header = false;
			document = true;
			this.latexCode  = new StringBuffer();
		}
		else {
			this.latexCode = new StringBuffer(code);
		}
	}

	/**
	 * Ends the Latex document.
	 */
	public void endDocument() {
		if (!document) return;
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\end{document}" + sep);
		document = false;
	}

	/**
	 * Ends the Latex document adding the bibliography commands.
	 * @param bibCommands Commands for the bibliography, as given by
	 * {@linkplain LATEXReport#getBibItem(String, String, String, String)}.
	 */
	public void endDocumentWithBibliography(String[] bibCommands) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep);
		latexCode.append("\\begin{thebibliography}{"+bibCommands.length+"}" + sep);
		for (int i=0; i<bibCommands.length; i++)
		{
			latexCode.append(bibCommands[i] + sep);
		}
		latexCode.append("\\end{thebibliography}" + sep);
		latexCode.append(sep);
		this.endDocument();
	}

	/**
	 * Ends the Latex document adding the bibliography commands.
	 * @param style Style name. Can be null to use default A&amp;A.
	 * @param bibFileName Name of the bibliographic file.
	 */
	public void endDocumentWithBibliography(String style, String bibFileName) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep);
		latexCode.append("\\bibliographystyle{"+style+"}" + sep);
		latexCode.append("\\input{aas_macros.tex}" + sep);
		latexCode.append("\\bibliography{"+bibFileName+"}" + sep);
		latexCode.append(sep);
		this.endDocument();
	}

	/**
	 * Adds a bibtex entry to the document. Web access required to solve
	 * the bibtex entry.
	 * @param bibtex Bibtex object.
	 * @throws JPARSECException If an error occurs.
	 */
	public void addCite(ADSElement bibtex) throws JPARSECException {
		this.bibTexCode.append(bibtex.getBibTexEntry() + sep + sep);
	}

	/**
	 * Adds a bib entry to the document.
	 * @param key The key.
	 * @param citep True for a citep command.
	 */
	public void addCite(String key, boolean citep) {
		String addP = "";
		if (citep) addP = "p";
		this.bibTexCode.append("~\\cite"+addP+"{"+key+"}" + sep);
	}

	/**
	 * Returns the bibtex code with all added bibtex entries.
	 * A file should be created using the same name stablished
	 * in the bibliography command.
	 * @return The set of bibtex references.
	 */
	public String getBibTexCode() {
		return this.bibTexCode.toString();
	}

	/**
	 * Begins a center tag.
	 */
	public void beginCenter() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\begin{center}" + sep);
	}

	/**
	 * Ends a center tag.
	 */
	public void endCenter() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\end{center}" + sep);
	}

	/**
	 * Begins a right justify tag (for text).
	 */
	public void beginRightJustify()
	{
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\begin{flushright}" + sep);
	}

	/**
	 * Ends a right justify tag (for text).
	 */
	public void endRightJustify() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\end{flushright}" + sep);
	}

	/**
	 * Begins a left justify tag (for text).
	 */
	public void beginLeftJustify() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\begin{flushleft}" + sep);
	}

	/**
	 * Ends a left justify tag (for text).
	 */
	public void endLeftJustify() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\end{flushleft}" + sep);
	}

	/**
	 * Begins a quotation (for text).
	 */
	public void beginQuotation() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\begin{quotation}" + sep);
	}

	/**
	 * Ends a quotation (for text).
	 */
	public void endQuotation() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\end{quotation}" + sep);
	}

	/**
	 * Begins a quote (for text).
	 */
	public void beginQuote() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\begin{quote}" + sep);
	}

	/**
	 * Ends a quote (for text).
	 */
	public void endQuote() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\end{quote}" + sep);
	}

	/**
	 * Begins a justify (for text).
	 */
	public void beginJustify() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\begin{justify}" + sep);
		justify = true;
	}

	/**
	 * Ends a justify (for text).
	 */
	public void endJustify() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\end{justify}" + sep);
		justify = false;
	}

	/**
	 * To begin a superindex.
	 */
	public void beginSuperIndex() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("$^{");
	}

	/**
	 * To end a superindex.
	 *
	 */
	public void endSuperIndex() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("}$");
	}

	/**
	 * To begin a subindex.
	 */
	public void beginSubIndex() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("$_{");
	}

	/**
	 * To end a subindex.
	 *
	 */
	public void endSubIndex() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("}$");
	}

	/**
	 * Writes an (indeed) very big title.
	 * @param title Text of the title.
	 */
	public void writeAVeryBigMainTitle(String title) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{Huge}");
		this.writeTextWithStyleAndColor(title, false);
		latexCode.append("\\end{Huge}" + sep + sep);
	}

	/**
	 * Writes a big title.
	 * @param title Text of the title.
	 */
	public void writeMainTitle(String title) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{LARGE}");
		this.writeTextWithStyleAndColor(title, false);
		latexCode.append("\\end{LARGE}" + sep + sep);
	}

	/**
	 * Writes a normal title.
	 * @param title Text of the title.
	 */
	public void writeTitle(String title) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{large}");
		this.writeTextWithStyleAndColor(title, false);
		latexCode.append("\\end{large}" + sep + sep);
	}

	/**
	 * Sets the line spacing in units of the default line break (1).
	 * @param sp A value, 2 for double space, and so on.
	 */
	public void setLineSpacing(double sp) {
		latexCode.append(sep + "\\linespread{"+Functions.formatValue(sp, 3)+"}" + sep + sep);
	}

	/**
	 * Writes a footnote. Not compatible with HTML.
	 * @param text Text to write.
	 */
	public void writeFootNote(String text) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\footnote{");
		this.writeTextWithStyleAndColor(text, false);
		latexCode.append("}" + sep);
	}

	/**
	 * Foces a new page. Not compatible with HTML.
	 */
	@Override
	public void forceNewPage() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep);
		latexCode.append("\\newpage" + sep);
		latexCode.append(sep);
	}

	/**
	 * Flush content in memory (clearpage). Not compatible with HTML.
	 */
	public void flush() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep);
		latexCode.append("\\clearpage" + sep);
		latexCode.append(sep);
	}

	/**
	 * Writes a comment, not visible in the resulting document.
	 * @param text Text to write.
	 */
	public void writeComment(String text) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("% " + text + sep);
	}

	/**
	 * Writes text in a small font.
	 * @param text Text to write.
	 */
	public void writeSmallTextLine(String text) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{small}");
		this.writeTextWithStyleAndColor(text, false);
		latexCode.append("\\end{small}" + sep + sep);
	}

	/**
	 * Writes a list of items with numbers. Not compatible with HTML.
	 * @param lines Lines to be formatted as items.
	 */
	@Override
	public void writeListUsingNumbers(String[] lines) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{enumerate}" + sep);
		for (int i=0; i<lines.length; i++)
		{
			latexCode.append("\\item ");
			this.writeTextWithStyleAndColor(lines[i], true);
			latexCode.append(sep);
		}
		latexCode.append("\\end{enumerate}" + sep + sep);
	}

	/**
	 * Writes raw text without any command.
	 * @param text Text to write.
	 */
	public void writeRawText(String text) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(text);
	}

	/**
	 * Creates a link in HTML format.
	 * @param href Link destination.
	 * @param highlighted Text to be highlighted.
	 * @return Code for the link.
	 */
	public String writeHTMLLink(String href, String highlighted) {
		String code = "\\htmladdnormallink{"+highlighted+"}{"+href+"}";
		return code;
	}

	/**
	 * Writes a label. Not compatible with HTML.
	 * @param label The label.
	 */
	public void writeLabel(String label) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		if (label != null) latexCode.append("\\label{"+label+"}" + sep);
	}

	/**
	 * Ends a long table tag adding a label.
	 * @param label The label. Set to null to avoid it.
	 */
	public void endLongTable(String label) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\hline\\noalign{\\smallskip}" + sep);
		if (label != null) latexCode.append("\\label{"+label+"}" + sep);
		latexCode.append("\\end{longtable}" + sep + sep);
		insideTable = false;
		tableFirstLine = false;
	}

	/**
	 * Writes an entire table object, row by row. Header should be written previously. The table
	 * is written using parenthesis for the error values, and without units. Parameters like background
	 * color and align are ignored.
	 * @param table The table object.
	 * @throws JPARSECException In case the number of dimensions in the Table is more than 2.
	 */
	public void writeRowsInTable(Table table) throws JPARSECException {
		if (table.getDimensions() > 2) throw new JPARSECException("The table must be 1 or 2 dimensions.");
		boolean useParentheses = true, includeUnit = false;
		for (int i=0; i<table.getNrows(); i++) {
			writeRowInTable(MeasureElement.toString(table.getRowValues(0, i), useParentheses, includeUnit), null, null, null, false);
		}
	}

	/**
	 * Write a row in a table including a possible title (tooltip text) that appears when
	 * the mouse is on the text item.<P>
	 *
	 * Align commands are right, left, center.<P>
	 *
	 * Column span is the number of columns to be agruped by a given text. If a table has 6 columns
	 * (length of columns array), then a column span of 6 alows to write text ocuping the whole row,
	 * as a title for example. In this case the columns array should have length 1 to write only that
	 * title.
	 *
	 * @param columns Columns to be written to complete the row.
	 * @param title Title for the tooltip in each column item.
	 * @param bgcolor Background color. Ignored, only for compatibility with HTML report.
	 * @param align Align command. Set to null to use default left align.
	 * @param colspan Column span command. Set to null to skip the command.
	 * @param format True to format text with the current style and color.
	 */
	public void writeRowInTable(String columns[], String title[], String bgcolor, String align, String colspan, boolean format) {
		for (int i=0; i<columns.length; i++)
		{
			columns[i] = "\\href{"+title[i]+"}{\\nolinkurl{"+columns[i]+"}}";
		}
	
		this.writeRowInTable(columns, bgcolor, align, colspan, format);
	}

	
	abstract  public void writeRowInTable(String columns[], String bgcolor, String align, String colspan);
	abstract  public void writeRowInTable(String columns[], String bgcolor, String align, String colspan, boolean formaat);
	
	/**
	 * Write a row in a table including a possible title (tooltip text) that appears when
	 * the mouse is on the text item.<P>
	 *
	 * Align commands are right, left, center.<P>
	 *
	 * Column span is the number of columns to be agruped by a given text. If a table has 6 columns
	 * (length of columns array), then a column span of 6 alows to write text ocuping the whole row,
	 * as a title for example. In this case the columns array should have length 1 to write only that
	 * title.
	 *
	 * @param columns Columns to be written to complete the row.
	 * @param title Title for the tooltip in each column item.
	 * @param bgcolor Background color. Ignored, only for compatibility with HTML report.
	 * @param align Align command. Set to null to use default left align.
	 * @param colspan Column span command. Set to null to skip the command.
	 */
	public void writeRowInTable(String columns[], String title[], String bgcolor, String align, String colspan) {
		for (int i=0; i<columns.length; i++)
		{
			columns[i] = "\\href{"+title[i]+"}{\\nolinkurl{"+columns[i]+"}}";
		}
	
		this.writeRowInTable(columns, bgcolor, align, colspan, true);
	}

	/**
	 * Writes a blank paragraph, like a big skip (2 cm).
	 */
	public void writeBigSkip() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append("\\vspace{2cm}" + sep);
	}

	/**
	 * Writes a blank line, like a small skip (0.5 cm).
	 */
	public void writeSmallSkip() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\vspace{0.5cm}" + sep + sep);
	}

	/**
	 * Writes a blank line, like a small skip.
	 * @param cm The amount of space in cm. 2cm is a big skip, 0.5 a small skip.
	 */
	public void writeVerticalSkip(double cm) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\vspace{"+Functions.formatValue(cm, 3)+"cm}" + sep + sep);
	}

	/**
	 * Writes an space in the code, to separate code sections. Not visible in the browser.
	 */
	public void writeSpace() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep);
	}

	/**
	 * Writes an horizontal line.
	 */
	public void writeHorizontalLine() {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		if (!insideTable) {
			latexCode.append(sep + "\\begin{table}[h]" + sep);
			latexCode.append("\\begin{tabular}[c]{ccc}");
		}
		latexCode.append("\\hline\\noalign{\\smallskip} ");
		if (!insideTable) {
			latexCode.append("\\\\" + sep);
			latexCode.append("\\end{tabular}");
			latexCode.append("\\end{table}" + sep + sep);
		} else {
			latexCode.append(sep);
		}
	}

	/**
	 * Writes the code for an image tag. Not compatible with HTML.
	 * @param width Width.
	 * @param height Height.
	 * @param align Align.
	 * @param src Link to the image.
	 * @param caption Caption for the image. Can be null.
	 * @param label Label as an id for the image. Can be null.
	 */
	public void writeImageWithCaption(String width, String height, String align, String src, String caption, String label) {
		this.writeImageWithCaption(width, height, null, align, src, caption, label);
	}

	public abstract void writeImageWithCaption(String width, String height, String angle, String align, String src, String caption, String label);
	
//	/**
//	 * Writes the code for a set of images. Not compatible with HTML.
//	 * @param width Width. Can be null.
//	 * @param height Height. Can be null.
//	 * @param angle Rotation angle of the image in degrees, or null.
//	 * @param align Align.
//	 * @param src Links to the images.
//	 * @param caption Caption for the image. Can be null.
//	 * @param label Label as an id for the image. Can be null.
//	 */
//	public void writeImagesWithCaption(String width, String height, String angle, String align, String[] src, String caption,
//			String label) {
//				if (!header) try { writeHeader(""); } catch (Exception exc) {}
//				String code = this.beginAlignment(align);
//				if (width != null) {
//					if (width.endsWith("%")) {
//						double scale = Double.parseDouble(width.substring(0, width.length() - 1)) / 100.0;
//						width = "" + Functions.formatValue(scale, 5) + "\\textwidth";
//					} else {
//						width += "pt";
//					}
//				}
//				if (height != null) {
//					if (height.endsWith("%")) {
//						double scale = Double.parseDouble(height.substring(0, height.length() - 1)) / 100.0;
//						height = "" + Functions.formatValue(scale, 5) + "\\textheight";
//					} else {
//						height += "pt";
//					}
//				}
//			
//				String fix = "";
//				if (isFixFigures()) fix = "[!ht]";
//				code += sep + "\\begin{figure}" + fix + sep;
//				for (int i=0;i<src.length;i++) {
//					code += "\\includegraphics[";
//					if (angle != null) {
//						code += "angle="+angle;
//						if (width != null || height != null) code += ", ";
//					}
//					if (width != null) {
//						code +="width="+width;
//						if (height != null) code+=", ";
//					}
//					if (height != null) code += "height="+height;
//					code +="]{"+src[i]+"}"+sep;
//				}
//			
//				if (caption != null) code += "\\smallskip"+sep;
//				if (caption != null) {
//					code += "\\caption{";
//					code += this.getTextWithStyleAndColor(caption, false);
//					code += "}"+sep;
//				}
//				if (label != null && !label.equals("")) code += "\\label{"+label+"}"+sep;
//				code += "\\end{figure}"+sep + sep;
//				code += this.endAlignment(align);
//			
//				this.latexCode.append(code);
//			}
//
	/**
	 * Returns the Latex code. The document is NOT closed
	 * automatically in case it is still opened.
	 * @return Latex code.
	 */
	public String getCode() {
		return this.latexCode.toString();
	}

	/**
	 * Creates a link.
	 * @param href Link destination.
	 * @param highlighted Text to be highlighted.
	 * @return Code for the link.
	 */
	public String writeLink(String href, String highlighted) {
		String code = "\\href{"+href+"}{"+highlighted+"}";
		return code;
	}

	/**
	 * Writes the header of a table.
	 * @param border Border style. Ignored, only for compatibility with HTML report.
	 * @param cellspacing Cell spacing. Ignored, only for compatibility with HTML report.
	 * @param cellpadding Cell padding. Ignored, only for compatibility with HTML report.
	 * @param width Width. Ignored, only for compatibility with HTML report.
	 */
	public void writeTableHeader(int border, int cellspacing, int cellpadding, String width) {
		if (!header) try { writeHeader(""); } catch (Exception exc) {}
		latexCode.append(sep + "\\begin{table}[h]" + sep);
		latexCode.append("\\begin{tabular*}{1.0\\textwidth}");
		insideTable = true;
		tableFirstLine = false;
	}
	
	protected String beginAlignment(String align)
	{
		if (align == null) align = "left";
		if (align.toLowerCase().equals("left")) align = "flushleft";
		if (align.toLowerCase().equals("right")) align = "flushright";

		String code = "\\begin{"+align.toLowerCase()+"}" + sep;
		return code;
	}
	
	protected String endAlignment(String align)
	{
		if (align == null) align = "left";
		if (align.toLowerCase().equals("left")) align = "flushleft";
		if (align.toLowerCase().equals("right")) align = "flushright";

		String code = "\\end{"+align.toLowerCase()+"}" + sep;
		return code;
	}
	
	
}
