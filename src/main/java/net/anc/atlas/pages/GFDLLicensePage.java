/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;

import jparsec.io.FileIO;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;

/**
 * @author Angelo Nicolini
 *
 */
public class GFDLLicensePage extends LicensePage {
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {
		StringBuilder license = new StringBuilder();
		
		license.append(Messages.getString("License.content"));
		license.append(" {\\textcopyright} 2018 Angelo Nicolini \\textless angelo.nicolini+atlas@gmail.com\\textgreater\\hfill \\break");
		license.append(FileIO.getLineSeparator());

		license.append(
				"Permission  is  granted  to  copy,  distribute  and/or  modify  this  document  under  the  terms  of  the  GNU  Free  Documentation  License  (GFDL), ");
		license.append(
				"Version 1.3 or any later version published by the Free Software Foundation with no Invariant Sections, no Front-Cover Texts, and no Back- ");
		license.append("Cover Texts. You can find a copy of the GFDL at this link \\url{https://www.gnu.org/licenses/fdl.html}.\\par ");
		license.append(
				"Many of the names used by companies to distinguish their products and services are claimed as trademarks. Where those names appear and the author is aware of those trademarks, then the names ");
		license.append("are in capital letters or initial capital letters.\\par ");
		license.append(
				"DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED UNDER THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE WITH THE FURTHER UNDERSTANDING THAT:\\par ");
		license.append(FileIO.getLineSeparator());
		license.append("\\begin{enumerate} ").append(FileIO.getLineSeparator());
		license.append("\\item ").append(FileIO.getLineSeparator());
		license.append(
				"DOCUMENT IS PROVIDED ON AN «AS IS» BASIS, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, ");
		license.append(
				"INCLUDING, WITHOUT LIMITATION, WARRANTIES THAT THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT ");
		license.append(
				"IS FREE OF DEFECTS MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE RISK AS ");
		license.append(
				"TO THE QUALITY, ACCURACY, AND PERFORMANCE OF THE DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT ");
		license.append(
				"IS WITH YOU. SHOULD ANY DOCUMENT OR MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT THE ");
		license.append(
				"INITIAL WRITER, AUTHOR OR ANY CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY SERVICING, REPAIR OR ");
		license.append(
				"CORRECTION.  THIS  DISCLAIMER  OF  WARRANTY  CONSTITUTES  AN  ESSENTIAL  PART  OF  THIS  LICENSE.  NO  USE  OF ");
		license.append(
				"ANY DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER; AND ");
		license.append("\\item ").append(FileIO.getLineSeparator());
		license.append(
				"UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE), CONTRACT,  ");
		license.append("OR OTHERWISE, SHALL THE AUTHOR, INITIAL WRITER, ANY CONTRIBUTOR, OR ANY DISTRIBUTOR OF THE ");
		license.append(
				"DOCUMENT OR MODIFIED VERSION OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH PARTIES, BE LIABLE TO ");
		license.append(
				"ANY PERSON FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER ");
		license.append("INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE ");
		license.append(
				"OR MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR LOSSES ARISING OUT OF OR RELATING TO USE OF THE ");
		license.append(
				"DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT, EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF ");
		license.append("THE POSSIBILITY OF SUCH DAMAGES. ");
		license.append("\\end{enumerate} ").append(FileIO.getLineSeparator());

		
		generator.writeParagraph(license.toString());
		generator.endFrame();

	}
}
