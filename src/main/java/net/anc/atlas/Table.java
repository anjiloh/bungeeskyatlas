/**
 * 
 */
package net.anc.atlas;

/**
 * Generic page containing a table
 * @author Angelo Nicolini
 *
 */
public interface Table extends Page {
	
	/**
	 * 
	 * @param caption
	 */
	void setHeader(String caption, String[] header);

}
