/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;
import java.util.logging.Logger;

import com.google.inject.Inject;

import jparsec.io.FileIO;
import net.anc.atlas.A3;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.EmisphereMinimap;
import net.anc.atlas.EmisphereMinimap.Entry;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;

/**
 * This compiles the LaTeX page for minimaps, both North and South emisphere
 * 
 * @author Angelo Nicolini
 *
 */
public class MinimapsChartsPage implements Page {

	@Inject
	private AtlasGenerator generator;
	private String filename;
	
	private final String centerX;
	private final String centerY;
	
	private static final Logger LOGGER = Logger.getLogger( Page.class.getName() );

	
	public MinimapsChartsPage() {
		if( AtlasProperties.getProperties().getPaperSize() instanceof A3 ) {
			centerX = "20.5";
			if( AtlasProperties.getProperties().isReflector() ) {
				centerY = "13.9";
			}
			else {
				centerY = "14.2";
			}
		}
		else {
			centerX = "14.5";
			if( AtlasProperties.getProperties().isReflector() ) {
				centerY = "9.75";
			}
			else {
				centerY = "10.02";
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		generator.beginSection(Messages.getString(title[0]));
		
		filename = title[1];
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {

		LOGGER.fine("Start minimap generation");
		
		generator.writeRawText("\\newgeometry{left=0.4cm, right=0.0cm, top=0.2cm, bottom=0.2cm}");
		generator.writeRawText(FileIO.getLineSeparator()) ;

		generator.writeRawText("\\begin{tikzpicture}%");
		generator.writeRawText(FileIO.getLineSeparator()) ;
		StringBuilder code = new StringBuilder();
		
		// TODO: fix the followng line for A4
		code.append("\\node[anchor=south west,inner sep=0] (image) at (0,0) {\\includegraphics[width=0.98000\\textwidth, height=0.95727\\textheight]{");
		code.append(filename);
		code.append("}   };").append(FileIO.getLineSeparator()) ;
		
		for (Content coordinates : content) {
			EmisphereMinimap.Entry entry = (Entry) coordinates;
			code.append("\\node[xshift=").append(centerX).append("cm, yshift=").append(centerY).append("cm] (a) at (");
			code.append(entry.theta).append(':').append(entry.ro);
			code.append(") {\\mmcircled{\\ref{subsec:").append(entry.chartId).append("}}};");
			code.append(FileIO.getLineSeparator()) ;
			
		}
		generator.writeRawText(code.toString());
		
		generator.writeRawText("\\end{tikzpicture}%" );
		generator.writeRawText(FileIO.getLineSeparator()) ;
		generator.writeRawText("\\restoregeometry") ;
		generator.writeRawText(FileIO.getLineSeparator()) ;
		
		LOGGER.fine("Minimap generation completed");
	}

}
