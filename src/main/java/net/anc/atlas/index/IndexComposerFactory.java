/**
 * 
 */
package net.anc.atlas.index;

import com.google.inject.assistedinject.Assisted;

/**
 * @author Angelo Nicolini
 *
 */
public interface IndexComposerFactory<T extends IndexComposer> {
	T create(@Assisted String name);
}

interface StarConverterFactory extends IndexComposerFactory<Star> {}
interface DSOConverterFactory extends IndexComposerFactory<DSO> {}

interface ConstellationsBuilderFactory extends IndexComposerFactory<ConstellationsIndexBuilder> {}
interface DSOBuilderFactory extends IndexComposerFactory<ObjectsIndexBuilder> {}
interface StarsBuilderFactory extends IndexComposerFactory<StarsIndexBuilder> {}
interface NovaeBuilderFactory extends IndexComposerFactory<NovaeIndexBuilder> {}

interface DSOEntryBuilderFactory extends IndexComposerFactory<ObjectsIndexEntryBuilder> {}
interface StarEntryBuilderFactory extends IndexComposerFactory<StarsIndexEntryBuilder> {}
interface NovaeEntryBuilderFactory extends IndexComposerFactory<NovaeIndexEntryBuilder> {}

