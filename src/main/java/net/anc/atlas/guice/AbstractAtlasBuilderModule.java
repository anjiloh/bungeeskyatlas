/**
 * 
 */
package net.anc.atlas.guice;

import com.google.inject.AbstractModule;

import net.anc.atlas.Atlas;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.DefaultAtlas;
import net.anc.atlas.MultiVolumeAtlas;
import net.anc.atlas.serializer.ExportChart;
import net.anc.atlas.serializer.ExportChartToPDFUsingPDFBox;
import net.anc.atlas.serializer.GeneratorFormat;
import net.anc.atlas.serializer.LaTeXBeamerGenerator;
import net.anc.atlas.serializer.LaTeXBookGenerator;


/**
 * Base Guice Configurator. 
 * 
 * @author Angelo Nicolini
 *
 */
public class AbstractAtlasBuilderModule extends AbstractModule {

	/* (non-Javadoc)
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {

		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BOOK ) {
			bind(AtlasGenerator.class).to(LaTeXBookGenerator.class);
		}
		else {
			bind(AtlasGenerator.class).to(LaTeXBeamerGenerator.class);
		}
//		bind(AtlasGenerator.class).toProvider(AtlasGeneratorProvider.class);
		
//		bind(ExportChart.class).to(ExportChartToPDFUsingIText.class);

		bind(ExportChart.class).to(ExportChartToPDFUsingPDFBox.class);

		
		if( AtlasProperties.getProperties().isMultiVolume() ) {
			bind(Atlas.class).to(MultiVolumeAtlas.class);
		}
		else {
			bind(Atlas.class).to(DefaultAtlas.class);
		}
	}

}
