/**
 * 
 */
package net.anc.atlas.pages;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import jparsec.io.FileIO;
import jparsec.util.Translate;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;

/**
 * @author nicolinia
 *
 */
public class CatalogLegendPage extends LegendPage {
	
	@Override
	public void writeContent(List<Content> content) {
		
//		generator.beginSection(string);
		
		
		generator.beginSubSection(Messages.getString("Legend.charts.title"));

		if( AtlasProperties.getProperties().isShowLegend() ) {
			generator.writeParagraph(Messages.getString("Legend.charts.content.legend.in.chart"));
		}
		else {
			generator.writeParagraph(Messages.getString("Legend.charts.content.legend.not.in.chart"));
		}
		
		try {
			String legendImgName = 
					"/legend"  
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			generator.writeImageWithCaption("100%", null,null, Paths.get(this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/"), Messages.getString("Legend.charts.img.caption"), "fig:legend");
	
			generator.writeRawText(Messages.getString("Legend.charts.content.1") + FileIO.getLineSeparator());
	
			StringBuilder text = new StringBuilder();
	
	
			text.append("\\begin{itemize}"+ FileIO.getLineSeparator());
			text.append("\\item "+ FileIO.getLineSeparator());
			text.append('{').append(Messages.getString("Legend.charts.stars.mag")).append(" ~\\ref{fig:legend_stars}}"+ FileIO.getLineSeparator());
	
			text.append("\\begin{figure}[!ht]"+ FileIO.getLineSeparator());
			legendImgName = "/legend_stars" 
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			text.append("\\includegraphics[width=1.0000\\textwidth]{").append(
					Paths.get( this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/") )
				.append('}').append( FileIO.getLineSeparator() );
			text.append("\\caption{").append(Messages.getString("Legend.charts.stars.mag.caption")).append( "}\\label{fig:legend_stars}"+ FileIO.getLineSeparator());
			text.append("\\end{figure}"+ FileIO.getLineSeparator()); 
			
			text.append("\\item "+ FileIO.getLineSeparator());
			text.append('{').append(  Messages.getString("Legend.charts.stars.types.1")).append("~\\ref{fig:legend_types}");
			text.append(Messages.getString("Legend.charts.stars.types.2")).append('}').append( FileIO.getLineSeparator())  ;
			text.append("\\begin{figure}[!ht]"+ FileIO.getLineSeparator());

			legendImgName = "/legend_types" 
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  					
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			text.append("\\includegraphics[width=0.1500\\textwidth]{").append( 
					Paths.get( this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/") )
				.append('}').append( FileIO.getLineSeparator());
			text.append("\\caption{" +  Messages.getString("Legend.charts.stars.types.caption") +"}\\label{fig:legend_types}"+ FileIO.getLineSeparator());
			text.append("\\end{figure}"+ FileIO.getLineSeparator());
	
			text.append("\\item "+ FileIO.getLineSeparator());
			text.append('{').append( Messages.getString("Legend.charts.dso") +"~\\ref{fig:legend_dso}}").append( FileIO.getLineSeparator());
			text.append("\\begin{figure}[!ht]"+ FileIO.getLineSeparator());
			
			legendImgName = "/legend_dso" 
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			text.append("\\includegraphics[width=1.0000\\textwidth]{").append( 
					Paths.get(this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/") )
					.append('}').append( FileIO.getLineSeparator());
			text.append("\\caption{" +  Messages.getString("Legend.charts.dso.caption") +"}\\label{fig:legend_dso}"+ FileIO.getLineSeparator());
			text.append("\\end{figure}"+ FileIO.getLineSeparator());
	
			text.append("\\end{itemize}	"+ FileIO.getLineSeparator());

			generator.writeRawText(text.toString());

			
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("cannot write legend", e);
		}


	}


}
