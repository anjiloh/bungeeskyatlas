/**
 * 
 */
package net.anc.atlas.index;

import java.util.Arrays;
import java.util.List;

import com.google.inject.Inject;

import jparsec.ephem.Functions;
import jparsec.graph.DataSet;
import jparsec.util.Translate;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;
import net.anc.atlas.pages.ObjectsByChartIndex;

/**
 * @author Angelo Nicolini
 *
 */
public class StarsByChartIndex extends ObjectsByChartIndex {
	
	private static String[] columnsName = {
			Translate.translate("Name"), 
			Messages.getString("Stars.chart.intex.column.other.name"), 
			Translate.translate("Right ascension"), // Translate.translate("Right ascension") +  " (J" + AtlasProperties.getProperties().getYear() + ".0)",
			Translate.translate("Declination"),
			Translate.translate("dRA") + "(rad/yr)", 
			Translate.translate("dDEC")+ "(rad/yr)", 
			"\\makecell[l]{" +  Translate.translate("dVr") + "\\\\(km/s)" + "}", 
			"Mag.", //Translate.translate("Magnitude"), 
			"\\makecell[l]{" +  Translate.translate("Spectral type").replace(" ", "\\\\") + "}",
			"Dist. (pc)", //Translate.translate("Distance") + "(pc)", 
			Translate.translate("Variable"),
			Translate.translate("Constellation") //, 
	};

	
	@Inject
	private AtlasGenerator generator;

	
	
	protected String getColumnsAlign(int headerLen) {
//		return String.format("*{%d}{p{0.08\\textwidth}}", headerLen);
		return "*{6}{p{0.08\\textwidth}}llllll";
	}
	
	@Override
	protected void writeContents(List<Content> data, int from, int to) {
		for (int i = from ; i < to; i++) {
			AstroItemDetail next = (AstroItemDetail)data.get(i);
			
			String name = next.getName();
//			if( name.contains("(")) {
//				name = "\\makecell[l]{ " + name.substring(0, name.indexOf('(') -1 ) + "\\\\" + name.substring(name.indexOf('(')) + "}"; 
//			}
			
			String dec = DataSet.replaceOne(Functions.formatDEC(next.getLatitude(), 0), "\"", "$\"$", 1);
			if( !dec.startsWith("-")) {
				dec = "+" + dec;
			}
			String magnitude =  Functions.formatValue(next.getMagnitude(), 1);
			if ("0.0".equals(magnitude)) {
				magnitude = "-";
			}
			String angle =Functions.formatValue(Double.parseDouble(next.getAngle()), 2) ;
			String type = next.getType();
			if( type == null) {
				type = "";
			}
			
			String[] inExtraInfo = next.getExtraInfo().split(";", -1);
			StringBuilder extraInfo = new StringBuilder("\\makecell[l]{");
			String spectralType = "-";
			String dRA = "-";
			String dDEC = "-";
			String dVR = "-";
			String variable = "-";
			
			for( int j = 0; j < inExtraInfo.length; ++j) {
				if( inExtraInfo[j].contains( Translate.translate("Spectral type") ) ){
					spectralType = inExtraInfo[j].split(":")[1];
				}
				else if( inExtraInfo[j].contains( Translate.translate("dRA") ) ){
					dRA = inExtraInfo[j].split(":")[1].replaceAll("[(].*?[)]", "");
					dRA = String.format("%6.3e", Double.valueOf(dRA));
				}
				else if( inExtraInfo[j].contains( Translate.translate("dDEC") ) ){
					dDEC = inExtraInfo[j].split(":")[1].replaceAll("[(].*?[)]", "");
					dDEC = String.format("%6.3e", Double.valueOf(dDEC));
				}
				else if( inExtraInfo[j].contains( Translate.translate("dVr") ) ){
					dVR = inExtraInfo[j].split(":")[1].replaceAll("[(].*?[)]", "");
				}
				else if( inExtraInfo[j].contains( Translate.translate("Parallax") ) ){
					// ignore
				}
				else if( inExtraInfo[j].contains( Translate.translate("Variable Type") ) ){
					//Pulsating (Bet Cep (Bet CMa))
//					variable = Translate.translate(inExtraInfo[j].split(":")[1].replaceAll("[(].*?[)]", ""));
					String fullVariableType = inExtraInfo[j].split(":")[1];
					if( fullVariableType.contains("(")) {
						variable = "\\makecell[l]{" +  Translate.translate(fullVariableType.substring(0, fullVariableType.indexOf('(')-1)) + "\\\\";
						variable += fullVariableType.substring(fullVariableType.indexOf('(')) + "}";
					}
					else {
						variable = Translate.translate(fullVariableType);
					}
					
					
					// ignore
				}
				else {
					extraInfo.append(inExtraInfo[j]).append("\\\\");
				}
			}
			extraInfo.append('}');
			
			StringBuilder otherNames = new StringBuilder();
			if( next.getOtherNames() != null && next.getOtherNames().contains(";")) {
				otherNames.append("\\makecell[l]{");
				String[] names = next.getOtherNames().split(";");
				for (String extraName : names) {
					otherNames.append(extraName).append("\\\\");
				}
				otherNames.reverse().delete(0, 2);
				otherNames.append('}');
			}
			else {
				otherNames.append(next.getOtherNames());
			}
			
			String[] datai = new String[] {
					name,
					otherNames.toString(),
					Functions.formatRA(next.getLongitude(), 1),  
					dec,
					dRA,
					dDEC,
					dVR,
					magnitude,
					spectralType,
					angle,
					variable,
					next.getConstellation(),
//					extraInfo.toString()
			};
			generator.writeRowInTable(datai, null, null, null);
		}
	}
	
	
	@Override
	public String[] getColumnsName() {
		return Arrays.copyOf(columnsName, columnsName.length);
	}

	@Override
	public String getId() {
		return "Stars.Index.title";
	}
	

}
