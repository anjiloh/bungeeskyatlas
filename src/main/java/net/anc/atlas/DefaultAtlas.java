/**
 * 
 */
package net.anc.atlas;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import jparsec.graph.DataSet;
import jparsec.io.FileIO;
import jparsec.util.Translate;
import net.anc.atlas.index.NovaeAndSupernovaeIndex;
import net.anc.atlas.pages.ChartPage;
import net.anc.atlas.pages.ConstellationsByChartIndex;
import net.anc.atlas.pages.LegendDataContent;
import net.anc.atlas.pages.PageFactory;

/**
 * @author Angelo Nicolini
 *
 */
public class DefaultAtlas implements Atlas {

	private static final Character DEG = Character.valueOf((char)176);
	
	protected String date = Messages.getString("Bungee.date");

	private String author = "Angelo Nicolini";
	private String institute = null; 

	
	@Inject
	protected AtlasGenerator generator;

	@Inject
	protected PageFactory<Page> pageFactory;
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writeHeaderPage()
	 */
	@Override
	public void writeHeaderPage() {

		Page headerPage = pageFactory.create("header");
		headerPage.setTitle("header.page");
		headerPage.writeContent(null);
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writePresentationPage()
	 */
	@Override
	public void writePresentationPage() {
		Page presentationPage = pageFactory.create("presentation");
		presentationPage.setTitle("presentation.page");
		presentationPage.writeContent(null);
		
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writeCreditsPage()
	 */
	@Override
	public void writeCreditsPage() {
		Page credits = pageFactory.create("credits");
		credits.setTitle("credits.page");
		List <Content> listOfCredits = new ArrayList<Content>();
		int counter = 1;
		do {
			String msg = Messages.getString("CreditsPage."+counter);
			if( ("!CreditsPage." + counter + "!").equals(msg)) {
				break;
			}
			else {
				listOfCredits.add(new MessageContent(msg));
				counter ++;
			}
		}
		while( true );

		
		credits.writeContent(listOfCredits);
		
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writeLegendPage()
	 */
	@Override
	public void writeLegendPage() {
		
		Table headerPage = (Table) pageFactory.create("generationOptions");
		
		headerPage.setTitle("generation.options.title");
		headerPage.setHeader(
				Messages.getString("generation.options.data"), 
				new String[] {Messages.getString("generation.options.head.1"), Messages.getString("generation.options.head.2")}
			);

		final List<Content> content = new ArrayList<Content>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4133291250974381339L;

			{
				add( new LegendDataContent( Messages.getString("generation.options.paper.size"),AtlasProperties.getProperties().getPaperSize().getName().toUpperCase())  );
				add( new LegendDataContent( Messages.getString("generation.options.paper.ppi"), Integer.toString(AtlasProperties.getProperties().getPixelPerInch()))  );

				add( new LegendDataContent( Translate.translate("Field of view (" + DEG + ")"), AtlasProperties.getProperties().getScale().getFieldSize())  );
				add( new LegendDataContent( Translate.translate("Stellar magnitudes"), Float.toString(AtlasProperties.getProperties().getStarsLimitingMagnitude()))  );
				add( new LegendDataContent( Messages.getString("generation.options.dso.magnitude"), Float.toString(AtlasProperties.getProperties().getObjectsLimitingMagnitude()))  );
				add( new LegendDataContent( Messages.getString("generation.options.show.legend"), Boolean.toString(AtlasProperties.getProperties().isShowLegend()))  );
				add( new LegendDataContent( Messages.getString("generation.options.multivolume"), Boolean.toString(AtlasProperties.getProperties().isMultiVolume()))  );
				add( new LegendDataContent( Messages.getString("generation.options.reflector"), 
						Translate.translate( AtlasProperties.getProperties().isReflector() ? "Reflector" : "Refractor" )  
						)
					);
			}
		};

		headerPage.writeContent(content);
		
		Page legendPage = pageFactory.create("legend");
		legendPage.setTitle("Legend.title");
		legendPage.writeContent(null);

	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writeLicensePage()
	 */
	@Override
	public void writeLicensePage() {
		Page licensePage = pageFactory.create("license");
		licensePage.setTitle("License.title");
		licensePage.writeContent(null);
		
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writeChartPages(java.util.List, java.util.List)
	 */
	@Override
	public void writeChartPages(List<Object[]> list) {
		StringBuilder caption = new StringBuilder();
		if (!AtlasProperties.getProperties().isWhite()) {
			caption.append(", ").append(Messages.getString("Bungee.darkedition"));
		}
		caption.append('.');

		generator.beginSection(Messages.getString("Charts.main.title"));
		
		list.forEach( nextChartPage -> {
			
			String constellation = (String) nextChartPage[0];
			int chartId = (int)(nextChartPage[1]);
			
			
			String title = String.format(Messages.getString("Chart.main.title"), 
					(chartId + 1),
					AtlasProperties.getProperties().getScale().getFieldSize(),
					AtlasProperties.getProperties().getScale().getRa(chartId),
					AtlasProperties.getProperties().getScale().getDec(chartId),
					constellation);
			
			Page chartPage = pageFactory.create("chartPage");
			((ChartPage)chartPage).setChartId(chartId);
			AtlasProperties.getProperties().setSuffix("_chart" + chartId);

			caption.append(title);
			
			//  Rendering 
			StringBuilder frametitle = new StringBuilder();
			frametitle.append(AtlasProperties.getProperties().getScale().getFieldSize()).append(DEG).append(' ').append(Messages.getString("Bungee.deg.around")).append(' ');
			frametitle.append(AtlasProperties.getProperties().getScale().getRa(chartId)).append("h, ").append(
					AtlasProperties.getProperties().getScale().getDec(chartId)
				).append(DEG);
			
			chartPage.setTitle( title, constellation);
			
			chartPage.writeContent(new ArrayList<Content>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -8671851935194000334L;
	
				{
					add( SkyMapComposer.getSkyMap( chartId ) );
				}
			});

		});		
	}
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#startIndexSection()
	 */
	@Override
	public void startIndexSection() {
		generator.beginSection(Messages.getString("Index.mainTitle"));
		
	}
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#startAtlas()
	 */
	@Override
	public void startAtlas() {
		generator.writeHeader(null, getAtlasTitle(), author, institute, date + " vers." +AtlasProperties.getProperties().getVersion(), null, true, true, true, "0.25", "0.25");
	
		// Hack to replace some code at the top of the latex file
		String code = generator.getCode();
		code = DataSet.replaceAll(code, "\\usepackage[size=a4]{beamerposter}", "\\usepackage[size="+AtlasProperties.getProperties().getPaperSize().getName() +"]{beamerposter}", true);
		code = DataSet.replaceAll(code, "\\usepackage[a4paper, landscape]{geometry}", "\\usepackage["+AtlasProperties.getProperties().getPaperSize().getLatexName() +", landscape]{geometry}", true);
		
		if (AtlasProperties.getProperties().getPathCover() != null) {
			StringBuilder before = new StringBuilder(code.substring(0, code.indexOf("\\begin{document}") -1 ));
			String after = code.substring(code.indexOf("\\begin{document}"));
		
			before.append("\\pretitle{%").append(FileIO.getLineSeparator());
			before.append("\\begin{center}").append(FileIO.getLineSeparator());
			before.append("\\LARGE").append(FileIO.getLineSeparator());
			before.append("\\includegraphics[scale=0.28]{" + AtlasProperties.getProperties().getPathCover() + "}\\\\[\\bigskipamount]");
			before.append('}').append(FileIO.getLineSeparator());
			before.append("\\posttitle{\\end{center}}").append(FileIO.getLineSeparator());

			code = before.toString() + after;
		}
			
//			generator.writeRawText(generator.writeImage("50%", null, "center", null, null, 	AtlasProperties.getProperties().getPathCover()));

		generator.setCode(code);
	}


	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#endIndexSection()
	 */
	@Override
	public void endIndexSection() {
		// do nothing
	}

	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writeIndexPage(int, java.util.List, net.anc.atlas.Index)
	 */
	@Override
	public void writeIndexPage(int chartId, List<Content> data, Index indexPage) {
		
		generator.beginFrame(false, false);
		
		String caption = null;
		String header[] = null;
		
		
		// TODO: remove this ugly if
		if( indexPage instanceof ConstellationsByChartIndex ||  indexPage instanceof NovaeAndSupernovaeIndex ){
			caption = Messages.getString(indexPage.getId());
		}
		else {
			caption = Messages.getString(indexPage.getId()) + " " + (chartId + 1);;
		}
		
		
		header = indexPage.getColumnsName();
		indexPage.setTitle(caption);
		((Table)indexPage).setHeader(caption, header);
		
		indexPage.writeContent(data);  
		
		generator.endFrame();
	}
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#endAtlas()
	 */
	@Override
	public void endAtlas(){
		generator.endBody();
		generator.endDocument();
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#toString()
	 */
	@Override
	public String toString() {
		return generator.getCode();
	}

	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Atlas#writeHistoryPage()
	 */
	@Override
	public void writeHistoryPage() {
		Table history = (Table) pageFactory.create("history");
		history.setTitle("History.title");
		history.setHeader(null, null);
		history.writeContent(null);
		
		
	}

	@Override
	public void writeMinimap() {
		String north = AtlasProperties.getProperties().getNorthMinimapFilename();
		
		Page northPage = pageFactory.create("minimaps");
		northPage.setTitle("north.minipage.title", north);
		
		EmisphereMinimap northMap = new NorthernEmisphereMinimap();
		northMap.compile();
		
		northPage.writeContent(northMap.getCoordinates());
		
		String south = AtlasProperties.getProperties().getSouthMinimapFilename();
		Page southPage = pageFactory.create("minimaps");
		southPage.setTitle("south.minipage.title", south);
		
		EmisphereMinimap southMap = new SouthernEmisphereMinimap();
		southMap.compile();
		southPage.writeContent(southMap.getCoordinates());
	}

	@Override
	public String getAtlasTitle() {
		return Messages.getString("Bungee.title") + AtlasProperties.getProperties().getYear();
	}

	
}
