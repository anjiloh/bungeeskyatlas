package 	net.anc.atlas;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

import com.google.inject.Guice;
import com.google.inject.Injector;

import jparsec.util.JPARSECException;
import net.anc.atlas.guice.ChartsOnlyBuilderModule;
import net.anc.atlas.guice.CompleteBuilderModule;
import net.anc.atlas.guice.MessierBuilderModule;
import net.anc.atlas.guice.NoDataBuilderModule;
import net.anc.atlas.guice.RecoverBuilderModule;
import net.anc.atlas.index.IndexModule;
import net.anc.atlas.pages.PagesForCatalogsModule;
import net.anc.atlas.pages.PagesModule;

/**
 *  Main atlas generator.
 *  
 *  Parameters are:
 *  
 *  <ol>
 * 		<li><b>cleanup</b>: creates a brand new atlas, by overriding any previous generated object</li>
 * 		<li><b>recover</b>: recovers a previous run which was, for any reason, interrupted
 * 		<li><b>nodata</b>: creates indexes part only
 *  </ol> 
 *  
 *  Other usages:
 *  
 *  <ol>
 *  	<li><b>messier</b>: creates a Messier Catalog</li>
 *  </ol>
 * 
 * 
 * @author Angelo Nicolini
 *
 */
public class MySkyAtlas {

	/**
	 * Main atlas generator.
	 * 
	 * @param args            .
	 * @throws JPARSECException 
	 * @throws IOException 
	 */
	public static void main(String args[]) throws JPARSECException, IOException {
		
		final LogManager logManager = LogManager.getLogManager();
		try (final InputStream is = MySkyAtlas.class.getResourceAsStream("/logging.properties")) {
            logManager.readConfiguration(is);
        }

		Injector injector = null;
        
        if( args.length == 0 ) {
        	System.err.println("Error: cleanup|recover|nodata");
        	System.exit(-1);
        }
        
		if( "nodata".equalsIgnoreCase(args[0]) ){
			injector = Guice.createInjector(
					new NoDataBuilderModule(),
					new PagesModule(),
					new IndexModule()
				);
		}
		else if( "cleanup".equalsIgnoreCase(args[0]) ){
			injector = Guice.createInjector(
					new CompleteBuilderModule(),
					new PagesModule(),
					new IndexModule()
				);
		}
		else if( "recover".equalsIgnoreCase(args[0]) ){
			injector = Guice.createInjector(
					new RecoverBuilderModule(),
					new PagesModule(),
					new IndexModule()
				);
		}
		else if( "charts".equalsIgnoreCase(args[0]) ){
			injector = Guice.createInjector(
					new ChartsOnlyBuilderModule(),
					new PagesModule(),
					new IndexModule()
				);
		}
		else if( "messier".equalsIgnoreCase(args[0]) ){
			injector = Guice.createInjector(
					new MessierBuilderModule(),
					new PagesForCatalogsModule(),
					new IndexModule()
				);
		}
		else {
        	System.err.println("Error: cleanup|recover|nodata|messier");
        	System.exit(-1);
		}
		BungeeSkyAtlas atlas = injector.getInstance(BungeeSkyAtlas.class);
		atlas.create();			
	}
	
	
}
