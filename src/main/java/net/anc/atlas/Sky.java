/**
 * 
 */
package net.anc.atlas;

import java.awt.Color;

import jparsec.astronomy.Constellation;
import jparsec.astronomy.CoordinateSystem.COORDINATE_SYSTEM;
import jparsec.ephem.EphemerisElement;
import jparsec.ephem.Target.TARGET;
import jparsec.astronomy.TelescopeElement;
import jparsec.graph.DataSet;
import jparsec.graph.chartRendering.Graphics;
import jparsec.graph.chartRendering.PlanetRenderElement;
import jparsec.graph.chartRendering.Projection.PROJECTION;
import jparsec.graph.chartRendering.RenderPlanet;
import jparsec.graph.chartRendering.SkyRenderElement;
import jparsec.graph.chartRendering.SkyRenderElement.COLOR_MODE;
import jparsec.graph.chartRendering.SkyRenderElement.FAST_LINES;
import jparsec.graph.chartRendering.SkyRenderElement.LEYEND_POSITION;
import jparsec.graph.chartRendering.SkyRenderElement.MILKY_WAY_TEXTURE;
import jparsec.graph.chartRendering.SkyRenderElement.REALISTIC_STARS;
import jparsec.graph.chartRendering.SkyRenderElement.SUPERIMPOSED_LABELS;
import jparsec.io.FileFormatElement;
import jparsec.io.FileIO;
import jparsec.io.ReadFile;
import jparsec.math.Constant;
import jparsec.time.AstroDate;
import jparsec.util.Configuration;
import jparsec.util.JPARSECException;
import jparsec.util.Translate;

/**
 * @author Angelo Nicolini
 *
 */
public class Sky {

	private volatile static EphemerisElement ephemeris = null;

	private SkyRenderElement skyRender;
	private PlanetRenderElement render ;
	private TelescopeElement telescope;

	public Sky(Sky other) throws JPARSECException{
		init();
		skyRender = other.skyRender.clone();
		
	}
	
	/**
	 * Accesso to the EphemerisElement. 
	 * If the Sky object has not created yet, it returns null 
	 * @return
	 * @throws JPARSECException 
	 */
	public static EphemerisElement getEphemeris() throws JPARSECException {
		if( ephemeris == null ) {
			ephemeris = new EphemerisElement(
					TARGET.NOT_A_PLANET,
					EphemerisElement.COORDINATES_TYPE.ASTROMETRIC,
					(new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1, 12, 0, 0)).jd(),
					EphemerisElement.GEOCENTRIC, 
					EphemerisElement.REDUCTION_METHOD.IAU_2009,
					EphemerisElement.FRAME.DYNAMICAL_EQUINOX_J2000);
		
//			eph.optimizeForSpeed();
			ephemeris.optimizeForAccuracy();
		}
		return ephemeris;
	}

	
	public Sky() throws JPARSECException{
		init();

		int marginX = 150; 
		int marginY = 150;
		
		int width = (int) ( 0.5 + AtlasProperties.getProperties().getPaperSize().getWidth()  * AtlasProperties.getProperties().getPixelPerInch() / AtlasProperties.MM_PER_INCH) - marginX * 2;
		int height = (int) (0.5 + AtlasProperties.getProperties().getPaperSize().getHeight() * AtlasProperties.getProperties().getPixelPerInch() / AtlasProperties.MM_PER_INCH) - marginY * 2;
		//
		//
		 
		skyRender = new SkyRenderElement(
				getCoordinateSystem(),
				PROJECTION.STEREOGRAPHICAL, 
				0, 
				0.0, 
				width,
				height,
				render, 
				telescope);
		
		initSkyProperties();
		
//        addCatalogs();
//
//        for (int i = 0; i < sky.getNumberOfExternalCatalogs(); i++) {
//            sky.drawExternalCatalogs[i] = true;
//        }
//		

	}

	/**
	 * definte the coordinate system of this sky 
	 * @return
	 */
	protected COORDINATE_SYSTEM getCoordinateSystem() {
		return COORDINATE_SYSTEM.HORIZONTAL;
	}

	@SuppressWarnings("unused")
	private void addCatalogs() throws JPARSECException {
		String contents[] = DataSet.arrayListToStringArray(ReadFile.readResource(FileIO.DATA_SKY_DIRECTORY + "objects.txt"));
        skyRender.addExternalCatalog(Translate.translate("Deep sky object"), Translate.translate("Deep sky object"), Color.PINK.getRGB(), contents, 
        		new FileFormatElement[] {
        				new FileFormatElement(1, 12, SkyRenderElement.EXTERNAL_CATALOG_FIELD_NAME1),
//        				new FileFormatElement(86, 300, SkyRenderElement.EXTERNAL_CATALOG_FIELD_DETAILS),
//        				new FileFormatElement(14, 27,  SkyRenderElement.EXTERNAL_CATALOG_FIELD_COORDINATES_TYPE),
        				new FileFormatElement(14, 27,  SkyRenderElement.EXTERNAL_CATALOG_FIELD_DETAILS),

        				new FileFormatElement(29, 39, SkyRenderElement.EXTERNAL_CATALOG_FIELD_RA_HOURS_WITH_DECIMALS),
        				new FileFormatElement(41, 52, SkyRenderElement.EXTERNAL_CATALOG_FIELD_DEC_DEG_WITH_DECIMALS), 
        				new FileFormatElement(54, 58, SkyRenderElement.EXTERNAL_CATALOG_FIELD_MAG), 
        				new FileFormatElement(60, 69, SkyRenderElement.EXTERNAL_CATALOG_FIELD_MAXSIZE_DEG),
        				new FileFormatElement(71, 80, SkyRenderElement.EXTERNAL_CATALOG_FIELD_MINSIZE_DEG),
        				new FileFormatElement(82, 84, SkyRenderElement.EXTERNAL_CATALOG_FIELD_POSITION_ANGLE_DEG),
        		}
        		, false
        		);
	}

	protected void initSkyProperties() throws JPARSECException {
		// start ephemeris first
		getEphemeris();
		
		if (AtlasProperties.getProperties().getPaperSize().getHeight() <= 3000) {
			skyRender.planetRender.highQuality = true;
		}
			
		skyRender.width =  AtlasProperties.getProperties().getPaperSize().getHeight() * 10 - 10;
		skyRender.height = (int) ((skyRender.width *  AtlasProperties.getProperties().getPaperSize().getWidth())) /  AtlasProperties.getProperties().getPaperSize().getHeight();

		skyRender.drawCoordinateGridHighZoom = true;
		
		skyRender.setColorMode(COLOR_MODE.WHITE_BACKGROUND);
		if (AtlasProperties.getProperties().isWhite()) {
			skyRender.setColorMode(COLOR_MODE.PRINT_MODE);
		}
		else {
			skyRender.setColorMode(COLOR_MODE.BLACK_BACKGROUND);
			Configuration.MAX_CACHE_SIZE = 1;
		}
		
		
		skyRender.drawStarsLimitingMagnitude = AtlasProperties.getProperties().getStarsLimitingMagnitude();
		skyRender.drawObjectsLimitingMagnitude = AtlasProperties.getProperties().getObjectsLimitingMagnitude();
		
		skyRender.coordinateSystem = COORDINATE_SYSTEM.EQUATORIAL;
		// as per Tomás's tip: keep it false!
		skyRender.drawClever = false;

		skyRender.drawConstellationLimits = true;
		skyRender.drawConstellationNames = true;
		skyRender.drawConstellationNamesType = Constellation.CONSTELLATION_NAME.LATIN;				
		skyRender.drawConstellationNamesFont = Graphics.FONT.DIALOG_ITALIC_32;

		skyRender.drawTransNeptunianObjects = false;
		skyRender.drawArtificialSatellites = false;
		skyRender.drawArtificialSatellitesOnlyThese = "ISS,HST";
		skyRender.drawComets = false;
		skyRender.drawAsteroids = false;
		skyRender.drawSuperNovaAndNovaEvents = true;
		skyRender.drawPlanetsMoonSun = false;
		skyRender.drawDeepSkyObjectsAllMessierAndCaldwell = true;
		skyRender.drawMeteorShowers = false;
		skyRender.drawMeteorShowersOnlyActive = true;
		
		skyRender.drawSpaceProbes = false;

		skyRender.drawStarsGreekSymbols = true;
		skyRender.drawStarsGreekSymbolsOnlyIfHasProperName = false;
		skyRender.drawStarsRealistic = REALISTIC_STARS.NONE_CUTE;

		skyRender.drawFastLabels = SUPERIMPOSED_LABELS.AVOID_SUPERIMPOSING_VERY_ACCURATE;
		skyRender.drawFastLabelsInWideFields = false;

		skyRender.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.OPTICAL;
//		sky.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.H_ALPHA;
//		sky.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.CO;
//		sky.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.WMAP;
//		sky.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.HI_21cm;
//		sky.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.IRAS_100;
		
		skyRender.drawFastLinesMode = FAST_LINES.GRID_AND_MILKY_WAY_AND_CONSTELLATIONS;

		skyRender.drawStarsLabelsLimitingMagnitude = 4;
		skyRender.drawSunSpots = false;
		skyRender.drawDeepSkyObjectsLabels = true;
		skyRender.drawStarsPositionAngleInDoubles = true;			
		if( AtlasProperties.getProperties().isShowLegend() ) {	
			skyRender.drawLeyend = LEYEND_POSITION.TOP;
		}
		else {
			skyRender.drawLeyend = LEYEND_POSITION.NO_LEYEND;
		}
		skyRender.drawCoordinateGridCardinalPoints = true;
		
		skyRender.drawStarsNamesFont = Graphics.FONT.DIALOG_PLAIN_20;
		skyRender.drawPlanetsNamesFont = Graphics.FONT.DIALOG_PLAIN_28;
//		sky.drawCoordinateGridFont = Graphics.FONT.DIALOG_PLAIN_27; // dimensione char legenda
		skyRender.drawDeepSkyObjectsNamesFont = Graphics.FONT.SANS_SERIF_ITALIC_16;
		skyRender.drawMinorObjectsNamesFont = Graphics.FONT.SANS_SERIF_PLAIN_16;
		skyRender.drawSkyBelowHorizon = true;
		skyRender.drawSkyCorrectingLocalHorizon = false;

		skyRender.drawDeepSkyObjectsTextures = !AtlasProperties.getProperties().isWhite();

		skyRender.drawFaintStars = true;


		if (skyRender.getColorMode() == COLOR_MODE.WHITE_BACKGROUND || skyRender.getColorMode() == COLOR_MODE.PRINT_MODE) {
			skyRender.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.NO_TEXTURE;
			skyRender.drawDeepSkyObjectsTextures = false;
			skyRender.drawStarsRealistic = REALISTIC_STARS.NONE_CUTE;
			skyRender.fillMilkyWay = true;
			skyRender.drawStarsColor = 255<<24 | 0<<16 | 0<<8 | 0;
		}

		
		if (skyRender.getColorMode() == COLOR_MODE.WHITE_BACKGROUND || skyRender.getColorMode() == COLOR_MODE.PRINT_MODE) {
			skyRender.drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.NO_TEXTURE;
			skyRender.drawDeepSkyObjectsTextures = false;
			skyRender.drawStarsRealistic = REALISTIC_STARS.NONE_CUTE;
			skyRender.fillMilkyWay = true;
			skyRender.drawStarsColor = 255<<24 | 0<<16 | 0<<8 | 0;
		}
	
//		if( !telescope.equals(TelescopeElement.HUMAN_EYE)) {
//			sky.telescope.ocular.focalLength = TelescopeElement.getOcularFocalLengthForCertainField((double) ((38 * sky.width) / sky.height) * Constant.DEG_TO_RAD, sky.telescope);
//		}
	}

	protected void init() throws JPARSECException {
		
		render = new PlanetRenderElement(false, true, true, false, false);
		RenderPlanet.ALLOW_SPLINE_RESIZING = false;
		RenderPlanet.FORCE_HIGHT_QUALITY = true;
		RenderPlanet.MAXIMUM_TEXTURE_QUALITY_FACTOR = 2;
		render.highQuality = true;
		
//		telescope = TelescopeElement.BINOCULARS_7x50;
//		telescope = TelescopeElement.REFRACTOR_10cm_f5;
//		telescope = TelescopeElement.BINOCULARS_11x80;
//		telescope = TelescopeElement.HUMAN_EYE;
//		telescope = TelescopeElement.SCHMIDT_CASSEGRAIN_80cm;
		
		// define the zoom! 
		// 15deg circa, as per Salvo Ali hint
//		telescope = new TelescopeElement("object", 1200, 100, 0, 0, 0f, new OcularElement());

		setTelescope(AtlasProperties.getProperties().getScale().getTelescope());
		
//		telescope = new TelescopeElement("object", 1000, 100, 0, 0, 0f, new OcularElement("my ocular", 82, 8.0, 32));
		
//		if( !telescope.equals(TelescopeElement.HUMAN_EYE)) {
//			telescope.ocular.focalLength = 370f;
//			telescope.ocular.focalLength = 90f;
//			telescope.ocular.focalLength = 300f;
//		}
		
		
	}

	
	public void setCentralLatitude(double dec) {
		skyRender.centralLatitude = dec * Constant.DEG_TO_RAD;
		
	}
	
	public void setCentralLongitude(double ra) {
		skyRender.centralLongitude = ra / Constant.RAD_TO_HOUR;
		
	}
	
	public SkyRenderElement getRender() {
		return skyRender;
	}

	/**
	 * delegate to sky.centralLongitude
	 * @return
	 */
	public double getCentralLongitude() {
		return skyRender.centralLongitude;
	}

	/**
	 * delegate to sky.centralLatitude
	 * @return
	 */
	public double getCentralLatitude() {
		return skyRender.centralLatitude;
	}
	
	
	public final double getStarsLimitingMagnitude() {
		return skyRender.drawStarsLimitingMagnitude;
	}
	
	public final double getObjectsLimitingMagnitude() {
		return skyRender.drawObjectsLimitingMagnitude;
	}
	
	/**
	 * set the telescope type, only for the hierarchy
	 * @param telescope
	 */
	protected void setTelescope(TelescopeElement telescope) {
		this.telescope = telescope;
	}

	protected SkyRenderElement getSkyRender() {
		return skyRender;
	}
	
}
