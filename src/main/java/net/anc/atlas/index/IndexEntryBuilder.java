/**
 * 
 */
package net.anc.atlas.index;

import jparsec.util.JPARSECException;
import net.anc.atlas.AstroItemDetail;

/**
 * @author Angelo Nicolini
 *
 */
public interface IndexEntryBuilder extends IndexComposer {
	
	String OBJECTS = "dsoEntry";
	String STARS = "starEntry";
	String NOVAE = "novaEntry";

	AstroItemDetail compose(AstroItemDetail item) throws JPARSECException;
}
