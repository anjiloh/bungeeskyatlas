/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;

import jparsec.util.JPARSECException;

/**
 * @author Angelo Nicolini
 *
 */
public class ChartsOnlyBungeeSkyAtlas extends AbstractBungeeSkyAtlas {

	@Override
	public void create() throws JPARSECException, IOException {
		startupPolicy.performOpertion();
		
		initialize();
		doPreamble();
		doMapsChapter();
		doChartsIndexChapter();
		terminate();
		
	}

}
