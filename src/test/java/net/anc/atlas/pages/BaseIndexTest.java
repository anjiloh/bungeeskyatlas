/**
 * 
 */
package net.anc.atlas.pages;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.TestModule;

/**
 * @author nicolinia
 *
 */
public class BaseIndexTest {

	protected Injector injector = Guice.createInjector(
			new TestModule(),
			new AbstractModule() {
		        @Override
		        protected void configure() {
		            bind(BaseIndex.class).toInstance(new BaseIndex() {

						@Override
						public String getId() {
							// TODO Auto-generated method stub
							return null;
						}

						@Override
						public String[] getColumnsName() {
							// TODO Auto-generated method stub
							return null;
						}

						@Override
						public void writeContent(List<Content> content) {
							// TODO Auto-generated method stub
							
						}
		            	
		            });
		        }
			}
			
			);

	@Inject
	protected AtlasGenerator generator;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
        injector.injectMembers(this);
		// start generator in the correct way
		generator.writeHeader(null, "title", "author", "institute", "date", null, true, false, true, "0.25", "0.25");

	}

	/**
	 * Test method for {@link net.anc.atlas.pages.BaseIndex#setHeader(java.lang.String, java.lang.String[])}.
	 */
	@Test
	public void testSetHeaderDarkPaper() {

		AtlasProperties.getProperties().setWhite(false);
		
		BaseIndex subject = injector.getInstance(BaseIndex.class);
		subject.setHeader("don't care", new String[] {"aa", "bb"});
		
		String code = generator.getCode();
		assertTrue(code.contains("\\node (a) at (0,0)    {\\circled{\\color{black}{\\ref{subsec:#1}}}};%"));
		assertTrue(code.contains("\\node (b) at (4.5,0)  {\\circled{\\color{black}{\\ref{subsec:#2}}}};%"));
		assertTrue(code.contains("\\node (c) at (0,4.5)  {\\circled{\\color{black}{\\ref{subsec:#3}}}};%"));
		assertTrue(code.contains("\\node (d) at (-4.5,0) {\\circled{\\color{black}{\\ref{subsec:#4}}}};%"));
		assertTrue(code.contains("\\node (e) at (0,-4.5) {\\circled{\\color{black}{\\ref{subsec:#5}}}};%"));

	}
	@Test
	public void testSetHeaderWhitePaper() {

		AtlasProperties.getProperties().setWhite(true);
		
		BaseIndex subject = injector.getInstance(BaseIndex.class);
		subject.setHeader("don't care", new String[] {"aa", "bb"});
		
		String code = generator.getCode();
		assertTrue(code.contains("\\node (a) at (0,0) {\\circled{\\ref{subsec:#1}}};%"));
		assertTrue(code.contains("\\node (b) at (4.5,0) {\\circled{\\ref{subsec:#2}}};%"));
		assertTrue(code.contains("\\node (c) at (0,4.5) {\\circled{\\ref{subsec:#3}}};%"));
		assertTrue(code.contains("\\node (d) at (-4.5,0) {\\circled{\\ref{subsec:#4}}};%"));
		assertTrue(code.contains("\\node (e) at (0, -4.5) {\\circled{\\ref{subsec:#5}}};%"));

	}

//	/**
//	 * Test method for {@link net.anc.atlas.pages.BaseIndex#setTitle(java.lang.String[])}.
//	 */
//	@Test
//	public void testSetTitle() {
//		fail("Not yet implemented"); // TODO
//	}

}
