/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;

import jparsec.util.JPARSECException;

/**
 * @author nicolinia
 *
 */
public class NoDataBungeeSkyAtlas extends AbstractBungeeSkyAtlas {

	
	public void create() throws JPARSECException, IOException {
		
		startupPolicy.performOpertion();
		
		initialize();
		doPreamble();
		doChartsIndexChapter();
		doIndexesChapter();		
		terminate();
	}

}
