/**
 * 
 */
package net.anc.atlas;

/**
 * Content to be shown in an index
 * 
 * @author Angelo Nicolini
 *
 */
public interface Content {
	
	/**
	 * this returns the number of items in a Content
	 * @return
	 */
	int size();

}
