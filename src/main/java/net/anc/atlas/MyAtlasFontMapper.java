/**
 * 
 */
package net.anc.atlas;

import java.awt.Font;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;

import jparsec.graph.DataSet;

/**
 * @author Angelo Nicolini
 *
 */
public class MyAtlasFontMapper  implements com.itextpdf.awt.FontMapper {
	
	private static final Logger LOGGER = Logger.getLogger( MyAtlasFontMapper.class.getName() );

	private String javaFonts[] = new String[] {"Dialog", "SansSerif", "Serif"};
	
    public BaseFont awtToPdf(Font font) {
        try {
        	// Use following line to know what is being passed from Java side
        	//System.out.println("Java is requesting "+font.getName()); //+". Trace: "+JPARSECException.getCurrentTrace());
        	
        	int which = DataSet.getIndex(javaFonts, font.getName());
        	if (which < 0) {
        		which = 1;
    	        LOGGER.log(Level.FINEST, "Font "+font.getName()+" not found. Using "+javaFonts[which]+" instead.");

//        		System.out.println("Font "+font.getName()+" not found. Using "+javaFonts[which]+" instead.");
        	}
            if (which == 2) return BaseFont.createFont(BaseFont.SYMBOL, BaseFont.CP1250, BaseFont.EMBEDDED);

        	String name = BaseFont.HELVETICA;
        	if (font.isBold()) name = BaseFont.HELVETICA_BOLD;
        	if (font.isItalic()) {
        		name = BaseFont.HELVETICA_OBLIQUE;
        		if (font.isBold()) name = BaseFont.HELVETICA_BOLDOBLIQUE;
        	}
        	/*
        	if (which == 1) {
            	name = BaseFont.TIMES_ROMAN;
            	if (font.isBold()) name = BaseFont.TIMES_BOLD;
            	if (font.isItalic()) {
            		name = BaseFont.TIMES_ITALIC;
            		if (font.isBold()) name = BaseFont.TIMES_BOLDITALIC;
            	}
        	}
        	*/
            return BaseFont.createFont(name, BaseFont.CP1252, BaseFont.EMBEDDED);
        } catch (DocumentException e) {
        	throw new RuntimeException("cannot create font mapper", e);
        } catch (IOException e) {
        	throw new RuntimeException("cannot create font mapper", e);
        }
    }
    
    @Nullable
    public Font pdfToAwt(BaseFont font, int size) { 
    	return null; 
    }


}

//***
/*	        FontMapper mapper = new FontMapper() {
String javaFonts[] = new String[] {"Dialog", "SansSerif", "Serif"};
String sysFonts[] = new String[] {"/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf", 
	"/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf"};
//String sysFonts[] = new String[] {"/usr/share/fonts/truetype/SourceSansPro/SourceCodePro.ttf", 
//	"/usr/share/fonts/truetype/SourceSansPro/SourceSansPro.ttf"};
//String sysFonts[] = new String[] {"/usr/share/fonts/truetype/linlibertine/LinLibertine.ttf", 
//	"/usr/share/fonts/truetype/linlibertine/LinBiolinum.ttf"};
//String sysFonts[] = new String[] {"/usr/share/fonts/truetype/linlibertine/LinLibertine.ttf", 
//	"/usr/share/fonts/truetype/junicode/Junicode.ttf"};
public BaseFont awtToPdf(Font font) {
    try {
    	// Use following line to know what is being passed from Java side
    	//System.out.println("Java is requesting "+font.getName()); //+". Trace: "+JPARSECException.getCurrentTrace());
    	
    	int which = DataSet.getIndex(javaFonts, font.getName());
    	if (which < 0) {
    		which = 1;
    		System.out.println("Font "+font.getName()+" not found. Using "+javaFonts[which]+" instead.");
    	}
        if (which == 2) return BaseFont.createFont(BaseFont.SYMBOL, BaseFont.CP1250, BaseFont.EMBEDDED);
    	String f = sysFonts[which];
    	
    	if (font.isBold()) {
    		if (font.isItalic()) {
    			f = DataSet.replaceOne(f, ".ttf", "-BoldOblique.ttf", 1);	                			
    		} else {
    			f = DataSet.replaceOne(f, ".ttf", "-Bold.ttf", 1);
    		}
    	} else {
    		if (font.isItalic())
        		f = DataSet.replaceOne(f, ".ttf", "-Oblique.ttf", 1);
    	}
    	
        return BaseFont.createFont(f, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
    } catch (DocumentException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return null;
}
public Font pdfToAwt(BaseFont font, int size) { return null; }
};
*/
