	/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;

/**
 * This class recovers the generation by getting the last completed record from the recovery file 
 * and purging the index files from the broken lines
 * 
 * TODO: what if the index file is empty or with a line only?? to be tested!
 * 
 * @author Angelo Nicolini
 *
 */
public class Recover implements Startup {

	private static final Logger LOGGER = Logger.getLogger( Startup.class.getName() );

	
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Startup#performOpertion()
	 */
	@Override
	public void performOpertion() {

		Path file = Paths.get(RecoveryPoint.MAP_INDEX_FILE);
		
		if( Files.exists(file )) {
			List<String> lines = null;
			try {
				lines = Files.readAllLines(file);
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "cannot read recover file, starting as brand new generation", e);
				return;
				
			}
			// is the last line valid?
			int chartId = getChartId(lines);

			LOGGER.info("Reset indexes to the MAP nr. " + chartId);
			
			try {
				purgeRecoveryFromChartId(chartId);
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Problem while recovering index of charts", e);
			}
			try {
				purgeObjectsChartId(chartId);
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Problem while recovering index of objects", e);
			}
			try {
				purgeStarFromChartId(chartId);
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Problem while recovering index of stars", e);
			}
			try {
				purgeNovaeFromChartId(chartId);
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Problem while recovering index of novae", e);
			}
			
		}
		else {
			// do nothing??
		}
		
	}

	private void purgeNovaeFromChartId(int chartId) throws IOException {

		Path file = Paths.get(BungeeSkyAtlas.NOVAE_INDEX_FILE);
//		List<String> recovery = new ArrayList<String>();
		List<String> recovery = null;
		try (Stream<String> stream = Files.lines(file)) {
			recovery = stream
					.filter(  line -> {
						String[] data = line.split(",");
						int storedChartId = -1;
						if( data.length == 10) {
							storedChartId = Integer.parseInt(data[9]);
						}
						return ( storedChartId != -1 && storedChartId < chartId ) ;
					} ).collect(Collectors.toList()); 
			
		}
		
		Files.write(file, recovery, StandardCharsets.UTF_8 , StandardOpenOption.TRUNCATE_EXISTING );		
		
	}

	private void purgeStarFromChartId(int chartId) throws IOException {

		Path file = Paths.get(BungeeSkyAtlas.STARS_INDEX_FILE);
//		List<String> recovery = new ArrayList<String>();
		List<String> recovery = null;
		try (Stream<String> stream = Files.lines(file)) {
			recovery = stream
					.filter(  line -> {
						String[] data = line.split(",");
						int storedChartId = -1;
						if( data.length == 10) {
							storedChartId = Integer.parseInt(data[9]);
						}
						return ( storedChartId != -1 && storedChartId < chartId ) ;
					} ).collect(Collectors.toList()); 
			
		}
		
		Files.write(file, recovery, StandardCharsets.UTF_8 , StandardOpenOption.TRUNCATE_EXISTING );		
	}

	private void purgeObjectsChartId(final int chartId) throws IOException {
		Path file = Paths.get(BungeeSkyAtlas.OBJECTS_INDEX_FILE);
//		List<String> recovery = new ArrayList<String>();
		List<String> recovery = null;
		try (Stream<String> stream = Files.lines(file)) {
			recovery = stream
					.filter(  line -> {
						String[] data = line.split(",");
						int storedChartId = -1;
						if( data.length == 10) {
							storedChartId = Integer.parseInt(data[9]);
						}
						return ( storedChartId != -1 && storedChartId < chartId ) ;
					} ).collect(Collectors.toList()); 
			
		}
		
		Files.write(file, recovery, StandardCharsets.UTF_8 , StandardOpenOption.TRUNCATE_EXISTING );
		
	}

	private void purgeRecoveryFromChartId(final int chartId) throws IOException {
		Path file = Paths.get(RecoveryPoint.MAP_INDEX_FILE);
//		List<String> recovery = new ArrayList<String>();
		List<String> recovery = null;
		try (Stream<String> stream = Files.lines(file)) {
			recovery = stream
					.filter(  line -> {
						String[] data = line.split(",");
						int storedChartId = -1;
						if( data.length == 3) {
							int offset  = 0;
							if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH ) {
								offset = 4;
							}
							storedChartId = Integer.parseInt(data[0].substring( data[0].indexOf("chart") + 5, data[0].indexOf('.') - offset));
						}
						return ( storedChartId != -1 && storedChartId < chartId ) ;
					} ).collect(Collectors.toList()); 
			
		}
		
		Files.write(file, recovery, StandardCharsets.UTF_8 , StandardOpenOption.TRUNCATE_EXISTING );
	}

	/**
	 * get the chartId from the last valid line
	 * TODO what if lines is empty?? 
	 * @param lines
	 * @return
	 */
	private int getChartId(List<String> lines) {
		int chartId = 0;
		int pos = lines.size() - 1;
		do {
			String line = lines.get( pos );
			String[] data = line.split(",");
			if( data.length == 3) {
				int offset  = 0;
				if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH ) {
					offset = 4;
				}
				String identifier = data[0].substring( data[0].indexOf("chart") + 5, data[0].indexOf('.') -offset );
				chartId = Integer.parseInt(identifier);
				break;
			}
			else {
				pos --;
			}
			
		}while(pos > 0);
		
		return chartId;
	}

}
