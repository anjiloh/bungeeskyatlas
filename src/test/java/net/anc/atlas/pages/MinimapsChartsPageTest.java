package net.anc.atlas.pages;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;
import net.anc.atlas.TestModule;

public class MinimapsChartsPageTest {

	protected Injector injector = Guice.createInjector(
			new TestModule(),
			new PagesModule(),
			new AbstractModule() {
		        @Override
		        protected void configure() {
		            bind(Page.class).to(MinimapsChartsPage.class);
		        }
			}
			
			);
	

	@Inject
	protected AtlasGenerator generator;
	
	@Before
	public void setUp() throws Exception {
        injector.injectMembers(this);
	}
	
	@Test
	public void testSetTitle() {
		MinimapsChartsPage minimaps = injector.getInstance(MinimapsChartsPage.class);
		minimaps.setTitle("north.minipage.title");
		System.out.println(generator.getCode());
		assertTrue(generator.getCode().contains("section{"+Messages.getString("north.minipage.title") ));
	}

//	@Test
//	public void testWriteContent() {
//		fail("Not yet implemented");
//	}

}
