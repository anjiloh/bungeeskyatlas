package com.guice.test;

import com.google.inject.assistedinject.Assisted;

public interface FerrariFactory extends CarFactory<Ferrari> {
	
	Ferrari create(@Assisted("part") String part);

}
