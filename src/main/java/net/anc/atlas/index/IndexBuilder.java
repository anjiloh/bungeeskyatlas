package net.anc.atlas.index;

import java.util.List;
import java.util.stream.Stream;

import net.anc.atlas.Content;

/**
 * Composer of a list of contents for an index.
 * See {@link Content } for details
 * 
 * @author Angelo Nicolini
 *
 */
public interface IndexBuilder extends IndexComposer {
	
	String CONSTELLATIONS = "constellations";
	String OBJECTS = "objects";
	String STARS = "stars";
	String NOVAE = "novae";
	
	List<Content> getIndex(Stream<String> source, int pageId);
	
}
