/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jparsec.io.FileIO;

/**
 * @author Angelo Nicolini
 *
 */
public class RecoveryPoint {
	private static final Logger LOGGER = Logger.getLogger(MySkyAtlas.class.getName());

	public static final String MAP_INDEX_FILE =  AtlasProperties.getProperties().getOutputPath() + FileIO.getFileSeparator() + "chartIndexFile.bin";

	private static final RecoveryPoint recoveryPoint = new RecoveryPoint();
	private List<String> recovery = new ArrayList<>();

	private RecoveryPoint() {

		try (Stream<String> stream = Files.lines(Paths.get(MAP_INDEX_FILE))) {
			recovery = stream
					.map(line -> line.substring(0,  line.indexOf(',')))
					.collect(Collectors.toList());
			LOGGER.fine("Index file found, skip already created charts");
		} catch (IOException e) {
			LOGGER.fine("No recovery file found");
		}
	}

	public static boolean isAlreadyCreated(String plotFile) {
		return recoveryPoint.recovery.contains(plotFile);

	}

	/**
	 * 
	 * @param plotFile
	 * @param longitude
	 * @param latitude
	 * @throws IOException
	 */
	public static void checkPoint(String plotFile, double longitude, double latitude) throws IOException {
		String row = plotFile +","+longitude+"," +latitude;
		Files.write(Paths.get(MAP_INDEX_FILE), (row + FileIO.getLineSeparator()).getBytes(),
				StandardOpenOption.APPEND, StandardOpenOption.CREATE);
	}

	public static void cleanUp() {
		try {
			Files.delete(Paths.get(MAP_INDEX_FILE));
		} catch (IOException e) {
			LOGGER.fine("delete of chart Index failed");
		}
		recoveryPoint.recovery.clear();
	}

}
