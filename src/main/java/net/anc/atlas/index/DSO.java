package net.anc.atlas.index;

import java.util.logging.Logger;

import jparsec.graph.chartRendering.RenderSky;
import jparsec.graph.chartRendering.RenderSky.OBJECT;
import jparsec.observer.LocationElement;
import net.anc.atlas.AstroItemDetail;

public class DSO implements IndexConverter {

	private static final Logger LOGGER = Logger.getLogger( IndexConverter.class.getName() );

	
		@Override
		public AstroItemDetail convert(Object data, RenderSky render) {
			Object[] obj = (Object[]) data;
			RenderSky.OBJECT objID = (OBJECT) obj[0];

			AstroItemDetail retValue = null;
				switch (objID) {
				 case STAR:
					 assert false; // NEVER!!!!!!
					 break;
				 case NOVA:
		
						retValue =	new AstroItemDetail(
										(String)obj[4] ,  	// name
										(LocationElement) obj[2] ,					// location
										(float) obj[3], 	// magnitude
										(String)obj[5] , // discover date (was angle)
										null, 
										null,
										AstroItemDetail.AstroItemType.NOVA,
										-1 //chartId
								);

							break;
				 case SUPERNOVA:
						retValue =	new AstroItemDetail(   // AstroItemDetail(String name, LocationElement location, float magnitude, String angle, String type, String constellation, AstroItemType itemType, int chartId)
										(String)obj[4] ,  	// name
										(LocationElement) obj[2] ,		// location
										(float) obj[3], 	// magnitude
										(String)obj[5] , // discover date (was angle)
										null, 
										null,
										AstroItemDetail.AstroItemType.SUPERNOVA,
										-1 //chartId
								);
							break;
				case DEEPSKY:
					LocationElement d = (LocationElement) obj[2];
					float[] angRadius = (float[])((((Object[])obj[5]))[1]);
	
					retValue =	new AstroItemDetail(
									(String)obj[4] ,
									d ,
									(float) obj[3], 
									Float.toString(angRadius[0]) + "x" + Float.toString(angRadius[1]),
									(String)((Object[])obj[5])[0] , 
									null,
									AstroItemDetail.AstroItemType.DSO,
									-1 //chartId
							);
					
					break;
				default:
					LOGGER.warning("non-dso object! found: " +	objID.toString());
				break;
			}
			
			return retValue;
		}

}
