/**
 * 
 */
package net.anc.atlas.index;

import jparsec.util.JPARSECException;
import net.anc.atlas.AstroItemDetail;

/**
 * @author Angelo Nicolini
 *
 */
public class NovaeIndexEntryBuilder implements IndexEntryBuilder {

	/* (non-Javadoc)
	 * @see net.anc.atlas.index.IndexEntryBuilder#compose(net.anc.atlas.AstroItemDetail)
	 */
	@Override
	public AstroItemDetail compose(AstroItemDetail item) throws JPARSECException {
		return item;
	}

}
