/**
 * 
 */
package net.anc.atlas.index;

import java.util.Arrays;
import java.util.List;

import com.google.inject.Inject;

import jparsec.ephem.Functions;
import jparsec.graph.DataSet;
import jparsec.time.AstroDate;
import jparsec.util.Translate;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.Index;
import net.anc.atlas.Table;
import net.anc.atlas.pages.BaseIndex;

/**
 * Index of Novae and Supernovae
 * 
 * @author Angelo Nicolini
 *
 */
public class NovaeAndSupernovaeIndex extends BaseIndex implements Index, Table {

	@Inject
	private AtlasGenerator generator;

	
	private String[] columnsName = {
			Translate.translate("Name"), 
			Translate.translate("Type"),
			Translate.translate("Right ascension") , //"\\makecell[l]{" + Translate.translate("Right ascension") +  "\\\\(J" + AtlasProperties.getProperties().getYear() + ".0)}",
			Translate.translate("Declination"),
			Translate.translate("Magnitude"), 
			Translate.translate("Date of observation"),
			Translate.translate("Constellation"),
			Translate.translate("Chart")
	};
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {

		for (Content next : content) {
			AstroItemDetail item = (AstroItemDetail)next;
			
			String dec = DataSet.replaceOne(Functions.formatDEC(item.getLatitude(), 0), "\"", "$\"$", 1);
			if( !dec.startsWith("-")) {
				dec = "+" + dec;
			}
			String magnitude =  Functions.formatValue(item.getMagnitude(), 1);
			if ("000.0".equals(magnitude)) {
				magnitude = "-";
			}
			
			String discoverDate = resolveDate(item.getAngle());
			
			String[] datai = new String[] {
					item.getName(),
					item.getItemType().toString(),
					Functions.formatRA(item.getLongitude(), 1),  
					dec,
					magnitude,
					discoverDate,
					item.getConstellation(),
					getFormattedChartIdRef(item.getChartId())
			};
			
			generator.writeRowInTable(datai, null, null, null);
			
		}
		generator.endLongTable(null);

	}
	
	/**
	 * this returns the reference to the owning Chart
	 * @param chartId
	 * @return
	 */
	protected String getFormattedChartIdRef(int chartId) {
		return "\\ref{subsec:" + Integer.toString(chartId + 1) + "}";
	}

	private String resolveDate(String discoverDate){
		
		String val[] = DataSet.toStringArray(discoverDate, " ", true);

		if( val.length == 1 ) {
			return Translate.translate(discoverDate.substring(0, 3), Translate.getDefaultLanguage()) +  " " + discoverDate.substring(3);
		}
		else if( val.length <= 2 ) {
			return discoverDate;
		}
		else {
			// var > 2
			int year = Integer.parseInt(val[0]);
			int month = 6;
			double day = 1;
//			if (val.length > 1 && !val[1].startsWith("M") && DataSet.isDoubleFastCheck(val[1])) month = Integer.parseInt(val[1]);
//			if (val.length > 2 && !val[2].startsWith("M") && DataSet.isDoubleFastCheck(val[2])) day = Double.parseDouble(val[2]);
			if (!val[2].startsWith("M") && DataSet.isDoubleFastCheck(val[2])) {
				day = Double.parseDouble(val[2]);
			}
			AstroDate astro = new AstroDate(year, month, day);
			return astro.toString();
		}

	}

	@Override
	public String[] getColumnsName() {
		return Arrays.copyOf(columnsName, columnsName.length);
	}

	@Override
	public String getId() {
		return "Nove.Index.title";
	}
		
}
	
