/**
 * 
 */
package net.anc.messier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import jparsec.astronomy.Constellation;
import jparsec.math.Constant;
import jparsec.time.AstroDate;
import jparsec.time.TimeElement;
import jparsec.time.TimeElement.SCALE;
import jparsec.time.TimeScale;
import jparsec.util.JPARSECException;
import jparsec.vo.SimbadElement;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.BungeeSkyAtlas;
import net.anc.atlas.Catalog;
import net.anc.atlas.MySkyAtlas;
import net.anc.atlas.OfflineSimbadCatalog;
import net.anc.atlas.Sky;

/**
 * @author nicolinia
 *
 */
public class MessierCatalog extends Catalog implements BungeeSkyAtlas {

	private static final Logger LOGGER = Logger.getLogger(MySkyAtlas.class.getName());
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.anc.atlas.BungeeSkyAtlas#create()
	 */
	@Override
	public void create() throws JPARSECException, IOException {
		startupPolicy.performOpertion();

		initialize();

		doPreamble();
		doMapsChapter();
		doChartsIndexChapter();
//		doIndexesChapter();		
		terminate();

	}

	/**
	 * build charts and create index
	 * 
	 * @param time
	 * @throws JPARSECException
	 * @throws IOException
	 */
	protected void buildMapsChapter(TimeElement time) throws JPARSECException, IOException {

		List<String> objects = new ArrayList<>();
		for (int i = 1; i <= 110; ++i) { // ATTENZIONE!!!
			objects.add("M" + i);
		}

		int messierChardId = 0;

		for (String object : objects) {
			LOGGER.log(Level.FINE, "creating map for chart " + object);

			SimbadElement retValue = OfflineSimbadCatalog.lookup(object);
			if (retValue == null) {
				throw new JPARSECException("The Object " + object + " cannot be found. Exiting");
			}

			AtlasProperties.getProperties().setSuffix("_chart" + (messierChardId++));

			Sky mySky = new DetailedMap();
			mySky.setCentralLongitude(retValue.getLocation().getLongitude() / Constant.DEG_TO_RAD);
			mySky.setCentralLatitude(retValue.getLocation().getLatitude() * Constant.RAD_TO_HOUR);
			renderAndRetrieveObjects(mySky, time, AtlasProperties.getProperties().getPDFAtlasFileName());

			mySky.setCentralLatitude(retValue.getLocation().getLatitude() / Constant.DEG_TO_RAD);
			mySky.setCentralLongitude(retValue.getLocation().getLongitude() * Constant.RAD_TO_HOUR);
			renderTheSky(mySky, time, AtlasProperties.getProperties().getPDFAtlasFileName());

		}

		LOGGER.log(Level.FINE, "All maps created");
		LOGGER.exiting(MySkyAtlas.class.getName(), "buildMapsChapter");

	}

	@Override
	protected Object[] getChartPageAsLine(String line) throws JPARSECException {
		Object[] retValue = super.getChartPageAsLine(line);

		SimbadElement messier = OfflineSimbadCatalog.lookup("M" + ((((Integer) retValue[1])).intValue() + 1));

		double jdUT = TimeScale.getJD(
				new TimeElement(
						new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1),
						SCALE.BARYCENTRIC_DYNAMICAL_TIME),
				AtlasProperties.getProperties().getLocation(), 
				Sky.getEphemeris(), 
				SCALE.UNIVERSAL_TIME_UTC
			);

		String mainConstelInMap = Constellation.getConstellation(Constellation
				.getConstellationName(messier.rightAscension, messier.declination, jdUT, Sky.getEphemeris()),
				Constellation.CONSTELLATION_NAME.LATIN);

		retValue[0] = mainConstelInMap;

		return retValue;
	}

}
