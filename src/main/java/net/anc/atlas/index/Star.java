package net.anc.atlas.index;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;

import jparsec.ephem.planets.EphemElement;
import jparsec.ephem.stars.StarElement;
import jparsec.ephem.stars.StarEphem;
import jparsec.graph.chartRendering.RenderSky;
import jparsec.io.ReadFile;
import jparsec.observer.City;
import jparsec.observer.ObserverElement;
import jparsec.time.AstroDate;
import jparsec.time.TimeElement;
import jparsec.util.JPARSECException;
import jparsec.vo.SimbadElement;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.OfflineSimbadCatalog;
import net.anc.atlas.Sky;

/**
 * 
 * This class creates an Index Entry from a rendered map object name.
 * It means it creates an index for main index file,  NOT for the atlas output file!
 * 
 * @author Angelo Nicolini
 *
 */
public 	class Star implements IndexConverter {

	private static final Logger LOGGER = Logger.getLogger( IndexConverter.class.getName() );

	private ReadFile sky2000 = null;

	private TimeElement time;
	private ObserverElement observer;
	
	public Star() {
		sky2000 = new ReadFile();
		sky2000.setFormat(ReadFile.FORMAT.JPARSEC_SKY2000);
		try {
			sky2000.setPath(StarEphem.PATH_TO_SkyMaster2000_JPARSEC_FILE);
			sky2000.readFileOfStars();
		} catch (JPARSECException e) {
			LOGGER.log(Level.SEVERE, "cannot load catalog", e);
			throw new RuntimeException(e);
		}
		try {
			time = new TimeElement((new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1, 12, 0, 0)).jd(), TimeElement.SCALE.TERRESTRIAL_TIME);
			observer = ObserverElement.parseCity(City.findCity("Milan"));
		} catch (JPARSECException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	@Nullable
	public AstroItemDetail convert(Object data, RenderSky renderer) {
		String targetName = (String) data;
		
		AstroItemDetail detail ;
		SimbadElement simbad  ;
		StarElement starElement = null;
		
		String catName;
		
		try {
			catName = StarEphem.getCatalogNameFromProperName(targetName);
			simbad = OfflineSimbadCatalog.lookup(catName);
		}
		catch (JPARSECException e) {
			LOGGER.log(Level.SEVERE, "cannot decode star " + targetName, e);
			throw new RuntimeException("conver failed, MUST STOP", e);
		}

		
		try {
			if( simbad == null ){
				LOGGER.finest("simbad failed for " + targetName + " (" + catName + ")") ;
	            int my_star = sky2000.searchByName(catName);
	            starElement  = (StarElement) sky2000.getReadElements()[my_star];
			}
			else {
//				OfflineSimbadCatalog.update(simbad);
				starElement = StarElement.parseSimbadElement(simbad);
			}

			EphemElement ephem = EphemElement.parseStarEphemElement(StarEphem.starEphemeris(time, observer, Sky.getEphemeris(), starElement, true));

			starElement.declination = ephem.declination;
			starElement.magnitude = ephem.magnitude;
			starElement.rightAscension = ephem.rightAscension;
			OfflineSimbadCatalog.update(new SimbadElement(starElement));
		
			if( !isInTheScreen( starElement.rightAscension, starElement.declination, renderer) ) {
				return null;
			}
		
			String type = starElement.type;
			if( StringUtils.isNotBlank(type)){
				type = type.replaceAll(",", ";");
			}
			
			detail = new 
					AstroItemDetail(
							targetName, 
							starElement.getEquatorialPosition(),
							starElement.magnitude,
							Double.toString(starElement.getDistance()),
							type,
							null,
							AstroItemDetail.AstroItemType.STAR,
							-1
							);
		
		} catch (JPARSECException e) {
			LOGGER.log(Level.SEVERE, "cannot decode star " + targetName, e);
			detail = null;
		}
		return detail;
		
		
	}
	
	private boolean isInTheScreen(double ra, double dec, RenderSky renderer) {

		double raSize  = AtlasProperties.getProperties().getScale().getRASize( renderer.render.centralLongitude, renderer.render.centralLatitude);
		double decSize = AtlasProperties.getProperties().getScale().getDECSize(renderer.render.centralLongitude, renderer.render.centralLatitude);
		
		double raSx = renderer.render.centralLongitude - raSize / 2;
		double raDx = renderer.render.centralLongitude + raSize / 2;
		
		double decDown = renderer.render.centralLatitude - decSize / 2;
		double decUp   = renderer.render.centralLatitude +  decSize / 2;

		return ((ra > raSx && ra < raDx ) && (dec > decDown && dec < decUp));

	}
}

