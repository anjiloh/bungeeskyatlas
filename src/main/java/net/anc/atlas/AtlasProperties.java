/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.StringUtils;

import jparsec.observer.City;
import jparsec.observer.ObserverElement;
import jparsec.util.JPARSECException;
import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;
import net.anc.atlas.serializer.GeneratorFormat;

/**
 * @author Angelo Nicolini
 *
 */
public final class AtlasProperties {

	private static final Logger LOGGER = Logger.getLogger(MySkyAtlas.class.getName());

	private static final AtlasProperties properties = new AtlasProperties();

	public static final double MM_PER_INCH = 25.4;
	
	private String atlasFileName;
	
	/**
	 * Set to false for the realistic 'Dark edition' background color: white or
	 * dark
	 */
	private boolean white;

	/**
	 * page size
	 */
	private PaperSize paperSize;

	/**
	 * generation year
	 */
	private int year;

	/**
	 * 
	 */
	private String suffix = "";

	/**
	 * 
	 */
	private String outputPath = "";

	/**
	 * Path to a cover image, or null
	 */
	private String pathCover = null;

	/**
	 * 
	 */
	private ObserverElement location;

	/**
	 * nr of degree to show in each page
	 */
	private Zoom scale;

	/**
	 * Pixel per Inch, default is 300
	 */
	private int pixelPerInch;

	/**
	 * template of the LaTeX generator
	 */
	private GeneratorFormat latexTemplate;

	/**
	 * Star upper magnitude
	 */
	private float starsLimitingMagnitude;
	/**
	 * DSO upper magnitude
	 */
	private float objectsLimitingMagnitude;

	/**
	 * Show the legend in maps
	 */
	private boolean showLegend;

	/**
	 * Version number. IT MUST BE DEFINED
	 */
	private String version;
	
	/**
	 * multivolume atlas, by default, false
	 */
	private boolean isMultiVolume = false;
	
	/**
	 * True if the atlas in a reflector one
	 */
	private boolean reflector = true;
	
	/**
	 * @return the isMultiVolume
	 */
	public boolean isMultiVolume() {
		return isMultiVolume;
	}

	/**
	 * @param isMultiVolume the isMultiVolume to set
	 */
	public void setMultiVolume(boolean isMultiVolume) {
		this.isMultiVolume = isMultiVolume;
	}

	private AtlasProperties() {

		InputStream inputStream = AtlasProperties.class.getClassLoader().getResourceAsStream("bungee.properties");
		Properties config = new Properties();
		try {
			config.load(inputStream);
			config.load(inputStream);
		} catch (IOException e2) {
			LOGGER.severe("Cannot load input properties, get the defalut values");
			inputStream = null;
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				// ignore
			}
		}
		if (inputStream == null) {

			config.setProperty("default.language", "ENGLISH");
			config.setProperty("background.color", "white");
			config.setProperty("paper.size", "A4");
			config.setProperty("target.year", "2018");
			config.setProperty("output.path", "d:/tmp/atlas/");

			config.setProperty("chart.scale", "25");
			config.setProperty("star.limits", "8.5");
			config.setProperty("object.limits", "14.0");
			config.setProperty("chart.legend", "YES");

			config.setProperty("charts.type.view", "refractor");
			config.setProperty("atlas.multivolume", "NO");


		}

		String defaultLanguage = StringUtils.defaultIfEmpty(config.getProperty("default.language"), "ENGLISH");
		LOGGER.fine("default.language = " + defaultLanguage);
		if ("ITALIAN".equalsIgnoreCase(defaultLanguage)) {
			Translate.setDefaultLanguage(Translate.LANGUAGE.ITALIAN);
		} else {
			Translate.setDefaultLanguage(Translate.LANGUAGE.ENGLISH);
		}
		String backgroundColor = StringUtils.defaultIfEmpty(config.getProperty("background.color"), "white");
		LOGGER.fine("background.color = " + backgroundColor);
		setWhite("white".equalsIgnoreCase(backgroundColor));
		try {
			setPathCover(Paths.get(this.getClass().getResource("/cover.jpg").toURI()).toFile().toString()
					.replaceAll("\\\\", "/"));
		} catch (URISyntaxException e1) {
			setPathCover(null);
		}

		String paperSize = StringUtils.defaultIfEmpty(config.getProperty("paper.size"), "A4");
		LOGGER.fine("paper.size = " + paperSize);

		if ("A4".equalsIgnoreCase(paperSize)) {
			setPaperSize(new A4());
		} else if ("A3".equalsIgnoreCase(paperSize)) {
			setPaperSize(new A3());
		}
		else {
			throw new RuntimeException("Unknow paperSize: " +paperSize);
		}
		String targetYear = StringUtils.defaultIfEmpty(config.getProperty("target.year"), "2018");
		LOGGER.fine("target.year = " + targetYear);

		setYear(Integer.parseInt(targetYear));

		setLatexTemplate(GeneratorFormat.BOOK);

		String chartTypeView = StringUtils.defaultIfEmpty(config.getProperty("charts.type.view"), "refractor");
		LOGGER.fine("charts.type.view = " + chartTypeView);
		setReflector("reflector".equalsIgnoreCase(chartTypeView));
		
		setPixelPerInch( NumberUtils.toInt(config.getProperty("chart.ppi"),300) );
		LOGGER.fine("chart.ppi = " + getPixelPerInch());

		
		String chartScale = StringUtils.defaultIfEmpty(config.getProperty("chart.scale"), "25");
		LOGGER.fine("chart.scale = " + chartScale);

		String outputPath = StringUtils.defaultIfEmpty(config.getProperty("output.path"), "d:/tmp/atlas/") + chartScale + "/" ;
		LOGGER.fine("output.path = " + outputPath);

		setOutputPath(outputPath + "\\");
		
	    final Path path = Paths.get(getOutputPath());
	    if(Files.notExists(path)){
	    	try {
				Files.createDirectories(path );
			} catch (IOException e) {
				throw new RuntimeException("directory does not exists and cannot be created!", e);
			}
	    }
		
		
		try {
			setLocation(ObserverElement.parseCity(City.findCity("Milan")));
		} catch (JPARSECException e) {
			throw new RuntimeException("cannot set observer", e);
		}

		String ver = config.getProperty("version");
		LOGGER.fine("version = " + ver);
		if( ver == null ) {
			throw new RuntimeException("version not defined, cannot start");
		}
		setVersion(ver);
		
		
		if ("25".equals(chartScale)) {
			if( "reflector".equalsIgnoreCase(chartTypeView)) {
				setScale(Zoom.VIEW_OF_25_DEG_NWT);
			}
			else {
				setScale(Zoom.VIEW_OF_25_DEG_DEF);
			}
			// setStarsLimitingMagnitude(8.5f);
			// setObjectsLimitingMagnitude(14.0f);
		} else if ("18".equals(chartScale)) {
			if( "reflector".equalsIgnoreCase(chartTypeView)) {
				setScale(Zoom.VIEW_OF_18_DEG_NWT);
			}
			else {
				setScale(Zoom.VIEW_OF_18_DEG_DEF);
			}
			// setStarsLimitingMagnitude(10.0f);
			// setObjectsLimitingMagnitude(15.0f);
		} else if ("15".equals(chartScale)) {
			if( "reflector".equalsIgnoreCase(chartTypeView)) {
				setScale(Zoom.VIEW_OF_15_DEG_NWT);
			}
			else {
				setScale(Zoom.VIEW_OF_15_DEG_DEF);
			}
			// setStarsLimitingMagnitude(10.0f);
			// setObjectsLimitingMagnitude(15.0f);
		}

		String starLimits = StringUtils.defaultIfEmpty(config.getProperty("star.limits"), "8.5");
		LOGGER.fine("star.limits = " + starLimits);

		setStarsLimitingMagnitude(Float.parseFloat(starLimits));

		String objectLimits = StringUtils.defaultIfEmpty(config.getProperty("object.limits"), "14.0");
		LOGGER.fine("object.limits = " + objectLimits);

		setObjectsLimitingMagnitude(Float.parseFloat(objectLimits));

		String chartLegend = StringUtils.defaultIfEmpty(config.getProperty("chart.legend"), "YES");
		LOGGER.fine("chart.legend = " + chartLegend);
		setShowLegend("YES".equalsIgnoreCase(chartLegend));
		
		String multiVol = StringUtils.defaultIfEmpty(config.getProperty("atlas.multivolume"), "NO");
		LOGGER.fine("atlas.multivolume = " + multiVol);
		setMultiVolume("YES".equalsIgnoreCase(multiVol));
		
		

		setAtlasFileName();
	}

	public boolean isWhite() {
		return white;
	}

	public final void setWhite(boolean white) {
		this.white = white;
	}

	/**
	 * @return the properties
	 */
	public static AtlasProperties getProperties() {
		return properties;
	}

	/**
	 * @return the paperSize
	 */
	public PaperSize getPaperSize() {
		return paperSize;
	}

	/**
	 * @param paperSize
	 *            the paperSize to set
	 */
	public void setPaperSize(PaperSize paperSize) {
		this.paperSize = paperSize;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the location
	 */
	public ObserverElement getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(ObserverElement location) {
		this.location = location;
	}

	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * @param suffix
	 *            the suffix to set
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	/**
	 * @return the outputPath
	 */
	public String getOutputPath() {
		return outputPath;
	}

	/**
	 * @param outputPath
	 *            the outputPath to set
	 */
	private void setOutputPath(String outputPath) {
		this.outputPath = outputPath.replaceAll("\\\\", "/");
	}

	/**
	 * @return the pathCover
	 */
	public String getPathCover() {
		return pathCover;
	}

	/**
	 * @param pathCover
	 *            the pathCover to set
	 */
	public final void setPathCover(String pathCover) {
		this.pathCover = pathCover;
	}

	/**
	 * @return the pixelPerInch
	 */
	public int getPixelPerInch() {
		return pixelPerInch;
	}

	/**
	 * @param pixelPerInch
	 *            the pixelPerInch to set
	 */
	public void setPixelPerInch(int pixelPerInch) {
		this.pixelPerInch = pixelPerInch;
	}

	/**
	 * @return the scale
	 */
	public Zoom getScale() {
		return scale;
	}

	/**
	 * @param scale
	 *            the scale to set
	 */
	public void setScale(Zoom scale) {
		this.scale = scale;
	}

	/**
	 * @return the latexTemplate
	 */
	public GeneratorFormat getLatexTemplate() {
		return latexTemplate;
	}

	/**
	 * @param latexTemplate
	 *            the latexTemplate to set
	 */
	public void setLatexTemplate(GeneratorFormat latexTemplate) {
		this.latexTemplate = latexTemplate;
	}

	/**
	 * @return the starsLimitingMagnitude
	 */
	public float getStarsLimitingMagnitude() {
		return starsLimitingMagnitude;
	}

	/**
	 * @param starsLimitingMagnitude
	 *            the starsLimitingMagnitude to set
	 */
	public void setStarsLimitingMagnitude(float starsLimitingMagnitude) {
		this.starsLimitingMagnitude = starsLimitingMagnitude;
	}

	/**
	 * @return the objectsLimitingMagnitude
	 */
	public float getObjectsLimitingMagnitude() {
		return objectsLimitingMagnitude;
	}

	/**
	 * @param objectsLimitingMagnitude
	 *            the objectsLimitingMagnitude to set
	 */
	public void setObjectsLimitingMagnitude(float objectsLimitingMagnitude) {
		this.objectsLimitingMagnitude = objectsLimitingMagnitude;
	}

	/**
	 * @return the showLegend
	 */
	public boolean isShowLegend() {
		return showLegend;
	}

	/**
	 * @param showLegend
	 *            the showLegend to set
	 */
	public void setShowLegend(boolean showLegend) {
		this.showLegend = showLegend;
	}

	/**
	 * @return the atlasFileName (full path). If the current generation is a single chart, it returns the name of that chart file name 
	 */
	public String getPDFAtlasFileName() {
		if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH ) {
			return atlasFileName + getSuffix() + "_ENG.pdf";
		}
		else {
			return atlasFileName + getSuffix() + ".pdf";
		}
	}

	/**
	 * @return the atlasFileName (full path)
	 */
	public String getLaTeXAtlasFileName() {
		if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH ) {
			return atlasFileName + "_ENG.tex";
		}
		else {
			return atlasFileName + ".tex";
		}
	}

	
	/**
	 * full path
	 * @param atlasFileName the atlasFileName to set
	 */
	private void setAtlasFileName() {
		this.atlasFileName = getOutputPath() + "BungeeSkyAtlas_" +getScale().getFieldSize() + "d" + "_" + getPaperSize().getName().toUpperCase();
		if( !isWhite() ){
			this.atlasFileName += "_D";
		}

	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	private void setVersion(String version) {
		this.version = version;
	}

	
	public String getNorthMinimapFilename() {
		if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH ) {
			return getOutputPath() + "BungeeSkyAtlas_NP" +getScale().getFieldSize() + "d" + "_" + getPaperSize().getName().toUpperCase() + "_ENG.pdf";
		}
		else {
			return getOutputPath() + "BungeeSkyAtlas_NP" +getScale().getFieldSize() + "d" + "_" + getPaperSize().getName().toUpperCase() + ".pdf";
		}
		
	}

	public String getSouthMinimapFilename() {
		if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH ) {
			return getOutputPath() + "BungeeSkyAtlas_SP" +getScale().getFieldSize() + "d" + "_" + getPaperSize().getName().toUpperCase() + "_ENG.pdf";
		}
		else {
			return getOutputPath() + "BungeeSkyAtlas_SP" +getScale().getFieldSize() + "d" + "_" + getPaperSize().getName().toUpperCase() + ".pdf";
		}
	}

	/**
	 * @return the atlas is a reflector one
	 */
	public boolean isReflector() {
		return reflector;
	}

	private void setReflector(boolean reflector) {
		this.reflector = reflector;
	}

}
