package net.anc.atlas;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jparsec.io.FileIO;
import net.anc.atlas.EmisphereMinimap.Entry;
import net.anc.atlas.pages.MinimapsChartsPage;

public class SkyMapComposerTest {

	private String centerX;
	private String centerY;

	@Before
	public void setUp() throws Exception {
		if( AtlasProperties.getProperties().getPaperSize() instanceof A3 ) {
			centerX = "20.5";
			centerY = "14.2";
		}
		else {
			centerX = "14.5";
			if( AtlasProperties.getProperties().isReflector() ) {
				centerY = "9.75";
			}
			else {
				centerY = "10.02";
			}
			
		}
	}

	@Test
	public void testCreate() {
		assertEquals(AtlasProperties.getProperties().getScale().getRa().length, AtlasProperties.getProperties().getScale().getDec().length);
		
		SkyMapComposer composer= new SkyMapComposer();
		composer.create();
		
		List<SkyMap> skyMap = SkyMapComposer.getSkyMap();
		
		for (SkyMap map : skyMap) {
			System.out.print(map);
			System.out.print(" chartid -> " + map.getChartId() );
			System.out.print(": N -> " + map.north.toString());
			System.out.print(", S -> " + map.south.toString());
			System.out.print(", E -> " + map.east.toString());
			System.out.print(", O -> " + map.west.toString());
			System.out.println(" ");
			
		}

	}

//	@Test
//	public void testCreateForMinimap() {
//		
//		ChartReferenceGenerator refGenerator = new ChartReferenceGenerator();
//		
//		SkyMapComposer composer= new SkyMapComposer();
//		composer.create();
//		refGenerator.northEmisphere();
////		refGenerator.southEmisphere();
//	}	
//	
	
	@Test 
	public void testCreateNorthForMinimap() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		MinimapsChartsPage page = new MinimapsChartsPage(); 
		Field cx = MinimapsChartsPage.class.getDeclaredField("centerX");
		cx.setAccessible(true);
		centerX = (String) cx.get(page);

		Field cy = MinimapsChartsPage.class.getDeclaredField("centerY");
		cy.setAccessible(true);
		centerY = (String) cy.get(page);
		
		SkyMapComposer composer= new SkyMapComposer();
		composer.create();
		
		NorthernEmisphereMinimap north = new NorthernEmisphereMinimap();
		north.compile();

		List<Content> results = north.getCoordinates();
		for (Content coordinates : results) {
			StringBuilder code = new StringBuilder();
			EmisphereMinimap.Entry entry = (Entry) coordinates;
			code.append("\\node[xshift=").append(centerX).append("cm, yshift=").append(centerY).append("cm] (a) at (");
			code.append(entry.theta).append(':').append(entry.ro);
			code.append(") {\\mmcircled{\\ref{subsec:").append(entry.chartId).append("}}};");
			code.append(FileIO.getLineSeparator()) ;
			System.out.print(code.toString());
		}	
	}
	
	
//	class ChartReferenceGenerator {
////		private final double[] step = {2.3, 2.3, 2.3, 2.9, 3.0};   //25 gradi
//		private final double[] step = {1.2, 1.4, 1.5, 2.2, 2.4};   //25 gradi A4
////		private final double[] step = {1.4, 1.4, 1.8, 1.9, 2.1, 2.1, 2.1};   // 18 gradi
////		private final double[] step = {1.4, 1.4, 1.5, 1.6, 1.6, 2.0, 2.1};   // 15 gradi
//		
//		public void northEmisphere() {
//
//			List<SkyMap> skyMap = SkyMapComposer.getSkyMap();
//			
//			
//			skyMap.sort( (chart1, chart2) -> {
//				if( chart1.dec == chart2.dec ) {
//					return Double.compare(chart1.ra, chart2.ra);
//				}
//				else {
//					return -1 * Double.compare(chart1.dec, chart2.dec);
//				}
//			});
//			
//			double currentDEC = 0;
//			int id = 0;
//			double ro = 0;
//			for (SkyMap map : skyMap) {
//				StringBuilder circle = new StringBuilder();
//				if( map.dec == 90 && map.ra == 0) {
//					circle.append("\\node[xshift=").append(centerX).append("cm, yshift=").append(centerY).append("cm] (a) at (0,0) ");
//					circle.append(" {\\circled{").append(map.getChartId() + 1).append("}};");
//					ro = 0;
//				}
//				else {
//					if( map.dec < -1) {  // TODO: 25: 0, 18 ha -1, da verificare 
//						System.out.println("finito");
//						break;
//					}
//					if( currentDEC != map.dec) {
//						ro += step[id];
//						id++;
//						currentDEC = map.dec;
//					}
//					
//					circle.append("\\node[xshift=").append(centerX).append("cm, yshift=").append(centerY).append("cm] (a) at ");
//					circle.append("(").append(map.getTetha()).append(":").append( ro ).append(")");
////					circle.append(" {\\\\mmcircled{").append(map.getChartId() + 1).append("}};");
//					circle.append(" {\\mmcircled{\\ref{subsec:").append(map.getChartId() + 1).append("}}};");
//
//				}
//				System.out.println(circle);
//			}
//			
//		}
//		
//		public void southEmisphere() {
//			List<SkyMap> skyMap = SkyMapComposer.getSkyMap();
//			
//			skyMap.sort( (chart1, chart2) -> {
//				if( chart1.dec == chart2.dec ) {
//					return Double.compare(chart1.ra, chart2.ra);
//				}
//				else {
//					return Double.compare(chart1.dec, chart2.dec);
//				}
//			});
//			
//			
//			double currentDEC = 0;
//			int id = 0;
//			double ro = 0;
//			for (SkyMap map : skyMap) {
//				StringBuilder circle = new StringBuilder();
//				if( map.dec == -90 && map.ra == 0) {
//					circle.append("\\node[xshift=20.500000cm, yshift=14.2000cm] (a) at (0,0) ");
//					circle.append(" {\\circled{").append(map.getChartId() + 1).append("}};");
//					ro = 0;
//				}
//				else {
//					if( map.dec > 0) {
//						System.out.println("finito");
//						break;
//					}
//					else if( currentDEC != map.dec) {
//						ro += step[id];
//						id++;
//						currentDEC = map.dec;
//					}
//					circle.append("\\node[xshift=20.500000cm, yshift=14.2000cm] (a) at ");
//					circle.append("(").append(-1 * map.getTetha()).append(":").append( ro ).append(")");
//					circle.append(" {\\circled{").append(map.getChartId() + 1).append("}};");
//				}
//				System.out.println(circle);
//			}
//
//		}
//	}
	
}
