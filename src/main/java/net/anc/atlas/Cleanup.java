/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

/**
 * @author Angelo Nicolini
 *
 */
public class Cleanup implements Startup {
	
	private static final Logger LOGGER = Logger.getLogger( MySkyAtlas.class.getName() );

	@Override
	public void performOpertion() {
		RecoveryPoint.cleanUp();
		try {
			Files.delete( Paths.get(BungeeSkyAtlas.OBJECTS_INDEX_FILE) );
		}
		catch(IOException e){
			LOGGER.fine("delete of Index failed");
		}
		try {
			Files.delete( Paths.get(BungeeSkyAtlas.STARS_INDEX_FILE) );
		}
		catch(IOException e){
			LOGGER.fine("delete of Index failed");
		}
		try {
			Files.delete( Paths.get(BungeeSkyAtlas.NOVAE_INDEX_FILE) );
		}
		catch(IOException e){
			LOGGER.fine("delete of Index failed");
		}
		
		
		
	}
	
	

}
