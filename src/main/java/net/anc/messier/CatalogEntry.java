/**
 * 
 */
package net.anc.messier;

import jparsec.vo.SimbadElement;
import net.anc.atlas.Content;

/**
 * @author Angelo Nicolini
 *
 */
public class CatalogEntry implements Content {

	private String name;
	private SimbadElement entry;
	
	
	/**
	 * @param name
	 * @param entry
	 */
	public CatalogEntry(String name, SimbadElement entry) {
		super();
		this.name = name;
		this.entry = entry;
	}


	/* (non-Javadoc)
	 * @see net.anc.atlas.Content#size()
	 */
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 1;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public SimbadElement getEntry() {
		return entry;
	}


	public void setEntry(SimbadElement entry) {
		this.entry = entry;
	}

}
