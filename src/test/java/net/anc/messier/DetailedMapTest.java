package net.anc.messier;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import javax.annotation.Nullable;

import org.junit.Test;

import com.itextpdf.awt.FontMapper;
import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import jparsec.graph.chartRendering.RenderPlanet;
import jparsec.graph.chartRendering.SkyRenderElement;
import jparsec.graph.chartRendering.frame.SkyRendering;
import jparsec.math.Constant;
import jparsec.time.AstroDate;
import jparsec.time.TimeElement;
import jparsec.time.TimeElement.SCALE;
import jparsec.time.TimeScale;
import jparsec.util.DataBase;
import jparsec.util.JPARSECException;
import jparsec.vo.SimbadElement;
import jparsec.vo.SimbadQuery;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.MyAtlasFontMapper;
import net.anc.atlas.Sky;

public class DetailedMapTest {

	@Test
	public void testM42() throws JPARSECException {
		AstroDate astro = new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1);

		TimeElement time = new TimeElement(astro, SCALE.BARYCENTRIC_DYNAMICAL_TIME);
		time = new TimeElement(
				TimeScale.getJD(time, AtlasProperties.getProperties().getLocation(), Sky.getEphemeris(), SCALE.UNIVERSAL_TIME_UTC), 
				SCALE.UNIVERSAL_TIME_UTC
			);
		
		Sky minimap = new DetailedMap();
		SimbadElement retValue = SimbadQuery.query("Herschel 400");		

		System.out.println(retValue.getLocation().getLatitude());
		System.out.println(retValue.getLocation().getLongitude());
		
		minimap.setCentralLatitude(retValue.getLocation().getLatitude() / Constant.DEG_TO_RAD);
		minimap.setCentralLongitude(retValue.getLocation().getLongitude() * Constant.RAD_TO_HOUR);
		renderTheSky(minimap, time, "c:\\tmp\\M42.pdf");
	}

	/**
	 *	
	 *Generate sky rendering
	 * @param mySky
	 * @param time
	 * @return
	 */
	@Nullable
	private List<Content> renderTheSky(Sky mySky, TimeElement time, String filename) {
		List<Content> data = null;
		try {
			SkyRenderElement sky = mySky.getRender();
			RenderPlanet.dateChanged();
			SkyRenderElement skyElement = sky.clone();
			int yMargin = 0;

			SkyRendering skyRender = new SkyRendering(
					time, 
					AtlasProperties.getProperties().getLocation(), 
					Sky.getEphemeris(),
					skyElement, 
					"Sky render", 
					yMargin
				);
			
			skyRender.getRenderSkyObject().setYCenterOffset(1);
			exportToPDFUsingiText(
					skyRender, 
					new Dimension(skyElement.width, skyElement.height),
					filename,
					null
				); // pic.getImage());

			DataBase.clearEntireDatabase();
			
		} catch (Exception ve) {
			ve.printStackTrace();
		}
		return data;

	}
	private void exportToPDFUsingiText(SkyRendering skyRender, Dimension size, String plotFile, BufferedImage image) throws IOException {
		int factor = 1;

		size = new Dimension(Double.valueOf(0.5 + (size.width+0*2)/(double)factor).intValue(), Double.valueOf(0.5 + (size.height+0*2)/(double)factor).intValue());
		com.itextpdf.text.Rectangle pageSize = new com.itextpdf.text.Rectangle(0, 0, size.width, size.height);
		Document document = new Document(pageSize);
		FontMapper mapper = new MyAtlasFontMapper();
		try ( OutputStream	fos = Files.newOutputStream(Paths.get(plotFile), StandardOpenOption.CREATE) ) {
			PdfWriter writer = PdfSmartCopy.getInstance(document, fos);
			writer.setCompressionLevel(9);
			writer.setFullCompression();

			document.open();
			
			// Create a custom font mapper that forces the use of the fonts we
			// want

			PdfContentByte canvas = writer.getDirectContent();
			PdfTemplate template = canvas.createTemplate(size.width, size.height);
			canvas.addTemplate(template, 0, 0);

			if (image != null) {
				assert false; // "never invoked";
				
				
			} else {
				// Vector graphics
    	        Graphics2D g2  = new PdfGraphics2D(template, (float)size.width, (float)size.height, mapper, false, false, 1f);
				g2.setColor(Color.WHITE);
				g2.fillRect(0, 0, size.width, size.height);
				g2.scale(1.0 / (double) factor, 1.0 / (double) factor);
				g2.translate(0,0);
				skyRender.paintChart(g2);

				g2.dispose();
				
			}

			document.close();
			fos.flush();
//			return details;
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		finally {
//			RecoveryPoint.checkPoint(plotFile, mySky.getCentralLongitude(), mySky.getCentralLatitude());
		}
//		return null;
	}

}
