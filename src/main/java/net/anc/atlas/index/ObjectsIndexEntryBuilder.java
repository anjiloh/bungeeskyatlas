package net.anc.atlas.index;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import jparsec.graph.DataSet;
import jparsec.graph.chartRendering.RenderSky;
import jparsec.util.JPARSECException;
import jparsec.util.Translate;
import jparsec.vo.SimbadElement;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.Messages;
import net.anc.atlas.OfflineSimbadCatalog;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public class ObjectsIndexEntryBuilder implements IndexEntryBuilder {
	
	
	private static Map<String, String> problematicObjects = new HashMap<String, String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1207057114257977608L;

		{
			put("382", "IC 382");
			put("423", "IC 423");
			put("5679D", "MCG 1-37-36");
			put("5619C", "MCG 1-37-13");
			put("7764A-3", "MCG -7-1-1");
			put("3010C", "MCG 7-20-67");
			put("4187C", "MCG 9-20-118");
			put("3697C", "MCG 4-27-44");
			
			put("1646-3","PGC 3084954");
			put("4728C", "PGC 214033");
			put("5745-3", "NGC 5745");
		}
	};
	
	public  AstroItemDetail compose(AstroItemDetail item) throws JPARSECException {
		String objName = getNormalizedName(item.getName());
		SimbadElement simbad = null;
	
		String queryName = RenderSky.searchDeepSkyObjectReturnMainName(objName);
		
		if( queryName == null ) {
			queryName = objName;
		}
		
		String completeName = null;
		
		if( queryName.endsWith("-1") ||queryName.endsWith("-2")  ||queryName.endsWith("A") ||queryName.endsWith("B") ) {
			completeName = queryName;
			queryName = getNormalizedName( item.getName() );
		}
		else {
			if( isProblematic(item.getName()) ) {
				completeName = item.getName();
			}
		}
		
		simbad = SimbadElement.searchDeepSkyObject(queryName);
		if( simbad == null ) {
			simbad = OfflineSimbadCatalog.lookup(queryName);
		}
		
		
		if( simbad == null ) {
			throw new JPARSECException("cannot find: " + objName  );
		}
		else {
			
			if( simbad.otherNames == null ) {
				simbad.otherNames = new String[] {  completeName };
			}
			else {
				simbad.otherNames = buildOtherNames(simbad.otherNames, completeName);
				
			}
			
			// fix some strange inversion
			if( StringUtils.isNotEmpty(simbad.otherNames[0]) ) {
				if( simbad.otherNames[0].contains("NGC") &&  !simbad.name.contains("NGC")) {
					 String on = simbad.otherNames[0];;
					 simbad.otherNames[0] = simbad.name;
					 simbad.name = on;
				}
			}
			
			
			OfflineSimbadCatalog.update(simbad);
		}

		item.setName( simbad.name );
		StringBuilder extraInfo = new StringBuilder();

		if( simbad.otherNames != null ) {
			for (String extraName : simbad.otherNames) {
				if( StringUtils.isNotEmpty(extraName)) {
					extraInfo.append(", ").append(extraName);
				}
			}
		}
		if( StringUtils.isNotEmpty(simbad.spectralType  ) ) {
			extraInfo.append(Translate.translate("Spectral type")).append(": ").append(simbad.spectralType);
		}

		if( simbad.bMinusV != SimbadElement.B_MINUS_V_UNAVAILABLE && simbad.bMinusV != 0) {
			extraInfo.append(", B-V: ").append(simbad.bMinusV);
		}
		if( simbad.properMotionRA != 0 ) {
			extraInfo.append(", dRA: ").append(simbad.properMotionRA).append( "(rad/yr)");
		}
		if( simbad.properMotionDEC != 0 ) {
			extraInfo.append(", dDEC: ").append(simbad.properMotionDEC).append( "(rad/yr)");
		}
		if( simbad.properMotionDEC != 0 ) {
			extraInfo.append(", dVr: ").append(simbad.properMotionRadialV).append( "(km/s)");
		}
		if( simbad.parallax != 0 ) {
			extraInfo.append(Translate.translate("Parallax")).append(simbad.parallax).append( "(mas)");
		}
		if( extraInfo.toString().startsWith(", ")) {
			extraInfo.delete(0, 2);
		}
		
		item.setExtraInfo( Messages.getObjectDescriptionTransation(extraInfo.toString()) );
		
		return item;
	}

	
	private String[] buildOtherNames(String[] source, String completeName) {
		if( completeName == null ) {
			return source;
		}
		
		for( int i = 0; i < source.length; ++i) {
			if( completeName.equals(source[i])) {
				return source;
			}
			else if( StringUtils.isBlank(source[i])) {
				source[i] = completeName;
				return source;
			}
		}
		String[] newArray = new String[source.length + 1];
		System.arraycopy(source, 0, newArray, 0, newArray.length);
		newArray[source.length] = completeName;
		return newArray;

	}


	private static String getNormalizedName( final String originalName ) {
		String objName = originalName; 
		int caldwell = originalName.indexOf("CALDWELL") ;
		if( caldwell > 0 ) {
			objName = originalName.substring(0, caldwell - 1);
		}
		
		String solvedName = problematicObjects.get(objName);
		if( solvedName != null ) {
			objName = solvedName;
		}
		
		objName = DataSet.replaceAll(objName, "I.", "IC ", true);
		return objName;
	}
	
	
	
	private static boolean isProblematic( final String originalName ) {
		String objName = originalName; 
		int caldwell = originalName.indexOf("CALDWELL") ;
		if( caldwell > 0 ) {
			objName = originalName.substring(0, caldwell - 1);
		}
		
		return  problematicObjects.containsKey(objName);
	}
}
