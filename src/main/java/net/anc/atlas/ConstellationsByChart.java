/**
 * 
 */
package net.anc.atlas;

/**
 * @author Angelo Nicolini
 *
 */
public class ConstellationsByChart implements Content {

	/**
	 * @param constellation
	 * @param chartId
	 */
	public ConstellationsByChart(String constellation, int chartId) {
		super();
		this.constellation = constellation;
		this.chartId = chartId;
	}
	private String constellation;
	private int chartId;
	/**
	 * @return the constellation
	 */
	public String getConstellation() {
		return constellation;
	}
	/**
	 * @param constellation the constellation to set
	 */
	public void setConstellation(String constellation) {
		this.constellation = constellation;
	}
	/**
	 * @return the chartId
	 */
	public int getChartId() {
		return chartId;
	}
	/**
	 * @param chartId the chartId to set
	 */
	public void setChartId(int chartId) {
		this.chartId = chartId;
	}
	
	/**
	 * this returns 2
	 */
	@Override
	public int size() {
		return 2;
	}
	
}
