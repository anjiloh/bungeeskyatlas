/**
 * 
 */
package net.anc.atlas.serializer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import com.itextpdf.awt.FontMapper;
import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfSmartCopy;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import jparsec.graph.chartRendering.frame.SkyRendering;
import net.anc.atlas.MyAtlasFontMapper;

/**
 * @author nicolinia
 *
 */
public class ExportChartToPDFUsingIText implements ExportChart {

	private int marginX;
	private int marginY;

	/* (non-Javadoc)
	 * @see net.anc.atlas.serializer.ExportChart#export(jparsec.graph.chartRendering.frame.SkyRendering, java.awt.Dimension, java.lang.String, boolean, java.awt.image.BufferedImage)
	 */
	@Override
	public void export(SkyRendering skyRender, Dimension size, String plotFile) {
		int factor = 1;

		size = new Dimension(Double.valueOf(0.5 + (size.width+marginX*2)/(double)factor).intValue(), Double.valueOf(0.5 + (size.height+marginY*2)/(double)factor).intValue());
		Rectangle pageSize = new Rectangle(0, 0, size.width, size.height);
		Document document = new Document(pageSize);
		FontMapper mapper = new MyAtlasFontMapper();
		try ( OutputStream	fos = Files.newOutputStream(Paths.get(plotFile), StandardOpenOption.CREATE) ) {
			PdfWriter writer = PdfSmartCopy.getInstance(document, fos);
			writer.setCompressionLevel(9);
			writer.setFullCompression();

			document.open();
			
			// Create a custom font mapper that forces the use of the fonts we
			// want

			PdfContentByte canvas = writer.getDirectContent();
			PdfTemplate template = canvas.createTemplate(size.width, size.height);
			canvas.addTemplate(template, 0, 0);

				// Vector graphics
//    	        LOGGER.log(Level.FINEST, "size.width : " + size.width + ", size.height: " + size.height);
	        Graphics2D g2  = new PdfGraphics2D(template, (float)size.width, (float)size.height, mapper, false, false, 1f);
			g2.setColor(Color.WHITE);
			g2.fillRect(0, 0, size.width, size.height);
			g2.scale(1.0 / (double) factor, 1.0 / (double) factor);
			g2.translate(marginX, marginY);
			skyRender.paintChart(g2);
			g2.dispose();

			document.close();
			fos.flush();
		} catch (Exception exc) {
//	        LOGGER.log(Level.SEVERE, "problem while exportToPDFUsingiText", exc);
		}
		finally {
		}

	}

	@Override
	public void initialize(int marginX, int marginY) {
		this.marginX = marginX;
		this.marginY = marginY;
	}

}
