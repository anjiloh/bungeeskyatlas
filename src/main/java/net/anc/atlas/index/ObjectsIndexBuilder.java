package net.anc.atlas.index;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.inject.Inject;

import jparsec.util.JPARSECException;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AstroItemDetail.AstroItemType;
import net.anc.atlas.Content;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public class ObjectsIndexBuilder implements IndexBuilder {

	@Inject
	private IndexComposerFactory<IndexComposer> builderFactory;

	@Override
	public List<Content> getIndex(Stream<String> source, int pageId) {
		List<Content> list = source
				.filter(line -> Integer.toString(pageId).equals(line.substring(line.lastIndexOf(',') + 1)))
				.map(AstroItemDetail::valueOf).filter(item -> item.getItemType() == AstroItemType.DSO)
				.sorted(new Comparator<AstroItemDetail>() {

					@Override
					public int compare(AstroItemDetail o1, AstroItemDetail o2) {
						if (o1.getMagnitude() == o2.getMagnitude()) {
							return o1.getName().compareTo(o2.getName());
						} else {
							return Float.compare(o1.getMagnitude(),o2.getMagnitude() );
						}
					}
				}).collect(Collectors.toList());

		list.forEach(item -> {
			try {
				((IndexEntryBuilder)builderFactory.create(IndexEntryBuilder.OBJECTS)).compose((AstroItemDetail)item);
			} catch (JPARSECException e) {
				throw new RuntimeException("cannot compose " + ((AstroItemDetail) item).getName(), e);
			}
		});

		return list;
	}

}
