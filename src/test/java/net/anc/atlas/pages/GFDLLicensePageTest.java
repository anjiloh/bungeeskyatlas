package net.anc.atlas.pages;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.TestModule;

public class GFDLLicensePageTest {
	
	protected Injector injector = Guice.createInjector(
			new TestModule(),
			new AbstractModule() {
		        @Override
		        protected void configure() {
		            bind(LicensePage.class).to(GFDLLicensePage.class);
		        }
			}
			
			);

	@Inject
	protected AtlasGenerator generator;
	
	@Inject
	private LicensePage subject; 
	
	
	@Before
	public void setUp() throws Exception {
        injector.injectMembers(this);
	}

//	@Test
//	public void testWriteContent() {
//		fail("Not yet implemented"); // TODO
//	}

	@Test
	public void testSetTitle() {
		subject.setTitle("test title");
		
		assertTrue( generator.getCode().contains("test title"));
		assertTrue( generator.getCode().contains("\\chapter*{!test title!}"));
		
		assertFalse( generator.getCode().contains("Permission  is  granted  to  copy"));
		assertFalse( generator.getCode().contains("Indice"));
		
		
//		subject.writeContent(null);
//		System.out.println(generator.getCode());
		
		
	}

}
