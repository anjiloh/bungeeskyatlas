/**
 * 
 */
package net.anc.atlas;

/**
 * I18n of the atlas
 * 
 * @author Angelo Nicolini
 *
 */
public class MessageContent implements Content {

	private String content;

	public MessageContent(String content) {
		this.content = content;
	}
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Content#size()
	 */
	@Override
	public int size() {
		return 1;
	}
	
	public String toString() {
		return content;
	}


}
