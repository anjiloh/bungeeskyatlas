package net.anc.atlas;

import java.util.List;

/**
 * 
 * This implements the atlas by creating its pages 
 * 
 * @author Angelo Nicolini
 *
 */
public interface Atlas {

	/**
	 * The Main Atlas Title
	 * @return
	 */
	String getAtlasTitle();
	
	/**
	 * Create a header page
	 */
	void writeHeaderPage();

	/**
	 * Create a presentation page
	 */
	void writePresentationPage();

	/**
	 * Create a credits page
	 */
	void writeCreditsPage();

	/**
	 * Create a legend page
	 */
	void writeLegendPage();

	/**
	 * Create a license page
	 */
	void writeLicensePage();

	/**
	 * Writes a list of charts
	 * @param list the list of charts to add to the atlas
	 */
	void writeChartPages(List<Object[]> list);

	/**
	 * Start a section in the atlas composed by index
	 */
	void startIndexSection();

	/**
	 * start of the atlas: title and additional packages 
	 */
	void startAtlas();

	/**
	 * Terminate a section in the atlas composed by index
	 */
	void endIndexSection();

	/**
	 * write a SINGLE page!
	 * @param chartId
	 * @param data
	 * @param indexPage
	 */
	void writeIndexPage(int chartId, List<Content> data, Index indexPage);

	/**
	 * Complete the atlas
	 */
	void endAtlas();

	/**
	 * used to serialize the .tex document...
	 */
	String toString();

	/**
	 * Create an history page
	 */
	void writeHistoryPage();
	
	/**
	 * write minimaps index
	 */
	void writeMinimap();

}