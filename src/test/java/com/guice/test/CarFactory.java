package com.guice.test;

import com.google.inject.assistedinject.Assisted;

public interface CarFactory<T extends Car> {

	T create(@Assisted String part);
}


interface ToyotaFactory extends CarFactory<Toyota> {
	
//	Toyota create(@Assisted("part") String part);

}
