package net.anc.atlas;

import org.junit.Before;
import org.junit.Test;

import jparsec.ephem.EphemerisElement;
import jparsec.ephem.Precession;
import jparsec.ephem.Target.TARGET;
import jparsec.ephem.planets.EphemElement;
import jparsec.ephem.stars.StarElement;
import jparsec.ephem.stars.StarEphem;
import jparsec.io.ConsoleReport;
import jparsec.math.Constant;
import jparsec.observer.City;
import jparsec.observer.LocationElement;
import jparsec.observer.ObserverElement;
import jparsec.time.AstroDate;
import jparsec.time.TimeElement;
import jparsec.util.JPARSECException;
import jparsec.vo.SimbadElement;
import jparsec.vo.SimbadQuery;


/**
 * This compares the initalization of EphemerisElement in two different way
 * @author Angelo Nicolini
 *
 */
public class Ephemeris {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSuggestedFormat() throws JPARSECException {
		double J2018 = Constant.J2000 + 18 * 365.25;
		System.out.println(J2018);
		TimeElement time = new TimeElement(J2018, TimeElement.SCALE.TERRESTRIAL_TIME);
		ObserverElement observer = ObserverElement.parseCity(City.findCity("Milan")); // irrelevant
//		for geocentric positions
		EphemerisElement eph = new EphemerisElement(TARGET.NOT_A_PLANET,
		EphemerisElement.COORDINATES_TYPE.ASTROMETRIC,
		                J2018, false, EphemerisElement.REDUCTION_METHOD.IAU_2006,
		                EphemerisElement.FRAME.ICRF);
		eph.optimizeForAccuracy(); // or speed, also ok

		
		SimbadElement almak = SimbadQuery.query("Almach");
		System.out.println("Simbad: " + almak.toString());
		
		// For stars
		eph.targetBody.setIndex(StarEphem.getStarTargetIndex("Almak"));
		StarElement star = StarEphem.getStarElement(eph.targetBody.getIndex());
		
		System.out.println("Star: " + (new SimbadElement(star)).toString());
		
		EphemElement ephem = EphemElement.parseStarEphemElement(StarEphem.starEphemeris(time, observer, eph, star, true));
		ConsoleReport.fullEphemReportToConsole(ephem);	

		LocationElement loc0 = new LocationElement("NGC 1777", false);
		loc0 = 	LocationElement.parseRectangularCoordinates(Precession.precessFromJ2000(J2018, loc0.getRectangularCoordinates(), eph));
		System.out.println("NGC1777: "+loc0.toStringAsEquatorialLocation());
	
	}
	
	
	@Test
	public void testBaseFormat() throws JPARSECException {
		double J2018 =(new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1, 12, 0, 0)).jd();
		System.out.println(J2018);
		TimeElement time = new TimeElement(J2018, TimeElement.SCALE.TERRESTRIAL_TIME);
		ObserverElement observer = ObserverElement.parseCity(City.findCity("Milan")); // irrelevant
//		for geocentric positions
		EphemerisElement eph = new EphemerisElement(TARGET.NOT_A_PLANET,
		EphemerisElement.COORDINATES_TYPE.ASTROMETRIC,
		                J2018, false, EphemerisElement.REDUCTION_METHOD.IAU_2009,
		                EphemerisElement.FRAME.DYNAMICAL_EQUINOX_J2000);
		eph.optimizeForAccuracy(); // or speed, also ok

		// For stars
		eph.targetBody.setIndex(StarEphem.getStarTargetIndex("Almak"));
		StarElement star = StarEphem.getStarElement(eph.targetBody.getIndex());
		EphemElement ephem = EphemElement.parseStarEphemElement(StarEphem.starEphemeris(time, observer, eph, star, true));
		ConsoleReport.fullEphemReportToConsole(ephem);	

		LocationElement loc0 = new LocationElement("NGC 1777", false);
		loc0 = 	LocationElement.parseRectangularCoordinates(Precession.precessFromJ2000(J2018, loc0.getRectangularCoordinates(), eph));
		System.out.println("NGC1777: "+loc0.toStringAsEquatorialLocation());

	}
	
	private void verify(String name) throws JPARSECException {
		double J2018 =(new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1, 12, 0, 0)).jd();
		TimeElement time = new TimeElement(J2018, TimeElement.SCALE.TERRESTRIAL_TIME);
		ObserverElement observer = ObserverElement.parseCity(City.findCity("Milan")); // irrelevant
//		for geocentric positions
		EphemerisElement eph = new EphemerisElement(TARGET.NOT_A_PLANET,
		EphemerisElement.COORDINATES_TYPE.ASTROMETRIC,
		                J2018, false, EphemerisElement.REDUCTION_METHOD.IAU_2009,
		                EphemerisElement.FRAME.DYNAMICAL_EQUINOX_J2000);
		eph.optimizeForAccuracy(); // or speed, also ok

		// For stars
		eph.targetBody.setIndex(StarEphem.getStarTargetIndex(name));
		StarElement star = StarEphem.getStarElement(eph.targetBody.getIndex());
		EphemElement ephem = EphemElement.parseStarEphemElement(StarEphem.starEphemeris(time, observer, eph, star, true));
		ConsoleReport.fullEphemReportToConsole(ephem);	
//
//		LocationElement loc0 = new LocationElement("NGC 1777", false);
//		loc0 = 	LocationElement.parseRectangularCoordinates(Precession.precessFromJ2000(J2018, loc0.getRectangularCoordinates(), eph));
//		System.out.println("NGC1777: "+loc0.toStringAsEquatorialLocation());
	}
	
	@Test
	public void testSingleStars() throws JPARSECException {
		verify("Capella");
	}

}
