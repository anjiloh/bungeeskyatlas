package net.anc.atlas;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;

import jparsec.math.Constant;
import jparsec.util.Translate;

/**
 * This defines varous test cases, not really related to specific classes
 * @author Angelo Nicolini
 *
 */
public class VariousTests {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testRound() {
		float val1 = 4.5f;
		float val2 = 2f;
		
		int result = (int)((val1 - val2)/2);
		assertEquals(1, result);

		result = Math.round((val1 - val2)/2);
		assertEquals(1, result);
		
		
		val1 = 4.5f;
		val2 = 0.8f;
		
		result = (int)((val1 - val2)/2);
		assertEquals(1, result);

		result = Math.round((val1 - val2)/2);
		assertEquals(2, result);  // DIFFERENT!!
		
		Float resultAsFloat = (val1 - val2)/2;
		assertEquals(1, resultAsFloat.intValue());
		
		
//		(int)(0.5 + (size.width+marginX*2)/factor), 
//		(int)(0.5 + (size.height+marginY*2)/factor)
		
		int width = 2500;
		int marginX = 2;
		int factor = 3;
		Double resultAsDouble = (0.5 + (width+marginX*2)/factor);
		assertEquals((Double)834.5, resultAsDouble);
		assertEquals(834, (int)(resultAsDouble.doubleValue()));
		assertEquals(834, resultAsDouble.intValue());


//		System.out.println(resultAsDouble);
		
	}

	@Test
	public void testDegree(){
		int deg = 176;
		System.out.println((char)deg);
		
		char degAsChar = 176;
		System.out.println(degAsChar);
	}

	
	@Test
	public void testPrecision() {
		double d = 0.0123133154;
		System.out.println(Math.ulp(d));
		d = Constant.RAD_TO_DEG;
		System.out.println(Math.ulp(d));
		d = Constant.RAD_TO_HOUR;
		System.out.println(Math.ulp(d));
		d = 0.7605460600180701	;
		System.out.println(Math.ulp(d));
		d = 1.5592775188077976	;
		System.out.println(Math.ulp(d));
		
		
	}
	
	
	@Test
	public void testFOVTransation() throws FileNotFoundException, UnsupportedEncodingException{
		String fov = "Field of view (°)";
		Translate.setDefaultLanguage(Translate.LANGUAGE.ITALIAN);
		System.out.println(Translate.translate(fov));
	}
	
	
}
