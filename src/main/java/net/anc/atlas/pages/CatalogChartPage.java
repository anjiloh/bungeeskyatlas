/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import jparsec.ephem.Functions;
import jparsec.io.FileIO;
import jparsec.util.Translate;
import jparsec.vo.SimbadElement;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.CatalogEntry;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;

/**
 * @author nicolinia
 *
 */
public class CatalogChartPage extends ChartPage implements Page {

	
	@Override
	public void setTitle(String... args) {
		String title = args[0];
		@SuppressWarnings("unused")
		String constellation = args[1];


//		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BOOK ) {
//			generator.writeRawText("\\newgeometry{left=0.4cm, right=0.0cm, top=0.1cm, bottom=0.2cm}");
			generator.beginSubSection(title + "\\label{subsec:" + (getChartId() + 1 ) + "}");
//		}
//		else {
//			generator.beginSubSection(title);
//		}
		
	}
	
	/**
	 * @param contains a SKyMap object (the navigator)
	 * 	 */
	@Override
	public void writeContent(List<Content> content) {

		CatalogEntry catalogEntry = (CatalogEntry) content.get(0);
		
		String title = String.format(
				Messages.getString("messier.main.title.subsection"), 
				catalogEntry.getName()
			);
		
//		generator.beginSubSection(title);
		generator.writeRawText("\\subsubsection{" + title + "}");
		generator.writeRawText( FileIO.getLineSeparator());
		
		generator.writeRawText(String.format(Messages.getString("messier.main.entry.catalog"), catalogEntry.getName()));
		
/////////////
		generator.writeLongTableHeader((String.format(Messages.getString("messier.main.entry.catalog.caption"), catalogEntry.getName())), "ll");
		generator.writeRawText("\\\\" + FileIO.getLineSeparator());
		
		generator.writeRawText(Messages.getString("messier.main.entry.catalog.column.1") + " & " + Messages.getString("messier.main.entry.catalog.column.2") + "\\\\" + FileIO.getLineSeparator());

		generator.writeRawText("\\hline\\hline\\noalign{\\smallskip}");
		generator.writeRawText(FileIO.getLineSeparator());
		generator.writeRawText("\\endfirsthead");
		generator.writeRawText(FileIO.getLineSeparator());

		generator.writeRawText("\\caption{Continue ");
		generator.writeRawText(String.format(Messages.getString("messier.main.entry.catalog"), catalogEntry.getName()));
		generator.writeRawText("}");
		generator.writeRawText("\\\\" + FileIO.getLineSeparator());
		generator.writeRawText("\\hline\\hline\\noalign{\\smallskip}");
		generator.writeRawText(FileIO.getLineSeparator());
		generator.writeRawText(Messages.getString("messier.main.entry.catalog.column.1") + " & " + Messages.getString("messier.main.entry.catalog.column.2") + "\\\\" + FileIO.getLineSeparator());

		generator.writeRawText("\\hline\\hline\\noalign{\\smallskip}");
		generator.writeRawText(FileIO.getLineSeparator());
		generator.writeRawText("\\endhead");
		generator.writeRawText(FileIO.getLineSeparator());

		
		generator.writeRowInTable(
				new String[] { Translate.translate("Name"), catalogEntry.getEntry().name},
				null, null, null );
				
		String plus = "";
		if (catalogEntry.getEntry().otherNames != null && catalogEntry.getEntry().otherNames.length>0) {
			for (int i=0; i< catalogEntry.getEntry().otherNames.length; i++) {
				plus += ", " + catalogEntry.getEntry().otherNames[i];
			}
			plus = plus.substring(1).trim();
		}
		if( StringUtils.isNotBlank(plus)) {
			generator.writeRowInTable(
					new String[] { Messages.getString("messier.main.entry.catalog.other.names") , plus},
					null, null, null );
			
		}
		
		generator.writeRowInTable(
				new String[] { Translate.translate("Right ascension"), Functions.formatRA(catalogEntry.getEntry().rightAscension)},
				null, null, null );
		
		generator.writeRowInTable(
				new String[] { Translate.translate("Declination"), Functions.formatDEC(catalogEntry.getEntry().declination)},
				null, null, null );

		if( catalogEntry.getEntry().type != null ) {
			generator.writeRowInTable(
					new String[] { Translate.translate("Type"), StringUtils.defaultString(catalogEntry.getEntry().type, "null")},
					null, null, null );
		}
		
		if( catalogEntry.getEntry().spectralType != null ) {
			generator.writeRowInTable(
					new String[] { Translate.translate("Spectral type"),  StringUtils.defaultString(catalogEntry.getEntry().spectralType, "null")},
					null, null, null );
		}
		
		if (catalogEntry.getEntry().bMinusV == SimbadElement.B_MINUS_V_UNAVAILABLE) {
			generator.writeRowInTable(
					new String[] { "B-V", Translate.translate("UNKNOWN")},
					null, null, null );
		} else {
			generator.writeRowInTable(
					new String[] { "B-V", Float.toString(catalogEntry.getEntry().bMinusV)},
					null, null, null );
		}
		
		generator.writeRowInTable(
				new String[] { Messages.getString("messier.main.entry.catalog.proper.motion") + " (" + Translate.translate("Right ascension") + ")", Float.toString(catalogEntry.getEntry().properMotionRA) + " (rad/yr)"},
				null, null, null );

		generator.writeRowInTable(
				new String[] { Messages.getString("messier.main.entry.catalog.proper.motion") + " (" + Translate.translate("Declination") + ")", Float.toString(catalogEntry.getEntry().properMotionDEC) + " (rad/yr)"},
				null, null, null );

		generator.writeRowInTable(
				new String[] { Messages.getString("messier.main.entry.catalog.proper.motion") + " (" +  Messages.getString("messier.main.entry.catalog.radial") + ")", Float.toString(catalogEntry.getEntry().properMotionRadialV) + " (km/s)"},
				null, null, null );
		
		generator.writeRowInTable(
				new String[] { Translate.translate("Parallax"), Float.toString(catalogEntry.getEntry().parallax) + " (mas)"},
				null, null, null );

		
		generator.endLongTable(null);
		
/////////////
		
//		generator.writeRawText("\\newgeometry{left=0.4cm, right=0.0cm, top=0.1cm, bottom=0.2cm}");

		generator.writeRawText("\\subsubsection{" + Messages.getString("messier.main.title.map") + "}");
		generator.forceNewPage();
		
		generator.		
			writeImageWithCaption(
				witdh, 	height, 
				null, 
				AtlasProperties.getProperties().getPDFAtlasFileName().replaceAll("\\\\", "/"),
				null, 
				null
			);

//		generator.writeRawText("\\restoregeometry");
	}



}
