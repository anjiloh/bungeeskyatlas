package net.anc.atlas.pages;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;
import net.anc.atlas.Table;
import net.anc.atlas.TestModule;

public class HistoryPageTest {

	protected Injector injector = Guice.createInjector(
			new TestModule(),
			new PagesModule(),
			new AbstractModule() {
		        @Override
		        protected void configure() {
		            bind(Page.class).to(HistoryPage.class);
		        }
			}
			
			);

	@Inject
	protected AtlasGenerator generator;
	
	@Before
	public void setUp() throws Exception {
        injector.injectMembers(this);
	}

	@Test
	public void testSetTitle() {
		HistoryPage history = injector.getInstance(HistoryPage.class);
		history.setTitle("History.title");
//		System.out.println(generator.getCode());
		
		assertTrue(generator.getCode().contains("section{"+Messages.getString("History.title") ));
	}

//	@Test
//	public void testWriteContent() {
//		fail("Not yet implemented"); // TODO
//	}

	@Test
	public void testSetHeader() {
		HistoryPage history = injector.getInstance(HistoryPage.class);
		assertTrue(history instanceof Table);
		history.setHeader(null, null);
		
//		System.out.println(generator.getCode());
		assertTrue(generator.getCode().contains("begin{longtable}[l]{lp{1.0\\textwidth}" ));
		assertTrue(generator.getCode().contains("caption{" + Messages.getString("History.caption")  ));
		assertTrue(generator.getCode().contains("endfirsthead" ));

		
		if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH) {
			assertTrue(generator.getCode().contains("caption{" + Messages.getString("History.caption") + " (continuation)" ));
		}
		else {
			assertTrue(generator.getCode().contains("caption{" + Messages.getString("History.caption") + " (continuazione)" ));
		}
		
		assertTrue(generator.getCode().contains("endhead" ));
	
	}

}
