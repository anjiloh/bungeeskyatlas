/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;

import com.google.inject.Inject;

import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;
import net.anc.atlas.Table;

/**
 * This class compiles the history of the atlas
 * 
 * @author Angelo Nicolini
 *
 */
public class HistoryPage implements Table {

	@Inject
	protected AtlasGenerator generator;

	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		generator.beginFrame(true, false);
		generator.beginSection(Messages.getString(title[0]));
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {
		String[][] history_en  = {
				{"1.0", "First Version"},
				{"1.1", "Update to the First Version:"
						+ "\\begin{itemize}"
						+ "\\item Corrected Star Index Generation; " 
						+ "\\item Fixed English version Generation; " 
						+ "\\item Fixed Dark Atlas Generation" 
						+ "\\end{itemize}"
				},
				{"2.0", "Second Version:"
						+ "\\begin{itemize}" 
						+ "\\item added version number to the cover;" 
						+ "\\item added History page;"
						+ "\\item fixed sky mapping for charts generation"
						+ "\\item update SkyMap implementation, in order to have a better coverage of the whole sky;"
						+ "\\item fixed Stars Indexes generation;"
						+ "\\item update iTextPDF version;"
						+ "\\item add minimaps;"
						+ "\\end{itemize}"
				},
				{"2.0.1", "Second Version Update:"
						+ "\\begin{itemize}" 
						+ "\\item Update to JParsec 1.115;" 
						+ "\\item added support for PDFBox2;" 
						+ "\\item added Messier Catalog generation Option;" 
						+ "\\end{itemize}"
				},

		};
		
		String[][] history_it  = {
				{"1.0", "Prima Versione"},
				{"1.1", "Aggiornamento della Prima Versione:"
						+ "\\begin{itemize} "  
						+ "\\item Corretta la creazione degli indici delle Stelle;"   
						+ "\\item Corretta generazione versione Inglese;"  
						+ "\\item corretta generazione atlante a colori inversi;" 
						+ "\\end{itemize}"
				},
				{"2.0", "Seconda Versione:"
						+ "\\begin{itemize}"  
						+ "\\item aggiunto numero di versione alla copertina;"   
						+ "\\item aggiunta pagina dello storico delle versioni;"  
						+ "\\item corretta mappatura cielo per generazione delle mappe;"
						+ "\\item modificata implementazone di SkyMap, per permettere una completa mappatura del cielo;"
						+ "\\item corretta generazione degli indici delle Stelle;"
						+ "\\item aggiornata versione di iTextPDF;"
						+ "\\item aggiunte minimappe;"
						+ "\\end{itemize}"
				},
				{"2.0.1", "Aggiornamento Seconda Versione"
						+	"\\begin{itemize}"  
						+ "\\item aggiornato a JParsec 1.115;"  
						+ "\\item aggiunto supporto per PDFBox2;" 
						+ "\\item aggiunta opzione per la generazione del Catalogo Messier;" 
						+ "\\end{itemize}"
				},

		};
		
		for (int i = 0 ; i < history_en.length; i++) {
			if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH) {
				generator.writeRowInTable(history_en[i], null, null, null);
			}
			else {
				generator.writeRowInTable(history_it[i], null, null, null);
			}
			
		}
		generator.endLongTable(null);

	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Table#setHeader(java.lang.String, java.lang.String[])
	 */
	@Override
	public void setHeader(String caption, String[] header) {
		
		generator.writeLongTableHeader(
				Messages.getString("History.caption"), 
				"lp{1.0\\textwidth}"
			);
		
		generator.writeHeaderRowInTable(new String[] {Messages.getString("History.header.1"), Messages.getString("History.header.2")}, null, null, null);
//		generator.writeHorizontalLine();

	}

}
