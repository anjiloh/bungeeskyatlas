/**
 * 
 */
package net.anc.atlas.index;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.inject.Inject;

import jparsec.util.JPARSECException;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AstroItemDetail.AstroItemType;
import net.anc.atlas.Content;

/**
 * @author Angelo Nicolini
 *
 */
public class StarsIndexBuilder implements IndexBuilder {

	@Inject
	private IndexComposerFactory<IndexComposer> builderFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.anc.atlas.index.IndexBuilder#getIndex(java.util.stream.Stream,
	 * int)
	 */
	@Override
	public List<Content> getIndex(Stream<String> source, int pageId) {

		List<Content> list = source
				.filter(line -> Integer.toString(pageId).equals(line.substring(line.lastIndexOf(',') + 1)))
				.map(AstroItemDetail::valueOf)
				.filter(item -> item.getItemType() == AstroItemType.STAR)
				.sorted(new Comparator<AstroItemDetail>() {

					@Override
					public int compare(AstroItemDetail o1, AstroItemDetail o2) {
						if (o1.getConstellation().equals(o2.getConstellation())) {
							return o1.getName().compareTo(o2.getName());
						} else {
							return o1.getConstellation().compareTo(o2.getConstellation());
						}
					}
				}).collect(Collectors.toList());

		list.forEach(item -> {
			try {
				((IndexEntryBuilder)builderFactory.create(IndexEntryBuilder.STARS)).compose((AstroItemDetail)item);
			} catch (JPARSECException e) {
				throw new RuntimeException("cannot compose " + ((AstroItemDetail) item).getName(), e);
			}
		});

		return list;
	}

}
