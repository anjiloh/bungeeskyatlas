package net.anc.atlas.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.ConstellationsByChart;
import net.anc.atlas.Content;

public class ConstellationsIndexBuilder implements IndexBuilder {

	/**
	 * @param pageId unused
	 */
	@Override
	public List<Content> getIndex(Stream<String> source, int pageId) {
		Map<Integer, Set<String>> collectionsByChart = new HashMap<>();

		source.forEach( line -> {
				AstroItemDetail idem = new AstroItemDetail(line);
				if( !collectionsByChart.containsKey(idem.getChartId()) ) { 
					collectionsByChart.put(idem.getChartId(), new TreeSet<>());
				}
				collectionsByChart.get(idem.getChartId()).add(idem.getConstellation());
				}
			
			);	
	
		List<Content> entries = new ArrayList<>();  
		for (Entry<Integer, Set<String>> entry: collectionsByChart.entrySet()) {
			for (String value : entry.getValue()) {
				entries.add(
						new ConstellationsByChart(value, (entry.getKey() + 1))// chart number starts for 1
					); 
			}
		}
		
		return entries;
	}

}
