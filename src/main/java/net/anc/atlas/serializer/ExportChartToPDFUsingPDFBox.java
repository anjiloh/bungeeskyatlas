/**
 * 
 */
package net.anc.atlas.serializer;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.google.inject.Inject;
import com.itextpdf.text.Rectangle;

import jparsec.graph.chartRendering.frame.SkyRendering;
import jparsec.util.JPARSECException;

/**
 * @author nicolinia
 *
 */
public class ExportChartToPDFUsingPDFBox implements ExportChart {

	private static final float MULTIPLICATOR = 72 / (100 * 2.54f);
	
	private static final Logger LOGGER = Logger.getLogger( ExportChart.class.getName() );

	
	private int marginX;
	private int marginY;

	@Inject
	public ExportChartToPDFUsingPDFBox() {
		System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
		System.setProperty("org.apache.pdfbox.rendering.UsePureJavaCMYKConversion", "true");
	}
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.serializer.ExportChart#export(jparsec.graph.chartRendering.frame.SkyRendering, java.awt.Dimension, java.lang.String, boolean, java.awt.image.BufferedImage)
	 */
	@Override
	public void export(SkyRendering skyRender, Dimension _size, String plotFile) {
		int factor = 1;

		Dimension size = new Dimension(Double.valueOf(0.5 + (_size.width+marginX*2)/(double)factor).intValue(), Double.valueOf(0.5 + (_size.height+marginY*2)/(double)factor).intValue());
		Rectangle pageSize = new Rectangle(0, 0, size.width, size.height);
		
		PDDocument pdfDocument = new PDDocument();
		pdfDocument.setVersion(1.5f);
		
		PDPage page = new PDPage(new PDRectangle(pageSize.getWidth() * MULTIPLICATOR, pageSize.getHeight() * MULTIPLICATOR));
//		PDPage page = new PDPage(PDRectangle.A4);
//		page.setRotation(90); 
		pdfDocument.addPage(page);
		BufferedImage bim = null;
	
		
		try {
		
			PDFRenderer pdfRenderer  = new PDFRenderer(pdfDocument);
			pdfRenderer.setSubsamplingAllowed(true);
			bim = skyRender.createBufferedImage();
			
//            ImageIOUtil.writeImage(bim, plotFile + ".png", AtlasProperties.getProperties().getPixelPerInch());

            PDImageXObject  pdImageXObject = LosslessFactory.createFromImage(pdfDocument, bim);
            try(PDPageContentStream contentStream = new PDPageContentStream(pdfDocument, page, AppendMode.OVERWRITE, false)) {
            	float scale = 0.28f;
            	contentStream.drawImage(pdImageXObject, 0 , 0, bim.getWidth()* scale, bim.getHeight() *scale);
            	contentStream.close();
            }
            
            pdfDocument.save(new File(plotFile));

            
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Exception while creating renderer", e);
		} catch (JPARSECException e) {
			LOGGER.log(Level.WARNING, "Exception while creating renderer", e);
		}
		finally {
			try {
				
				pdfDocument.close();
			} catch (IOException e) {
				// do nothing here
			}
		}


	}

	@Override
	public void initialize(int marginX, int marginY) {
		this.marginX = marginX;
		this.marginY = marginY;
	}

}
