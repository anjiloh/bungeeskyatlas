/**
 * 
 */
package net.anc.atlas.guice;

import net.anc.atlas.AtlasProperties;
import net.anc.atlas.BungeeSkyAtlas;
import net.anc.atlas.ChartsOnlyBungeeSkyAtlas;
import net.anc.atlas.Cleanup;
import net.anc.atlas.Startup;

/**
 * Guice configurator for a complete atlas generation, with a full cleanup of old indexes
 * 
 * @author Angelo Nicolini
 *
 */
public class ChartsOnlyBuilderModule extends AbstractAtlasBuilderModule {
	@Override
	protected void configure() {
		
		super.configure();
		bind(Startup.class).to(Cleanup.class);
		if( AtlasProperties.getProperties().isMultiVolume() ) {
			throw new RuntimeException("Not yet implemented");
//			bind(BungeeSkyAtlas.class).to(MultiVolumeCompleteBungeeSkyAtlas.class);
		}
		else {
			bind(BungeeSkyAtlas.class).to(ChartsOnlyBungeeSkyAtlas.class);
		}
		
	}
}
