/**
 * 
 */
package net.anc.atlas;

/**
 * Startup of the enviroment. It can be the cleanup of the environemnt, by removing index files,
 * or purge them from broken generations
 * @author Angelo Nicolini
 *
 */
public interface Startup {

	void performOpertion();
}
