package net.anc.atlas;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public interface PaperSize {

	/**
	 * paper width in mm
	 * @return
	 */
	int getWidth();
	/**
	 * paper height in mm
	 * @return
	 */
	int getHeight();
	
	/**
	 * Margin X
	 * @return
	 */
	int getMarginX();

	/**
	 * Margin Y
	 * @return
	 */
	int getMarginY();
	
	
	/**
	 * return the LateX package name
	 * @return
	 */
	String getLatexName();

	
	/**
	 * format name
	 */
	String getName();
}
