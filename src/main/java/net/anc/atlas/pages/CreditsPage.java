/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.inject.Inject;

import jparsec.util.JPARSECException;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;
import net.anc.atlas.MySkyAtlas;
import net.anc.atlas.Page;

/**
 * @author Angelo Nicolini
 *
 */
public class CreditsPage implements Page {

	private static final Logger LOGGER = Logger.getLogger( MySkyAtlas.class.getName() );

	@Inject
	private AtlasGenerator generator;	
	
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		generator.beginSection(Messages.getString("credits.page"));

	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {
		generator.writeParagraph(Messages.getString("CreditsPage.thank_you_all")); //$NON-NLS-1$
		
		
		try {
			generator.writeList(
					content
						.stream()
						.map( value -> value.toString() )
						.toArray(size -> new String[size])
					);
		} catch (JPARSECException e) {
			LOGGER.log(Level.SEVERE, "error while creating credit page", e); 
		}


	}

}
