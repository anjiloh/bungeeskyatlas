package net.anc.atlas.index;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import jparsec.ephem.stars.StarElement;
import jparsec.ephem.stars.StarEphem;
import jparsec.graph.DataSet;
import jparsec.io.ReadFile;
import jparsec.util.JPARSECException;
import jparsec.util.Translate;
import jparsec.vo.SimbadElement;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.OfflineSimbadCatalog;

/**
 * This decorates an AstroItemDetails containing a Star, and transform it to a printable object
 * for the Atlas Index section.
 * 
 * @author Angelo Nicolini
 *
 */
public class StarsIndexEntryBuilder implements IndexEntryBuilder  {
	
	private static final Logger LOGGER = Logger.getLogger( StarsIndexEntryBuilder.class.getName() );

//	private static Pattern pattern = Pattern.compile("\\((\\w+)\\p{Space}(\\w+)\\)");
	
	private static ReadFile sky2000 = null;

	static {
		sky2000 = new ReadFile();
		sky2000.setFormat(ReadFile.FORMAT.JPARSEC_SKY2000);
		try {
			sky2000.setPath(StarEphem.PATH_TO_SkyMaster2000_JPARSEC_FILE);
			sky2000.readFileOfStars();
		} catch (JPARSECException e) {
			LOGGER.log(Level.SEVERE, "cannot load catalog", e);
			throw new RuntimeException( "cannot load catalog", e);
		}
	}
	
	/*
	 * Example of data descriptions:
	 * http://inis.jinr.ru/sl/tcaep/astro/constell/21430096.htm
	 * (non-Javadoc)
	 * @see net.anc.atlas.index.IndexEntryBuilder#compose(net.anc.atlas.AstroItemDetail)
	 */
	public AstroItemDetail compose(AstroItemDetail item) throws JPARSECException {

		String targetName = (String) item.getName();
		SimbadElement simbad ;
		StarElement starElement;

		String catName = StarEphem.getCatalogNameFromProperName(targetName);
		simbad = OfflineSimbadCatalog.lookup(catName);
		
		try {
			
			
			if( simbad == null ){
				LOGGER.finest("simbad failed for " + targetName + " (" + catName + ")") ;
	            int my_star = sky2000.searchByName(catName);
	            starElement  = (StarElement) sky2000.getReadElements()[my_star];
			}
			else {
				starElement = StarElement.parseSimbadElement(simbad);
			}
			
			if( !targetName.equals(starElement.name)) {
//				Matcher matcher = pattern.matcher(starElement.name);
//				StringBuilder name = new StringBuilder(targetName);
//				if( matcher.find() ) {
//					name.append(" (").append(matcher.group(1));
//					name.append(' ').append(matcher.group(2));
//					name.append(')');
//				}
//				StringBuilder name = new StringBuilder(targetName);
//				name.append(" (").append(simbad.name).append(')');
//				item.setName(name.toString());
				if( simbad == null ){
					item.setOtherNames(starElement.name);
				}
				else {
					item.setOtherNames(simbad.name);
				}
				
				long occours = item.getOtherNames().chars().filter(ch -> ch == '(').count();
				
				if( occours > 1 ) {
					item.setOtherNames(item.getOtherNames().substring(0, item.getOtherNames().lastIndexOf('(')-1));
				}
			}
	

			StringBuilder extraInfo = new StringBuilder();
			if( StringUtils.isNotEmpty(starElement.spectrum  ) ) {
				extraInfo.append(Translate.translate("Spectral type")).append(": ").append( DataSet.replaceAll(starElement.spectrum, ",", "; ", true));
			}
			if( starElement.properMotionRA != 0 ) {
				extraInfo.append("; dRA: ").append( starElement.properMotionRA).append( "(rad/yr)");
			}
			if( starElement.properMotionDEC != 0 ) {
				extraInfo.append("; dDEC: ").append(starElement.properMotionDEC).append( "(rad/yr)");
			}
			if( starElement.properMotionDEC != 0 ) {
				extraInfo.append("; dVr: ").append(starElement.properMotionRadialV).append( "(km/s)");
			}
			if( starElement.parallax != 0 ) {
				extraInfo.append("; ").append(Translate.translate("Parallax")).append(": ").append(starElement.parallax).append( "(mas)");
			}
			if( starElement.getVariableType() != null ) {
				extraInfo.append("; ").append(Translate.translate("Variable Type")).append(": ").append(starElement.getVariableType());
			}
			if( extraInfo.toString().startsWith("; ")) {
				extraInfo.delete(0, 2);
			}
		
			item.setExtraInfo(  DataSet.replaceAll(extraInfo.toString() , ",", "; ", true) );
		

		} catch (JPARSECException e) {
			LOGGER.log(Level.SEVERE, "cannot decode star " + targetName, e);
			item = null;
		}
		return item;
	}

}
