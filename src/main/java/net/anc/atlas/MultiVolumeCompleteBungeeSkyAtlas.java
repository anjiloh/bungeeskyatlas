/**
 * 
 */
package net.anc.atlas;

import java.io.IOException;
import java.util.logging.Logger;

import jparsec.util.JPARSECException;

/**
 * @author Angelo Nicolini
 *
 */
public class MultiVolumeCompleteBungeeSkyAtlas extends MultiVolumeBungeeSkyAtlas {

	private static final Logger LOGGER = Logger.getLogger( MySkyAtlas.class.getName() );
	

	protected void doPreambleVolumeOne(){

		LOGGER.entering(this.getClass().getName(), "doPreamble");
		
		
		// write a License
		atlas.writeLicensePage();
		// write header and tableofcontents
		atlas.writeHeaderPage();
		// write the history of the atlas
		atlas.writeHistoryPage();
		// Write a presentation frame
		atlas.writePresentationPage();
		// write a credits page
		atlas.writeCreditsPage();
		// write a License
		atlas.writeLegendPage();

		LOGGER.exiting(this.getClass().getName(), "doPreamble");
			
	}
	
	protected void doPreambleVolumeTwo(){

		LOGGER.entering(this.getClass().getName(), "doPreamble");
		
		// write a License
		atlas.writeLicensePage();
		// write header and tableofcontents
		atlas.writeHeaderPage();
		// write the history of the atlas
		atlas.writeHistoryPage();
		// Write a presentation frame
		atlas.writePresentationPage();
		// write a credits page
		atlas.writeCreditsPage();
		// write a License
		atlas.writeLegendPage();

		LOGGER.exiting(this.getClass().getName(), "doPreamble");
			
	}
	
	
	/* (non-Javadoc)
	 * @see net.anc.atlas.BungeeSkyAtlas#create()
	 */
	@Override
	public void create() throws JPARSECException, IOException {
		
		startupPolicy.performOpertion();
		
		// Volume 1
		initializeVolumeOne();
		doPreamble();
		doMapsChapter();
		doChartsIndexChapter();
		terminate();
		

		// Volume 2
		initializeVolumeTwo();
		doPreamble();
		doIndexesChapter();		
		terminate();

	}

	

}
