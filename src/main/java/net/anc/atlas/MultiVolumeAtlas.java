/**
 * 
 */
package net.anc.atlas;

/**
 * @author Angelo Nicolini
 *
 */
public class MultiVolumeAtlas extends DefaultAtlas {

	private int currentVolumeNumber = 1;

	/**
	 * start of the atlas: title, volume number,  and additional packages 
	 */
	@Override
	public void startAtlas() {
		generator.setCode("");
		
		super.startAtlas();
		
		if( currentVolumeNumber == 1 ) {
			generator.writeRawText("\\part*{Volume I}");
			currentVolumeNumber++;
		}
		else {
			generator.writeRawText("\\part*{Volume II}");
		}
	}

	
	
	
}
