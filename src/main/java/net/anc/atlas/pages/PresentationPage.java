/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;

import com.google.inject.Inject;

import jparsec.io.HTMLReport.SIZE;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;

/**
 * @author Angelo Nicolini
 *
 */
public class PresentationPage implements Page {
	
	@Inject
	private AtlasGenerator generator;


	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		generator.setTextSize(SIZE.VERY_SMALL);
		generator.setAvoidFormatting(generator.getAvoidFormatting() + "$");

		generator.beginSection(
				Messages.getString("Bungee.presentation.title") + AtlasProperties.getProperties().getYear());
		
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {

		generator.setTextSize(SIZE.NORMAL);
		
		StringBuilder presentation = new StringBuilder();
		
		presentation.append(Messages.getString("Bungee.presentation.content.1"));
		presentation.append("J").append(AtlasProperties.getProperties().getYear()).append(' ');
		presentation.append(Messages.getString("Bungee.presentation.content.2"));
		
		//.append(FileIO.getLineSeparator());
		
		generator.writeRawText(presentation.toString());
		
		generator.endFrame();



	}

}
