package net.anc.atlas;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import jparsec.io.FileIO;
import net.anc.atlas.OfflineSimbadCatalog.SimbadEntry;

public class OfflineSimbadCatalogTest {

	@Before
	public void setUp() throws Exception {
	}

//	@Test
//	public void testLookup() {
//		fail("Not yet implemented"); // TODO
//	}
//
//	@Test
//	public void testUpdate() {
//		fail("Not yet implemented"); // TODO
//	}
	
	
	@Test
	public void showCatalogContent() throws IOException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		
		OfflineSimbadCatalog.lookup("any");
		
		

		
		Optional<Field> fileNameAccessor = Arrays.stream(OfflineSimbadCatalog.class.getDeclaredFields())
	     	.filter(f ->  Modifier.isStatic(f.getModifiers() ))
	     	.filter(f -> "simbadOfflineCatalog".equals(f.getName()) )
	     	.findAny();
		
		assertTrue(fileNameAccessor.isPresent());
		fileNameAccessor.get().setAccessible(true);
		String filename = (String) fileNameAccessor.get().get(null);
		
		FileInputStream fileInputStream = null;
		ObjectInputStream objectInputStream = null;
		try {
			fileInputStream = new FileInputStream(filename);
			objectInputStream = new ObjectInputStream(fileInputStream);

			@SuppressWarnings("unchecked")
			Map<String, SimbadEntry> catalog = (Map<String, SimbadEntry>) objectInputStream.readObject();
			
			
			catalog.entrySet().forEach(
					item -> { 
						String simbadToString = item.getValue().subject.toString().replaceAll(FileIO.getLineSeparator(), ";");

						System.out.println(simbadToString + ", " + item.getValue().timestamp);}
					);
			
		} 
		finally {
			if (objectInputStream != null) {
				try {
					objectInputStream.close();
				} catch (IOException e) {
					// do nothing
				}
			}
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}
		
	}

}
