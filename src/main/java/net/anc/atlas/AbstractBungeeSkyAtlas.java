/**
 * 
 */
package net.anc.atlas;

import java.awt.Dimension;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import com.google.inject.Inject;

import jparsec.astronomy.Constellation;
import jparsec.graph.chartRendering.RenderPlanet;
import jparsec.graph.chartRendering.SkyRenderElement;
import jparsec.graph.chartRendering.frame.SkyRendering;
import jparsec.io.FileIO;
import jparsec.io.WriteFile;
import jparsec.time.AstroDate;
import jparsec.time.TimeElement;
import jparsec.time.TimeElement.SCALE;
import jparsec.time.TimeScale;
import jparsec.util.Configuration;
import jparsec.util.DataBase;
import jparsec.util.JPARSECException;
import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;
import net.anc.atlas.AstroItemDetail.AstroItemType;
import net.anc.atlas.index.IndexBuilder;
import net.anc.atlas.index.IndexComposer;
import net.anc.atlas.index.IndexComposerFactory;
import net.anc.atlas.index.IndexConverter;
import net.anc.atlas.pages.PageFactory;
import net.anc.atlas.serializer.ExportChart;

/**
 * @author Angelo Nicolini
 *
 */
public abstract class AbstractBungeeSkyAtlas implements BungeeSkyAtlas {

	private static final Logger LOGGER = Logger.getLogger( MySkyAtlas.class.getName() );

//	private int marginX = 150;
//	private int marginY = 150;
	
	@Inject
	protected Atlas atlas;

	@Inject
	private PageFactory<Page> pageFactory;
	
	@Inject
	private IndexComposerFactory<IndexComposer> indexConverterFactory;
	
	@Inject
	protected Startup startupPolicy;
	
	@Inject
	protected ExportChart exportChart;
	
	public AbstractBungeeSkyAtlas(){
		SkyMapComposer composer = new SkyMapComposer();
		composer.create();
		
	}
	

	/**
	 * Compile a standard preamble for the atlas
	 */
	protected void doPreamble() {
		LOGGER.entering(this.getClass().getName(), "doPreamble");
		
		// write a License
		atlas.writeLicensePage();
		// write header and tableofcontents
		atlas.writeHeaderPage();
		// write the history of the atlas
		atlas.writeHistoryPage();
		// Write a presentation frame
		atlas.writePresentationPage();
		// write a credits page
		atlas.writeCreditsPage();
		// write a License
		atlas.writeLegendPage();

		LOGGER.exiting(this.getClass().getName(), "doPreamble");
	}
	
	
	
	protected void doIndexesChapter() throws IOException {
			LOGGER.entering(MySkyAtlas.class.getName(), "doIndexesChapter");
			atlas.startIndexSection();
	
			// index of constellation
			
			Stream<String> source = Files.lines(Paths.get(OBJECTS_INDEX_FILE));
			
			List<Content> entries = ((IndexBuilder)indexConverterFactory.create(IndexBuilder.CONSTELLATIONS)).getIndex(source, 0);
			atlas.writeIndexPage( 0, entries, (Index) pageFactory.create("constellationsByChart"));		
			source.close();

			// index of objects
			for (int i = 0; i < AtlasProperties.getProperties().getScale().getRa().length; i++) {
				LOGGER.fine("starting objects index for id: " + i);
				final int pageId = i;
				
				source = Files.lines(Paths.get(OBJECTS_INDEX_FILE));
				List<Content> list =((IndexBuilder)  indexConverterFactory.create(IndexBuilder.OBJECTS)).getIndex(source, pageId); 
				source.close();
				atlas.writeIndexPage(pageId, list, (Index) pageFactory.create("DSOsByChart"));
	
//				System.gc();
	
			}
			
			// stars
			for (int i = 0; i < AtlasProperties.getProperties().getScale().getRa().length; i++) {
				LOGGER.fine("starting stars index for id: " + i);
	
				final int pageId = i;
	
				source = Files.lines(Paths.get(STARS_INDEX_FILE));
				List<Content> list = ((IndexBuilder)indexConverterFactory.create(IndexBuilder.STARS)).getIndex(source, pageId); 
				atlas.writeIndexPage(pageId, list, (Index) pageFactory.create("starsByChart"));
				source.close();
			}
	
			// novae/ supernovae
			List<Content> list = new ArrayList<>();
			for (int i = 0; i < AtlasProperties.getProperties().getScale().getRa().length; i++) {
				LOGGER.fine("starting novae index for id: " + i);
	
				final int pageId = i;
				
				source = Files.lines(Paths.get(NOVAE_INDEX_FILE));
				list.addAll( 
						((IndexBuilder)indexConverterFactory.create(IndexBuilder.NOVAE))
						.getIndex(source, pageId) );
				source.close();
			}
			
			atlas.writeIndexPage(-1, list,  (Index) pageFactory.create("novaeByChart"));
	
			
			atlas.endIndexSection();
		
			LOGGER.exiting(MySkyAtlas.class.getName(), "doIndexesChapter");
		
	}

	/**
	 * This creates the index of objects in any chart
	 * @throws IOException
	 */
	protected void doChartsIndexChapter() throws IOException {
		LOGGER.entering(MySkyAtlas.class.getName(), "doMapsIndexChapter");

		// create minimap indexes

		atlas.writeMinimap();
		
		// create charts indexes
		List<Object[]> list = null;
		try(Stream<String> stream = Files.lines(Paths.get(RecoveryPoint.MAP_INDEX_FILE))) {;

			list = stream
				.map( line -> {
					try {
						return getChartPageAsLine(line);
					} catch (JPARSECException e) {
						throw new RuntimeException("Cannot parse line: " + line, e);
					}
				})
				.collect(Collectors.toList());
			
			stream.close();
			
		}
		atlas.writeChartPages(list);


		LOGGER.exiting(MySkyAtlas.class.getName(), "doMapsIndexChapter");
		
	}

	protected Object[] getChartPageAsLine(String line) throws JPARSECException {
		final String[] data = line.split(",");
		
		int pos = data[0].length() - 4 - 1; // -4 = .pdf, -1 because I want the last char
		if( Translate.getDefaultLanguage() == LANGUAGE.ENGLISH) {
			pos -= 4; // _ENG
		}
		
		
		final StringBuilder charIdAsString = new StringBuilder();
		while( Character.isDigit(data[0].charAt(pos)) ) {
			charIdAsString.append(data[0].charAt(pos));
			pos--;
		}
		String chartId = charIdAsString.reverse().toString();
		LOGGER.fine("adding the map " + chartId);
		
		double jdUT = TimeScale.getJD(
				new TimeElement(new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1), SCALE.BARYCENTRIC_DYNAMICAL_TIME),
				AtlasProperties.getProperties().getLocation(), 
				Sky.getEphemeris(), 
				SCALE.UNIVERSAL_TIME_UTC
			);
		AtlasProperties.getProperties().setSuffix("_chart" + chartId);

		String mainConstelInMap = Constellation.getConstellation(
				Constellation.getConstellationName(Double.parseDouble(data[1]), Double.parseDouble(data[2]), jdUT, Sky.getEphemeris()),
				Constellation.CONSTELLATION_NAME.LATIN
			);
		
		try {
			return new Object[] {mainConstelInMap, Integer.valueOf(chartId)};
		}
		catch(NumberFormatException e) {
			throw new RuntimeException("chartIndexFile.bin not coherent with the request: cannot process request. A cleanup is required.");
		}
		
	}
	
	protected void doMapsChapter() throws JPARSECException, IOException {
		LOGGER.entering(this.getClass().getName(), "doMapsChapter");

		
		AstroDate astro = new AstroDate(AtlasProperties.getProperties().getYear(), 1, 1);
		TimeElement time = new TimeElement(astro, SCALE.BARYCENTRIC_DYNAMICAL_TIME);
		time = new TimeElement(
				TimeScale.getJD(time, AtlasProperties.getProperties().getLocation(), Sky.getEphemeris(), SCALE.UNIVERSAL_TIME_UTC), 
				SCALE.UNIVERSAL_TIME_UTC
			);

		// north pole: dec 90; ra=0
		// Generate the charts with the following distribution

        buildMapsChapter(time);
        
		LOGGER.exiting(this.getClass().getName(), "doMapsChapter");
		
	}

	/**
	 * build charts and create index
	 * @param time
	 * @throws JPARSECException
	 * @throws IOException 
	 */
	protected void buildMapsChapter(TimeElement time) throws JPARSECException, IOException {

		
		// create minimaps first
		Sky minimap = new MiniMap();
		minimap.setCentralLatitude(90);
		minimap.setCentralLongitude(0);
		renderTheSky(minimap, time, AtlasProperties.getProperties().getNorthMinimapFilename());
		DataBase.clearEntireDatabase();
		
		minimap.setCentralLatitude(-90);
		minimap.setCentralLongitude(0);
		renderTheSky(minimap, time, AtlasProperties.getProperties().getSouthMinimapFilename());
		DataBase.clearEntireDatabase();
		
		// create all charts
		try{
			
			FileWriter dsoWriter   = new FileWriter(OBJECTS_INDEX_FILE, true);
			FileWriter starsWriter = new FileWriter(STARS_INDEX_FILE, true);
			FileWriter novaeWriter = new FileWriter(NOVAE_INDEX_FILE, true);
			
			
			for (int i = 0; i < AtlasProperties.getProperties().getScale().getRa().length; i++) {
		        LOGGER.log(Level.FINE, "creating map for chart " + i);
				AtlasProperties.getProperties().setSuffix("_chart" + i);
	
		        
				if( RecoveryPoint.isAlreadyCreated(AtlasProperties.getProperties().getPDFAtlasFileName()) ) {
					LOGGER.info( "already created, skipping " + RecoveryPoint.isAlreadyCreated(AtlasProperties.getProperties().getPDFAtlasFileName()) );
				}
				else {	
					Sky mySky = new Sky();
					mySky.setCentralLongitude(AtlasProperties.getProperties().getScale().getRa()[i]);
					mySky.setCentralLatitude(AtlasProperties.getProperties().getScale().getDec()[i]);
					List<Content> data = renderAndRetrieveObjects(mySky, time, AtlasProperties.getProperties().getPDFAtlasFileName());
					
				
					LOGGER.log(Level.FINE, "created map for chart, now starting index creation");
					
					final int chartNumber = i;
		
					data.stream()
							.filter( item -> ((AstroItemDetail)item).getItemType() == AstroItemType.DSO )
							.forEach(
								item -> {
									((AstroItemDetail)item).setChartId(chartNumber);
									try {
										dsoWriter.write(((AstroItemDetail)item).asCSVString() + FileIO.getLineSeparator());
										
										dsoWriter.flush();
									} catch (IOException e) {
								        LOGGER.log(Level.SEVERE, "Problem while serializing index, continuing", e);
									}
								}
							);
					LOGGER.log(Level.FINE, "created objects index for chart");
					data.stream()
						.filter( item -> ((AstroItemDetail)item).getItemType() == AstroItemType.STAR )
						.forEach(
							item -> {
								((AstroItemDetail)item).setChartId(chartNumber);
								try {
									starsWriter.write(((AstroItemDetail)item).asCSVString() + FileIO.getLineSeparator());
									
									dsoWriter.flush();
								} catch (IOException e) {
							        LOGGER.log(Level.SEVERE, "Problem while serializing index, continuing", e);
								}
							}
						);
					LOGGER.log(Level.FINE, "created stars index for chart");

					data.stream()
						.filter( item -> ((AstroItemDetail)item).getItemType() == AstroItemType.NOVA ||  ((AstroItemDetail)item).getItemType() == AstroItemType.SUPERNOVA )
						.forEach(
							item -> {
								((AstroItemDetail)item).setChartId(chartNumber);
								try {
									novaeWriter.write(((AstroItemDetail)item).asCSVString() + FileIO.getLineSeparator());
									
									novaeWriter.flush();
								} catch (IOException e) {
							        LOGGER.log(Level.SEVERE, "Problem while serializing index, continuing", e);
								}
							}
						);
					LOGGER.log(Level.FINE, "created novae/supernovae index for chart");

					
					LOGGER.log(Level.FINE, "created all indexes for chart");

				}			
				
//				System.gc();

			}			
	        LOGGER.log(Level.FINE, "All maps created");
	
	        try {
	        	dsoWriter.close();
	        }
	        catch(Exception e ){
	        	// ignore
	        }
	        try {
	        	starsWriter.close();
	        }
	        catch(Exception e ){
	        	// ignore
	        }
	        try {
	        	novaeWriter.close();
	        }
	        catch(Exception e ){
	        	// ignore
	        }
	        
	        
		}
		finally {
		}
		LOGGER.exiting(MySkyAtlas.class.getName(), "buildMapsChapter");

			
	}

	/**
	 *	
	 *Generate sky rendering
	 * @param mySky
	 * @param time
	 * @return
	 */
	@Nullable
	protected List<Content> renderAndRetrieveObjects(Sky mySky, TimeElement time, String filename) {
		List<Content> data = null;
		try {
			SkyRendering skyRender = renderTheSky(mySky, time, filename);

			// fill data from skyRender
			data = new ArrayList<Content>();
			data.addAll(getObjectsInChart(skyRender, "minorObjects"));
			data.addAll(getObjectsInChart(skyRender, "starNames"));
			
			RecoveryPoint.checkPoint(AtlasProperties.getProperties().getPDFAtlasFileName(), mySky.getCentralLongitude(), mySky.getCentralLatitude());
			DataBase.clearEntireDatabase();
			
		} catch (Exception ve) {
	        LOGGER.log(Level.SEVERE, "problem while renderingTheSky", ve);

		}
		return data;

	}


	protected SkyRendering renderTheSky(Sky mySky, TimeElement time, String filename)
			throws JPARSECException, IOException {
		SkyRenderElement sky = mySky.getRender();
		RenderPlanet.dateChanged();
		SkyRenderElement skyElement = sky.clone();
		int yMargin = 0;

		SkyRendering skyRender = new SkyRendering(
				time, 
				AtlasProperties.getProperties().getLocation(), 
				Sky.getEphemeris(),
				skyElement, 
				"Sky render", 
				yMargin
			);
		
		skyRender.getRenderSkyObject().setYCenterOffset(1);
		exportChart.export(
				skyRender, 
				new Dimension(skyElement.width, skyElement.height),
				filename
			); // pic.getImage());

		
		return skyRender;
	}
	
	protected List<Content>  getObjectsInChart(SkyRendering skyRender, String dbName)
			throws JPARSECException {
		
		
		Object o = DataBase.getData(dbName, skyRender.getRenderSkyObject().getDataBaseThreadID(), true);

		List<Content> objectsFound = new ArrayList<Content>();
		
		if( o != null ){
			
			List<Object> objects = new ArrayList<Object>(Arrays.asList((Object[]) o));
			
			IndexConverter indexBuilder;
			
			if(  objects.get(0) instanceof String ){
				indexBuilder = (IndexConverter) indexConverterFactory.create(IndexConverter.STAR );
			}
			else {
				indexBuilder = (IndexConverter)indexConverterFactory.create(IndexConverter.OBJECT );
			}
			
			for (int i = 0; i < objects.size(); i++) { 
				AstroItemDetail astroData = indexBuilder.convert(objects.get(i), skyRender.getRenderSkyObject());
				if( astroData == null  ) {
					// ignore
				}
				else {
					astroData.setConstellation(
							Constellation.getConstellationName(
								astroData.getLocation(), 
								TimeScale.getJD(
										skyRender.getTimeObject(), 
										AtlasProperties.getProperties().getLocation(), 
										skyRender.getEphemerisObject(), 
										SCALE.UNIVERSAL_TIME_UTC
									), 
								skyRender.getEphemerisObject()
							)
						);
					objectsFound.add(astroData);
				}
			}
		}
		return objectsFound ;

	}
				


	
	
	public void terminate() throws JPARSECException {
		LOGGER.entering(MySkyAtlas.class.getName(), "finalize");

		atlas.endAtlas();
		
		
		// Write the latex file
//		String out = AtlasProperties.getProperties().getLaTeXAtlasFileName();
		String out = getAtlasFileName();
		WriteFile.writeAnyExternalFile(out, atlas.toString());

		LOGGER.fine( "created " + out);


	}

	/**
	 * Atlas file name
	 * @return
	 */
	protected String getAtlasFileName() {
		return AtlasProperties.getProperties().getLaTeXAtlasFileName();
	}


	/**
	 * This initalize the atlas creation and start the atlas itself
	 * @throws JPARSECException
	 */
	protected void initialize() throws JPARSECException {
		LOGGER.entering(MySkyAtlas.class.getName(), "initialize");
		
		
		atlas.startAtlas();

		
		// Full screen mode for the atlas
//		marginX = marginY = 0;

		
		if (!AtlasProperties.getProperties().isWhite()) {
			Configuration.MAX_CACHE_SIZE = 1;
		}
		
		LOGGER.exiting(this.getClass().getName(), "initialize");
	}

}
