# Project info


This repository holds the source code for the BungeeSkyAtlas project.

The project home page is located [here][1].
You will find pieces of information with regards to purpose, history and development of JPARSEC.

The project has its [wiki][2].



# Java source/target level

The Java source and target is 1.8 currently.

# Guidelines to use in Eclipse

1. Ensure the M2E plugin is installed;
2. Download JPARSEC from its GIT repository. For further info see [jparsec][3]
3. import JPARSEC as Eclipse Project
4. overwrite JPARSEC classes with the ones found in src/extra-jparsec/java/


[1]: https://sites.google.com/site/studidiastronomia/home/bungee-sky-atlas "Bungee Sky Atlas"

[2]: https://bitbucket.org/anjiloh/bungeeskyatlas/wiki/Home "Bungee Sky Atlas wiki"

[3]: http://conga.oan.es/~alonso/doku.php?id=jparsec/ "JPARSEC homepage"
