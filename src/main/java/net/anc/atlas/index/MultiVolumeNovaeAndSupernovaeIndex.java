/**
 * 
 */
package net.anc.atlas.index;

/**
 * MultiVolume cannot have cross references
 * 
 * @author Angelo Nicolini
 *
 */
public class MultiVolumeNovaeAndSupernovaeIndex extends NovaeAndSupernovaeIndex {

	@Override
	protected String getFormattedChartIdRef(int chartId) {
		return Integer.toString(chartId + 1);
	}
}
