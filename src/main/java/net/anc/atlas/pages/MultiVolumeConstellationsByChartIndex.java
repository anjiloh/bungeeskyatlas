/**
 * 
 */
package net.anc.atlas.pages;

/**
 * @author Angelo Nicolini
 *
 */
public class MultiVolumeConstellationsByChartIndex extends ConstellationsByChartIndex {

	@Override
	protected String getFormattedChartIdRef(int chartId) {
		return Integer.toString(chartId);
	}
}
