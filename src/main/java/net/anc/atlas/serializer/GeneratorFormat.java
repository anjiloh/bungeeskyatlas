/**
 * 
 */
package net.anc.atlas.serializer;

/**
 * @author Angelo Nicolini
 *
 */
public enum GeneratorFormat {
		BOOK,
		BEAMER
}
