/**
 * 
 */
package net.anc.atlas.index;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import jparsec.util.JPARSECException;
import net.anc.atlas.AstroItemDetail;

/**
 * @author Angelo Nicolini
 *
 */
public class ObjectsIndexEntryBuilderTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for {@link net.anc.atlas.index.ObjectsIndexEntryBuilder#compose(net.anc.atlas.AstroItemDetail)}.
	 */
	@Test
	public void testComposeCaldwellPos0() {
		String line="CALDWELL 99 - Coalsack,3.364520788192749,-1.092537522315979,1.0,100.0,3.5x2.5,2,Crux,DSO,30";
		AstroItemDetail testable = AstroItemDetail.valueOf(line);
		AstroItemDetail result = null;
		ObjectsIndexEntryBuilder subject = new ObjectsIndexEntryBuilder();
		try {
			result = subject.compose(testable);
		} catch (JPARSECException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(result);
	}

	@Test
	public void testComposeCaldwellPos1() {
		String line="4372 CALDWELL 108,3.258646011352539,-1.2698695659637451,1.0,7.2,0.04166667x0.04166667,5,Musca,DSO,31";
		AstroItemDetail testable = AstroItemDetail.valueOf(line);
		AstroItemDetail result = null;
		ObjectsIndexEntryBuilder subject = new ObjectsIndexEntryBuilder();
		try {
			result = subject.compose(testable);
		} catch (JPARSECException e) {
			fail("Unexpected exception " + e.getMessage());
		}
		assertNotNull(result);
	}
}
