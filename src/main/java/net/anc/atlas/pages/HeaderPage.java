/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;

import com.google.inject.Inject;

import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.Page;

/**
 * @author Angelo Nicolini
 *
 */
public class HeaderPage implements Page {
	
	@Inject
	private AtlasGenerator generator;


	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {
		generator.beginFrame(true, false);
		generator.writeRawText("\\tableofcontents")	;
		generator.forceNewPage();
		generator.endFrame();


	}

}
