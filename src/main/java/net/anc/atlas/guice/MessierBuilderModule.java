/**
 * 
 */
package net.anc.atlas.guice;

import com.google.inject.AbstractModule;

import net.anc.atlas.Atlas;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.BungeeSkyAtlas;
import net.anc.atlas.Cleanup;
import net.anc.atlas.Startup;
import net.anc.atlas.serializer.ExportChart;
import net.anc.atlas.serializer.ExportChartToPDFUsingPDFBox;
import net.anc.atlas.serializer.GeneratorFormat;
import net.anc.atlas.serializer.LaTeXBeamerGenerator;
import net.anc.atlas.serializer.LaTeXBookGenerator;
import net.anc.messier.MessierAtlas;
import net.anc.messier.MessierCatalog;

/**
 * Guice configurator for a complete atlas generation, with a full cleanup of old indexes
 * 
 * @author Angelo Nicolini
 *
 */
public class MessierBuilderModule extends AbstractModule {
	@Override
	protected void configure() {
		
		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BOOK ) {
			bind(AtlasGenerator.class).to(LaTeXBookGenerator.class);
		}
		else {
			bind(AtlasGenerator.class).to(LaTeXBeamerGenerator.class);
		}
//		bind(AtlasGenerator.class).toProvider(AtlasGeneratorProvider.class);
		
//		bind(ExportChart.class).to(ExportChartToPDFUsingIText.class);

		bind(ExportChart.class).to(ExportChartToPDFUsingPDFBox.class);

		
		bind(Startup.class).to(Cleanup.class);
		bind(BungeeSkyAtlas.class).to(MessierCatalog.class);
		
		bind(Atlas.class).to(MessierAtlas.class);

		
	}
}
