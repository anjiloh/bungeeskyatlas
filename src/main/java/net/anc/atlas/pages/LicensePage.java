/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;

import com.google.inject.Inject;

import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;

/**
 * @author Angelo Nicolini
 *
 */
public class LicensePage implements Page {

	@Inject
	protected AtlasGenerator generator;

	
	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		generator.beginFrame(true, false);
		
		StringBuilder licenseTitle = new StringBuilder(	generator.getCode() );
		licenseTitle.append("\\chapter*{").append(Messages.getString(title[0])).append('}');
		generator.setCode(licenseTitle.toString());
 
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#writeContent(java.util.List)
	 */
	@Override
	public void writeContent(List<Content> content) {

		generator.writeParagraph(Messages.getString("License.content"));
		generator.endFrame();
	}

}
