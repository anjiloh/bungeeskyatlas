/**
 * 
 */
package net.anc.atlas.pages;

import com.google.inject.Inject;

import jparsec.graph.DataSet;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Index;
import net.anc.atlas.Table;
import net.anc.atlas.serializer.GeneratorFormat;

/**
 * @author Angelo Nicolini
 *
 */
public abstract class BaseIndex implements Index, Table {
	
	@Inject
	private AtlasGenerator generator;

	@Override
	public void setHeader(String caption, String[] header) {
		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BEAMER ) {
			generator.writeTableHeader(caption);
		}
		else {
			generator.
				writeLongTableHeader(caption, String.format("%0" + header.length + "d", 0).replace("0","l"));
			generator.	writeHeaderRowInTable(header, null, null, null);
			
		}
		generator.writeHorizontalLine();
		
		if( !AtlasProperties.getProperties().isWhite() ) {
			String code = generator.getCode();
//			code = DataSet.replaceAll(code, "rowcolors{2}{gray!25}{white}", "rowcolors{2}{gray!85}{black}", true);
			
			code = DataSet.replaceAll(code, "\\node (a) at (0,0) {\\circled{\\ref{subsec:#1}}};%", "\\node (a) at (0,0)    {\\circled{\\color{black}{\\ref{subsec:#1}}}};%", true);
			code = DataSet.replaceAll(code, "\\node (b) at (4.5,0) {\\circled{\\ref{subsec:#2}}};%", "\\node (b) at (4.5,0)  {\\circled{\\color{black}{\\ref{subsec:#2}}}};%", true);
			code = DataSet.replaceAll(code, "\\node (c) at (0,4.5) {\\circled{\\ref{subsec:#3}}};%", "\\node (c) at (0,4.5)  {\\circled{\\color{black}{\\ref{subsec:#3}}}};%", true);
			code = DataSet.replaceAll(code, "\\node (d) at (-4.5,0) {\\circled{\\ref{subsec:#4}}};%", "\\node (d) at (-4.5,0) {\\circled{\\color{black}{\\ref{subsec:#4}}}};%", true);
			code = DataSet.replaceAll(code, "\\node (e) at (0, -4.5) {\\circled{\\ref{subsec:#5}}};%", "\\node (e) at (0,-4.5) {\\circled{\\color{black}{\\ref{subsec:#5}}}};%", true);

			generator.setCode(code);
		}
		
	}

	/* (non-Javadoc)
	 * @see net.anc.atlas.Page#setTitle(java.lang.String[])
	 */
	@Override
	public void setTitle(String... title) {
		if( title.length > 0 ) {
			generator.
//			LaTexGenerator.getGenerator().
				beginSubSection(title[0]);
		}
	}

}
