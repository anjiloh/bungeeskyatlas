package net.anc.atlas;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.junit.Before;
import org.junit.Test;

public class NumberFormatTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testScientificData() {
//		fail("Not yet implemented"); // TODO
//	    System.out.println(String.format("%6.3e", Double.valueOf(value)));
	
		DecimalFormat df = new DecimalFormat("0.000E0");
		df.setRoundingMode(RoundingMode.CEILING);
		
//		-6.6467953E-7 1.2799082E-7
	    
	    int scale =10;

	    String value = "-6.6467953E-7";
	    System.out.println("Original: " + value);
	    BigDecimal bd = new BigDecimal(value, new MathContext(3)).setScale(scale, BigDecimal.ROUND_HALF_UP);
	    System.out.println(bd);
	    System.out.println(String.format("%6.3e",bd.doubleValue()));
	    System.out.println(String.format("%6.3e", Double.valueOf(value)));
	    System.out.println(String.format("%6.3s", df.format(Double.valueOf(value))));

	    
	    value = "-2.9816043E-8";
	    System.out.println("Original: " + value);
	    bd = new BigDecimal(value, new MathContext(10)).setScale(scale, BigDecimal.ROUND_HALF_UP);
//	    System.out.println(bd.toEngineeringString());	
//	    System.out.println(String.format(" bd.doubleValue() ->      %6.3e",bd.doubleValue()));
	    System.out.println(String.format(" Double.valueOf(value) -> %6.3e", Double.valueOf(value)));
//	    System.out.println(String.format("%6.3s", df.format(Double.valueOf(value))));

	    
	    
	    
	   }

}
