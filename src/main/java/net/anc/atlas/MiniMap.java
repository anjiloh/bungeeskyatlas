/**
 * 
 */
package net.anc.atlas;

import jparsec.astronomy.CoordinateSystem.COORDINATE_SYSTEM;
import jparsec.astronomy.OcularElement;
import jparsec.astronomy.TelescopeElement;
import jparsec.graph.JPARSECStroke;
import jparsec.graph.chartRendering.Graphics;
import jparsec.graph.chartRendering.SkyRenderElement.FAST_LINES;
import jparsec.graph.chartRendering.SkyRenderElement.LEYEND_POSITION;
import jparsec.graph.chartRendering.SkyRenderElement.REALISTIC_STARS;
import jparsec.math.Constant;
import jparsec.util.JPARSECException;

/**
 * 
 * This creates a new minmap. 
 * 
 * The projection to use is a stereographic one, because angles must be preserved.
 * 
 * @author Angelo Nicolini
 *
 */
public class MiniMap extends Sky {

	public MiniMap() throws JPARSECException {
		super();
	}
	
	@Override
	protected void init() throws JPARSECException {
		super.init();
		if( AtlasProperties.getProperties().isReflector() ) {
			setTelescope(new TelescopeElement("newton", 85, 50, 0, 0, 1.5f, new OcularElement("Binoculars", 371f, 65 * Constant.DEG_TO_RAD, 7)));
		}
		else {
			setTelescope(new TelescopeElement("Object", 85, 50, 0, 0, 1.5f, new OcularElement("Binoculars", 371f, 65 * Constant.DEG_TO_RAD, 7)));
		}

	}

	@Override
	protected COORDINATE_SYSTEM getCoordinateSystem() {
		return COORDINATE_SYSTEM.EQUATORIAL;
	}

	
	@Override
	protected void initSkyProperties() throws JPARSECException {
		
		super.initSkyProperties();
		
		getSkyRender().drawLeyend = LEYEND_POSITION.NO_LEYEND;
		getSkyRender().drawFastLinesMode = FAST_LINES.NONE;
		getSkyRender().drawStarsLimitingMagnitude = 5.5f;
		getSkyRender().drawObjectsLimitingMagnitude = 7.5f;
		getSkyRender().coordinateSystem = getCoordinateSystem();

		getSkyRender().drawStarsLabelsLimitingMagnitude = getSkyRender().drawStarsLimitingMagnitude-2;
		getSkyRender().drawSuperNovaAndNovaEvents = false;
		getSkyRender().drawSkyCorrectingLocalHorizon = true;
		getSkyRender().drawConstellationLimits = false;
		getSkyRender().drawStarsRealistic = REALISTIC_STARS.STARRED;
		
		getSkyRender().drawConstellationNamesFont = Graphics.FONT.SANS_SERIF_ITALIC_18;
		getSkyRender().drawStarsNamesFont = Graphics.FONT.SANS_SERIF_PLAIN_11;
		getSkyRender().drawConstellationStroke = JPARSECStroke.STROKE_DEFAULT_LINE_THIN;
	
	}
	
	
}
