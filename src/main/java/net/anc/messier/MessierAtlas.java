/**
 * 
 */
package net.anc.messier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jparsec.util.JPARSECException;
import jparsec.vo.SimbadElement;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.CatalogEntry;
import net.anc.atlas.Content;
import net.anc.atlas.DefaultAtlas;
import net.anc.atlas.Messages;
import net.anc.atlas.OfflineSimbadCatalog;
import net.anc.atlas.Page;
import net.anc.atlas.pages.CatalogChartPage;

/**
 * @author Angelo Nicolini
 *
 */
public class MessierAtlas extends DefaultAtlas   {
	
	
	public MessierAtlas() {
		super();
	}
	
	@Override
	public String getAtlasTitle() {
		return Messages.getString("catalog.messier.title");
	}
	
	/**
	 * No minimpas for a Messier Catalog
	 */
	@Override
	public void writeMinimap() {
		// do nothing
	}

	
	@Override
	public void writeChartPages(List<Object[]> list) {
		StringBuilder caption = new StringBuilder();
		if (!AtlasProperties.getProperties().isWhite()) {
			caption.append(", ").append(Messages.getString("Bungee.darkedition"));
		}
		caption.append('.');

		generator.beginSection(Messages.getString("Charts.main.title"));
		
		list.forEach( nextChartPage -> {
			
			String constellation = (String) nextChartPage[0];
			int chartId = (int)(nextChartPage[1]);
			
			
			String title = String.format(
					Messages.getString("messier.main.title"), 
					(chartId + 1),
					constellation
				);
			
			Page chartPage = pageFactory.create("chartPage");
			((CatalogChartPage)chartPage).setChartId(chartId);
			AtlasProperties.getProperties().setSuffix("_chart" + chartId);

			caption.append(title);
			
			//  Rendering 
			chartPage.setTitle( title, constellation);
			
			String name = "M"+(chartId + 1);
			
			SimbadElement simbadEntry = OfflineSimbadCatalog.lookup(name);
			if( simbadEntry.otherNames == null || simbadEntry.otherNames.length == 0) {
				SimbadElement altElement;
				try {
					altElement = SimbadElement.searchDeepSkyObject(name);
					if(altElement.otherNames != null && altElement.otherNames.length > 0) {
						List<String> names = new ArrayList<>();
						names.addAll(	Arrays.asList(altElement.otherNames) );
						names.remove(name);
						simbadEntry.otherNames = new String[names.size()];
						names.toArray(simbadEntry.otherNames);
					}
					OfflineSimbadCatalog.update(simbadEntry);
				} catch (JPARSECException e) {
					// as no element found
				}		

			}
			
			chartPage.writeContent(new ArrayList<Content>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -8671851935194000334L;
	
				{
					add( new CatalogEntry( name , simbadEntry ) );
				}
			});

		});		
	}

	
}
