/**
 * 
 */
package net.anc.atlas;

import java.util.List;

/**
 * Northern emisphere minimap adapter. 
 * 
 * @author Angelo Nicolini
 *
 */
public class NorthernEmisphereMinimap extends EmisphereMinimap {

	@Override
	public void compile() {

		List<SkyMap> skyMap = SkyMapComposer.getSkyMap();
		
		skyMap.sort( (chart1, chart2) -> {
			if( chart1.dec == chart2.dec ) {
				return Double.compare(chart1.ra, chart2.ra);
			}
			else {
				return -1 * Double.compare(chart1.dec, chart2.dec);
			}
		});
		int separationPoint = -1;
		if( AtlasProperties.getProperties().getScale() == Zoom.VIEW_OF_25_DEG_DEF || 	AtlasProperties.getProperties().getScale() == Zoom.VIEW_OF_25_DEG_NWT ) {
			separationPoint = 0;
		}

		
		double currentDEC = 0;
		int id = 0;
		double ro = 0;
		for (SkyMap map : skyMap) {
			if( map.dec == 90 && map.ra == 0) {
				ro = 0;

				Entry entry = new Entry();
				entry.ro = 0;
				entry.theta = 0;
				entry.chartId = map.getChartId() + 1;
				coordinates.add(entry);
				
			}
			else {
				if( map.dec < separationPoint) {  
					break;
				}
				if( currentDEC != map.dec) {
					
					ro += strechFactor.get(AtlasProperties.getProperties().getPaperSize()).get(AtlasProperties.getProperties().getScale() )[id];
//					if( AtlasProperties.getProperties().isReflector()) {	
//						ro += strechFactorNWT.get(AtlasProperties.getProperties().getScale() )[id];
//					}
//					else {
//						ro += strechFactorDEF.get(AtlasProperties.getProperties().getScale() )[id];
//					}
					id++;
					currentDEC = map.dec;
				}
				
				Entry entry = new Entry();
				entry.ro = ro;
				entry.theta = map.getTetha();
				entry.chartId = map.getChartId() + 1;
				coordinates.add(entry);
			}
		}
	}
}
