/**
 * 
 */
package net.anc.atlas;

/**
 * Base implementation for an A4 page size
 * @author Angelo Nicolini
 *
 */
public class A4 implements PaperSize {

	private String id = "A4";
	
	@Override
	public int getWidth() {
		return 210;
	}

	@Override
	public int getHeight() {
		return 297;
	}

	@Override
	public int getMarginX() {
		return 0;
	}

	@Override
	public int getMarginY() {
		return 0;
	}

	@Override
	public String getLatexName() {
		return "a4paper";
	}

	@Override
	public String getName() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		A4 other = (A4) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
