/**
 * 
 */
package net.anc.atlas.index;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;

/**
 * Guice module for indexes
 * 
 * @author Angelo Nicolini
 *
 */
public class IndexModule extends AbstractModule {

	@Override
	protected void configure() {
		MapBinder<String, IndexComposerFactory<?>> mapbinder = MapBinder.newMapBinder(
				binder(),
				new TypeLiteral<String>() {
				}, new TypeLiteral<IndexComposerFactory<?>>() {
			});

		install(new FactoryModuleBuilder().build(StarConverterFactory.class));
		mapbinder.addBinding(IndexConverter.STAR).to(StarConverterFactory.class);
		
		install(new FactoryModuleBuilder().build(DSOConverterFactory.class));
		mapbinder.addBinding(IndexConverter.OBJECT).to(DSOConverterFactory.class);
		
		install(new FactoryModuleBuilder().build(ConstellationsBuilderFactory.class));
		mapbinder.addBinding(IndexBuilder.CONSTELLATIONS).to(ConstellationsBuilderFactory.class);
		install(new FactoryModuleBuilder().build(DSOBuilderFactory.class));
		mapbinder.addBinding(IndexBuilder.OBJECTS).to(DSOBuilderFactory.class);
		install(new FactoryModuleBuilder().build(StarsBuilderFactory.class));
		mapbinder.addBinding(IndexBuilder.STARS).to(StarsBuilderFactory.class);
		install(new FactoryModuleBuilder().build(NovaeBuilderFactory.class));
		mapbinder.addBinding(IndexBuilder.NOVAE).to(NovaeBuilderFactory.class);

		install(new FactoryModuleBuilder().build(DSOEntryBuilderFactory.class));
		mapbinder.addBinding(IndexEntryBuilder.OBJECTS).to(DSOEntryBuilderFactory.class);
		install(new FactoryModuleBuilder().build(StarEntryBuilderFactory.class));
		mapbinder.addBinding(IndexEntryBuilder.STARS).to(StarEntryBuilderFactory.class);
		install(new FactoryModuleBuilder().build(NovaeEntryBuilderFactory.class));
		mapbinder.addBinding(IndexEntryBuilder.NOVAE).to(NovaeEntryBuilderFactory.class);

		bind(new TypeLiteral<IndexComposerFactory<IndexComposer>>() {}).to(ConcreteIndexFactory.class);
	
	}

}
