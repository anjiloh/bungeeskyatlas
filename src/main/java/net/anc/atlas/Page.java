/**
 * 
 */
package net.anc.atlas;

import java.util.List;

/**
 * Generic page to be shown
 * 
 * @author Angelo Nicolini
 *
 */
public interface Page {
	
	void setTitle(String...title);

	/**
	 * write the content of the page
	 * @param content
	 */
	void writeContent(List<Content> content);
}
