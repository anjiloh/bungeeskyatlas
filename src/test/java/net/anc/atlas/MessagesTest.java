/**
 * 
 */
package net.anc.atlas;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import jparsec.util.Translate;
import jparsec.util.Translate.LANGUAGE;

/**
 * @author Angelo Nicolini
 *
 */
public class MessagesTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for {@link net.anc.atlas.Messages#getString(java.lang.String)}.
	 */
	@Test
	public void testGetString() {
		Translate.setDefaultLanguage(LANGUAGE.ENGLISH);
		String result = Messages.getString("Bungee.title");
		assertEquals("Bungee Sky Atlas for", result);
	}

//	/**
//	 * Test method for {@link net.anc.atlas.Messages#getObjectDescriptionTransation(java.lang.String)}.
//	 */
//	@Test
//	public void testGetObjectDescriptionTransation() {
//		fail("Not yet implemented"); // TODO
//	}

}
