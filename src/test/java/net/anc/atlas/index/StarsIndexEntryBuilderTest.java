package net.anc.atlas.index;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import jparsec.observer.LocationElement;
import jparsec.util.JPARSECException;
import net.anc.atlas.AstroItemDetail;
import net.anc.atlas.AstroItemDetail.AstroItemType;

public class StarsIndexEntryBuilderTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCompose() {
		// Sirius,lon: 101° 17' 13.760", lat: -16° 42' 58.017", rad: 2.6369917198459993,-1.44,2.6369917198459993,D;;10.0;;,<null>,-1,STAR,

		AstroItemDetail detail = new AstroItemDetail(
				"Sirius", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				null,
				AstroItemType.STAR,
				-1
				);
		StarsIndexEntryBuilder subject = new StarsIndexEntryBuilder();
		AstroItemDetail retValue = null;
		try {
			retValue = subject.compose(detail);
		} catch (JPARSECException e) {
			fail("Unexpected error:" + e.getMessage() );
		}
		assertNotNull(retValue);
		System.out.println(retValue);
		detail = new AstroItemDetail(
				"Cursa", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				null,
				AstroItemType.STAR,
				-1
				);
		subject = new StarsIndexEntryBuilder();
		retValue = null;
		try {
			retValue = subject.compose(detail);
		} catch (JPARSECException e) {
			fail("Unexpected error:" + e.getMessage() );
		}
		assertNotNull(retValue);
		System.out.println(retValue);
	}

	

	@Test
	public void testComposeErakis() {

		AstroItemDetail detail = new AstroItemDetail(
				"Erakis", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				null,
				AstroItemType.STAR,
				-1
				);
		StarsIndexEntryBuilder subject = new StarsIndexEntryBuilder();
		AstroItemDetail retValue = null;
		try {
			retValue = subject.compose(detail);
		} catch (JPARSECException e) {
			fail("Unexpected error:" + e.getMessage() );
		}
		assertNotNull(retValue);
		assertEquals("21430096 (Mu Cep)", retValue.getOtherNames());


	}
	
	@Test
	public void testComposePollux() {

		AstroItemDetail detail = new AstroItemDetail(
				"Pollux", 
				new LocationElement(12, 23, 58.017),
				12.0f,
				"1.0",
				"type",
				null,
				AstroItemType.STAR,
				-1
				);
		StarsIndexEntryBuilder subject = new StarsIndexEntryBuilder();
		AstroItemDetail retValue = null;
		try {
			retValue = subject.compose(detail);
		} catch (JPARSECException e) {
			fail("Unexpected error:" + e.getMessage() );
		}
		assertNotNull(retValue);
		assertEquals("Mu Cep", retValue.getOtherNames());


	}
}
