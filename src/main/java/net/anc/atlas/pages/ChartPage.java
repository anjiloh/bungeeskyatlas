/**
 * 
 */
package net.anc.atlas.pages;

import java.util.List;
import java.util.logging.Logger;

import com.google.inject.Inject;

import net.anc.atlas.A3;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.Page;
import net.anc.atlas.SkyMap;
import net.anc.atlas.serializer.GeneratorFormat;

/**
 * @author Angelo Nicolini

 *
 */
public class ChartPage implements Page  {

	@Inject
	protected AtlasGenerator generator;
	private static final Logger LOGGER = Logger.getLogger( Page.class.getName() );

	
	private int chartId;

	protected final String witdh;
	protected final String height;
	
	/**
	 * 
	 * @param chartId
	 * @param title
	 */

	public ChartPage() {
		if(AtlasProperties.getProperties().getPaperSize() instanceof A3) {
			witdh = "98%"; 
			height = "95.727%"; 
		}
		else {
			witdh = "98%";
			height = "94.6%";
			
		}
	}
	
	
	public void setChartId(int chartId) {
		this.chartId = chartId;
	}
	
	public int getChartId() {
		return chartId;
	}

	
	@Override
	public void setTitle(String... args) {
		String title = args[0];
		@SuppressWarnings("unused")
		String constellation = args[1];


		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BOOK ) {
			generator.writeRawText("\\newgeometry{left=0.4cm, right=0.0cm, top=0.1cm, bottom=0.2cm}");
			generator.beginSubSection(title + "\\label{subsec:" + (chartId + 1 ) + "}");
		}
		else {
			generator.beginSubSection(title);
		}
		
	}
	
	
	/**
	 * @param contains a SKyMap object (the navigator)
	 * 	 */
	@Override
	public void writeContent(List<Content> content) {

		
		generator.		
			writeImageWithCaption(
				witdh, 	height, 
				null, 
				AtlasProperties.getProperties().getPDFAtlasFileName().replaceAll("\\\\", "/"),
				null, 
				null
			);

		
		SkyMap navigator = (SkyMap) content.get(0);
		// +1 because chartId starts from 0 TODO change chartId settings

		LOGGER.finer("navigator (" + navigator.getChartId() + "): " + navigator.toString());
		
		generator.writeNavigator(navigator.getChartId() + 1, navigator.north.getChartId() + 1, navigator.east.getChartId()+ 1, navigator.south.getChartId() + 1, navigator.west.getChartId() + 1);

		
	}




}
