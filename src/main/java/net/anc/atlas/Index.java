/**
 * 
 */
package net.anc.atlas;

/**
 * Index page
 * 
 * @author Angelo Nicolini
 *
 */
public interface Index extends Page {

	/**
	 * Idenfitier of that index. Used to compose title (by getting it from messages.properties)
	 * @return
	 */
	String getId();
	
	String[] getColumnsName();
}
