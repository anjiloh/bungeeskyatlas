package net.anc.atlas.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.inject.Inject;

import jparsec.util.Translate;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.ConstellationsByChart;
import net.anc.atlas.Content;
import net.anc.atlas.Index;
import net.anc.atlas.Table;
import net.anc.atlas.serializer.GeneratorFormat;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public class ConstellationsByChartIndex extends BaseIndex implements Index, Table {

	@Inject
	private AtlasGenerator generator;
	
	
	private static String[] columnNames = {
		Translate.translate("Constellation"), Translate.translate("Chart")
	};
	

	@Override
	public void writeContent(List<Content> content) {
		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BEAMER ) {
			// TODO: to be implemented
		}
		else {
			
			int size = (int) (content.size() / 6.0) ;
			
			if( size *6 < content.size() ){
				size++;
			}
			
			int cursor = 0;
			
			List<Content> column1 = new ArrayList<Content>();
			for( int i = cursor ; i < size; ++i ){
				column1.add(content.get(i));
			}			
			cursor += size;
			List<Content> column2 = new ArrayList<Content>();
			for( int i = 0 ; i < size; ++i ){
				column2.add(content.get(i + cursor));
			}			
			cursor += size;
			List<Content> column3 = new ArrayList<Content>();
			for( int i = 0 ; i < size; ++i ){
				column3.add(content.get(i + cursor));
			}			
			cursor += size;
			List<Content> column4 = new ArrayList<Content>();
			for( int i = 0 ; i < size; ++i ){
				column4.add(content.get(i + cursor));
			}			
			cursor += size;
			List<Content> column5 = new ArrayList<Content>();
			for( int i = 0 ; i < size; ++i ){
				column5.add(content.get(i + cursor));
			}			
			cursor += size;
			List<Content> column6 = new ArrayList<Content>();
			for( int i = cursor ; i < content.size(); ++i ){
				column6.add(content.get(i));
			}			

			for( int i = 0 ; i < size; ++i ){
				ConstellationsByChart nextFromColumn1 = (ConstellationsByChart)column1.get(i);
				ConstellationsByChart nextFromColumn2 = (ConstellationsByChart)column2.get(i);
				ConstellationsByChart nextFromColumn3 = (ConstellationsByChart)column3.get(i);
				ConstellationsByChart nextFromColumn4 = (ConstellationsByChart)column4.get(i);
				ConstellationsByChart nextFromColumn5 = (ConstellationsByChart)column5.get(i);
				ConstellationsByChart nextFromColumn6 ;
				if( i < column6.size()) {
					nextFromColumn6 = (ConstellationsByChart)column6.get(i);
				}
				else {
					nextFromColumn6 = new ConstellationsByChart(null, -1);
				}
				String[] datai = { 
						nextFromColumn1.getConstellation(), 
						getFormattedChartIdRef(nextFromColumn1.getChartId()), 
						nextFromColumn2.getConstellation(), 
						getFormattedChartIdRef(nextFromColumn2.getChartId()), 
						nextFromColumn3.getConstellation(), 
						getFormattedChartIdRef(nextFromColumn3.getChartId()),
						nextFromColumn4.getConstellation(), 
						getFormattedChartIdRef(nextFromColumn4.getChartId()), 
						nextFromColumn5.getConstellation(), 
						getFormattedChartIdRef(nextFromColumn5.getChartId()), 
						nextFromColumn6.getConstellation() == null ? "" : nextFromColumn6.getConstellation(), 
						nextFromColumn6.getChartId() == -1 ? "" : getFormattedChartIdRef(nextFromColumn6.getChartId())  
					};
				generator.writeRowInTable(datai, null, null, null);
			}
			generator.endLongTable(null);
			

		}		
	}
	
	
	/**
	 * this returns the reference to the owning Chart
	 * @param chartId
	 * @return
	 */
	protected String getFormattedChartIdRef(int chartId) {
		return "\\ref{subsec:" + Integer.toString(chartId) + "}";
	}
	
	@Override
	public void setHeader(String caption, String[] header) {
		// caption unused

		if( AtlasProperties.getProperties().getLatexTemplate() == GeneratorFormat.BEAMER ) {
			generator.writeTableHeader(caption);
			generator.writeHorizontalLine();
		}
		else {
			
			assert (header.length == 2);  // TODO how to do if more than 2 columns

			generator.writeLongTableHeader(caption, "llllllllllll");
			// 6 columns
			generator.writeHeaderRowInTable(new String[] {
					header[0],header[1],
					header[0],header[1],
					header[0],header[1],
					header[0],header[1],
					header[0],header[1],
					header[0],header[1] 
					}, null, null, null);

			
		}
	}
	

	@Override
	public String[] getColumnsName() {
		return Arrays.copyOf(columnNames, columnNames.length);
	}

	@Override
	public String getId() {
		return "Index.caption";
	}
	
}
