/**
 * 
 */
package net.anc.atlas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This creates a sky map.
 * 
 * It is used by the Windrose compass in any page
 * 
 * @author Angelo Nicolini
 *
 */
public class SkyMapComposer {
	
	private List<Double> allDECs = new ArrayList<Double>();
	
	private static List<SkyMap> skyMap = null; 

	public static List<SkyMap> getSkyMap() {
		return skyMap;
	}
	
	
	
	public void create() {
		
		SkyMapComposer.skyMap = new ArrayList<SkyMap>(AtlasProperties.getProperties().getScale().getRa().length);
		for (int index = 0; index < AtlasProperties.getProperties().getScale().getRa().length; index++) {
			SkyMapComposer.skyMap.add(
					new SkyMap(
							AtlasProperties.getProperties().getScale().getRa(index), 
							AtlasProperties.getProperties().getScale().getDec(index), 
							index
						)
					);
		}		
		
		
		for (SkyMap map : SkyMapComposer.skyMap) {
			if( !allDECs.contains(map.dec)) {
				allDECs.add(map.dec);
			}
		}
		
//		allDECs.sort(Comparator.naturalOrder());

		for (SkyMap map : skyMap) {
			setNorthSouth(skyMap, map);
			setEastWest(skyMap, map);
		}
		
		
	}

	
	/**
	 * This identifies the north and south neighbors of a sector. The main algorithm is
	 * <ol>
	 * 	<li>find the north/south sector with the same RA
	 *  <li>find the north/south sector closed to the RA to the RA of the central sector
	 * </ol> 
	 * 
	 * @param skyMap
	 * @param sector
	 */
	private void setNorthSouth(List<SkyMap> skyMap, SkyMap sector) {

		int north = 0;
		int south = 0;

		
		// RA:  verticale
		// DEC: orizzontale
		
		int northRAPos = 0;
		int southRAPos = 0;
		for( int i = 0; i < allDECs.size(); ++i ){
			if( sector.dec == allDECs.get(i)) {
				northRAPos = i - 1;
				southRAPos = i + 1;
				break;
			}
 		}
		// north pole
		if( sector.ra == 0 && sector.dec == 90 ){
			final double firstNonPolarDec = skyMap.stream().filter(s -> s.dec < 90).findFirst().get().dec;
			
			// north pole
			List<SkyMap> availableSectors = skyMap.stream()
					.filter(map -> map.dec == firstNonPolarDec)  
					.sorted((first, second) -> {
						if( first.ra == second.ra ) {
							return 0;
						}
						else {
							return first.ra < second.ra ? 1 : -1;
						}
					})
					.collect(Collectors.toList());
		
			double perfectNorth = 12;
			double perfectSouth = 0;
			
			double minNorth = 100;
			double minSouth = 100;
			
			for( int i = 0; i < availableSectors.size(); ++i ){
				
				double maybeNorth = Math.abs( availableSectors.get(i).ra - perfectNorth );
				if( maybeNorth < minNorth ) {
					minNorth = maybeNorth;
					north = i;
				}
				double maybeSouth = Math.abs( availableSectors.get(i).ra - perfectSouth );
				if( maybeSouth < minSouth ) {
					minSouth = maybeSouth;
					south = i;
				}
			}
			sector.north = availableSectors.get(north);
			sector.south = availableSectors.get(south);
		}
		else if( sector.ra == 0 && sector.dec == -90 ){
			// south pole
			
			final double lastNonPolarDec = skyMap.stream().sorted(Collections.reverseOrder( new Comparator<SkyMap>() {

				@Override
				public int compare(SkyMap o1, SkyMap o2) {
					return o1.dec > o2.dec ? -1  : (o1.dec < o2.dec ? 1 : 0  )  ;
				}
			} )).filter(s -> s.dec > -90).findFirst().get().dec;
			// south pole
			List<SkyMap> availableSectors = skyMap.stream()
					.filter(map -> map.dec == lastNonPolarDec)  
					.sorted((first, second) -> {
						if( first.ra == second.ra ) {
							return 0;
						}
						else {
							return first.ra < second.ra ? 1 : -1;
						}
					})
					.collect(Collectors.toList());
			final double perfectNorth = 0;
			final double perfectSouth = 12;
			
			double minNorth = 100;
			double minSouth = 100;
			
			for( int i = 0; i < availableSectors.size(); ++i ){
				
				double maybeNorth = Math.abs( availableSectors.get(i).ra - perfectNorth );
				if( maybeNorth < minNorth ) {
					minNorth = maybeNorth;
					north = i;
				}
				double maybeSouth = Math.abs( availableSectors.get(i).ra - perfectSouth );
				if( maybeSouth < minSouth ) {
					minSouth = maybeSouth;
					south = i;
				}
			}
			sector.north = availableSectors.get(north);
			sector.south = availableSectors.get(south);
			
		}
		else {
		
			
			// north
			double northDEC = allDECs.get(northRAPos);
			
			// is there a sector with the same RA?
			Optional<SkyMap> northSector = skyMap.stream().filter(chart -> chart.dec == northDEC && chart.ra == sector.ra ).findFirst();
			
			if( northSector.isPresent() ){
				sector.north = northSector.get();
	
	//			north = northSector.get().getChartId();
			}
			else {
				// there is NOT a sector with the same RA
				// then, find the sector with the closest RA to the current one
				List<SkyMap> sectorsAtNorth =  skyMap.stream().filter(chart -> chart.dec == northDEC).collect(Collectors.toList());
				double minNorth = 100;
				for( int i = 0; i < sectorsAtNorth.size(); ++i ){
					double maybeNorth = Math.abs( sectorsAtNorth.get(i).ra - sector.ra );
					if( maybeNorth < minNorth ) {
						minNorth = maybeNorth;
						north = i;
					}
				}
				sector.north = sectorsAtNorth.get(north);
				
			}
			
	
			// south
			double southDEC = allDECs.get(southRAPos);
			
			// is there a sector with the same RA?
			Optional<SkyMap> southSector = skyMap.stream().filter(chart -> chart.dec == southDEC && chart.ra == sector.ra ).findFirst();
			
			if( southSector.isPresent() ){
				sector.south = southSector.get();
	//			south = southSector.get().getChartId();
			}
			else {
				// there is NOT a sector with the same RA
				// then, find the sector with the closest RA to the current one
				List<SkyMap> sectorsAtSouth =  skyMap.stream().filter(chart -> chart.dec == southDEC).collect(Collectors.toList());
				double minSouth = 100;
				for( int i = 0; i < sectorsAtSouth.size(); ++i ){
					double maybeSouth = Math.abs( sectorsAtSouth.get(i).ra - sector.ra );
					if( maybeSouth < minSouth ) {
						minSouth = maybeSouth;
						south = i;
					}
				}
				sector.south = sectorsAtSouth.get(south);			
			}
		}
		
		
		
	}

	/**
	 * This identifies the East and West neighbors of a sector 
	 * 
	 * @param skyMap
	 * @param sector
	 */
	private void setEastWest(List<SkyMap> skyMap, SkyMap sector) {

		List<SkyMap> availableSectors;
		int east = 0;
		int west = 0;
		
		if( sector.ra == 0 && sector.dec == 90 ){
			final double firstNonPolarDec = skyMap.stream().filter(s -> s.dec < 90).findFirst().get().dec;
			
			// north pole
			availableSectors = skyMap.stream()
					.filter(map -> map.dec == firstNonPolarDec)  
					.sorted((first, second) -> {
						if( first.ra == second.ra ) {
							return 0;
						}
						else {
							return first.ra < second.ra ? 1 : -1;
						}
					})
					.collect(Collectors.toList());
		
			double perfectEast = 6;
			double perfectWest = 18;
			
			double minEast = 100;
			double minWest = 100;
			
			for( int i = 0; i < availableSectors.size(); ++i ){
				
				double maybeEast = Math.abs( availableSectors.get(i).ra - perfectEast );
				if( maybeEast < minEast ) {
					minEast = maybeEast;
					east = i;
				}
				double maybeWest = Math.abs( availableSectors.get(i).ra - perfectWest );
				if( maybeWest < minWest ) {
					minWest = maybeWest;
					west = i;
				}
			}
		}
		else if( sector.ra == 0 && sector.dec == -90 ){
			
			final double lastNonPolarDec = skyMap.stream().sorted(Collections.reverseOrder( new Comparator<SkyMap>() {

				@Override
				public int compare(SkyMap o1, SkyMap o2) {
					return o1.dec > o2.dec ? -1  : (o1.dec < o2.dec ? 1 : 0  )  ;
				}
			} )).filter(s -> s.dec > -90).findFirst().get().dec;
			// south pole
			availableSectors = skyMap.stream()
					.filter(map -> map.dec == lastNonPolarDec)  
					.sorted((first, second) -> {
						if( first.ra == second.ra ) {
							return 0;
						}
						else {
							return first.ra < second.ra ? 1 : -1;
						}
					})
					.collect(Collectors.toList());
			double perfectEast = 6;
			double perfectWest = 18;
			
			double minEast = 100;
			double minWest = 100;
			
			for( int i = 0; i < availableSectors.size(); ++i ){
				
				double maybeEast = Math.abs( availableSectors.get(i).ra - perfectEast );
				if( maybeEast < minEast ) {
					minEast = maybeEast;
					east = i;
				}
				double maybeWest = Math.abs( availableSectors.get(i).ra - perfectWest );
				if( maybeWest < minWest ) {
					minWest = maybeWest;
					west = i;
				}
			}
			
		}
		else {
			
			availableSectors = skyMap.stream()
					.filter(map -> map.dec == sector.dec)
					.sorted((first, second) -> {
						if( first.ra == second.ra ) {
							return 0;
						}
						else {
							return first.ra < second.ra ? 1 : -1;
						}
					})
					.collect(Collectors.toList());
			
			int location = availableSectors.indexOf(sector);
			east = 0;
			west = 0;
			if( location == 0 ) {
				west = availableSectors.size() - 1;
				east = 1;
			}
			else if( location == availableSectors.size() - 1 ) {
				// todo No, this is false...
				west = location - 1;
				east = 0;
			}
			else {
				west = location - 1;
				east = location + 1;
			}
			
		}		
		// TODO: quick Fix: INVERT east and west: the windrose is broken!!!
		sector.east = availableSectors.get(west);
		sector.west = availableSectors.get(east);
	}


	/**
	 * find a SkyMap by its ID
	 * @param chartId
	 */
	public static Content getSkyMap(int chartId) {
		return skyMap.stream().filter( map -> map.getChartId() == chartId ).findFirst().get();
	}


}
