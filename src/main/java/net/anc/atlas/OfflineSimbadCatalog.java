/**
 * 
 */
package net.anc.atlas;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jparsec.util.JPARSECException;
import jparsec.vo.SimbadElement;
import jparsec.vo.SimbadQuery;

/**
 * Offline copy of the used entries downloaded from Simbad
 * 
 * @author Angelo Nicolini
 *
 */
public class OfflineSimbadCatalog {

	private static final long aWeek = 7 * 24 * 60 * 60 *1000;
	
	static class SimbadEntry implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4477173050232846730L;
		SimbadElement subject;
		long timestamp;
	}
	
	
	
	private volatile static String simbadOfflineCatalog = AtlasProperties.getProperties().getOutputPath() 	+ "/simbadOfflineCatalog.bin";

	private static OfflineSimbadCatalog offlineCatalog = null;
	private Map<String, SimbadEntry> catalog; 

	@SuppressWarnings("unchecked")
	private OfflineSimbadCatalog() {

		File file = new File(simbadOfflineCatalog);
		try {
			if (file.createNewFile()) {
				catalog = new HashMap<String, OfflineSimbadCatalog.SimbadEntry>();
				return;
			}
			;
		} catch (IOException e) {
			throw new RuntimeException("cannot write offline catalog", e);
		}

		ObjectInputStream objectInputStream = null;
		try (InputStream 	fileInputStream = Files.newInputStream(Paths.get(simbadOfflineCatalog)) ) {
			objectInputStream = new ObjectInputStream(fileInputStream);

			catalog = (Map<String, SimbadEntry>) objectInputStream.readObject();

		} 
		catch(EOFException e) {
			catalog = new HashMap<String, OfflineSimbadCatalog.SimbadEntry>();
		}
		catch (IOException e) {
			throw new RuntimeException("cannot write offline catalog", e);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("cannot write offline catalog", e);
		} finally {

			if (objectInputStream != null) {
				try {
					objectInputStream.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}
	}

	/**
	 * look for an object of name 'objName' in Simbad, and if needed, update its offline catalog
	 * The offline catalog is update if the data is older than one week
	 * @param objName
	 * @return
	 */
	public static SimbadElement lookup(String objName) {

		if (offlineCatalog == null) {
			offlineCatalog = new OfflineSimbadCatalog();
		}

		SimbadElement element = null;


		SimbadEntry entry = offlineCatalog.catalog.get(objName);
		if( entry == null ) {
			element = queryWithRetry(objName);
		}
		else {
			if(  System.currentTimeMillis() - entry.timestamp > aWeek ) {
				element = queryWithRetry(objName);
			}
			else {
				element = entry.subject;
			}
		}
		

		return  element;

	}

	/**
	 * force the update of the catalog
	 * @param simbad
	 */
	public static void update(SimbadElement simbad) {

		if (offlineCatalog == null) {
			offlineCatalog = new OfflineSimbadCatalog();
		}
		
		if( simbad == null ) {
			return;
		}

		SimbadEntry entry = offlineCatalog.catalog.get(simbad.name);
		if( entry == null ){
			updateCatalog();
		}
		else {
			if(  System.currentTimeMillis() - entry.timestamp > aWeek ) {
				entry.subject = simbad;
				entry.timestamp =  System.currentTimeMillis();
				offlineCatalog.catalog.put(simbad.name, entry);
				updateCatalog();
			}
		}
	
	}

	private static void updateCatalog() {

		OutputStream fout = null;
		int counter = 0;
		do {
			try {
				fout = Files.newOutputStream(Paths.get(simbadOfflineCatalog), StandardOpenOption.CREATE);
				break;
			} catch (IOException e) {
			}
			try {
				TimeUnit.SECONDS.sleep(1);
				counter++;
			} catch (InterruptedException e) {
			}
		} while (counter < 3);

		
		if( fout == null ) {
			throw new RuntimeException("cannot open offline catalog");
		}
		
		try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
			oos.flush();
			oos.writeObject(offlineCatalog.catalog);
			oos.close();
			// fout.close();
		} catch (IOException ex) {
			throw new RuntimeException("cannot write offline catalog", ex);
		}
		finally {
			try {
				fout.close();
			} catch (IOException e) {
				// ingore
			}
		}
	}
	
	
	private static SimbadElement queryWithRetry(String name) {
		int counter = 0;
		SimbadElement element = null;
		do {
			try {
				element = SimbadQuery.query(name);
				break;
			} catch (JPARSECException e) {
					counter ++;
					try {
						TimeUnit.SECONDS.sleep(3);
					} catch (InterruptedException e1) {
						// ignore
					}
			}
		}while( counter < 3);
		
		if( element != null ) {
			SimbadEntry entry = new SimbadEntry();
			entry.timestamp = System.currentTimeMillis();
			entry.subject = element;
			offlineCatalog.catalog.put(element.name, entry);
			updateCatalog();	
		}
		
		return element;

	}
	
}
