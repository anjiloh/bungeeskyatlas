package net.anc.messier;

import jparsec.astronomy.OcularElement;
import jparsec.astronomy.TelescopeElement;
import jparsec.astronomy.CoordinateSystem.COORDINATE_SYSTEM;
import jparsec.graph.JPARSECStroke;
import jparsec.graph.chartRendering.Graphics;
import jparsec.graph.chartRendering.SkyRenderElement.FAST_LINES;
import jparsec.graph.chartRendering.SkyRenderElement.LEYEND_POSITION;
import jparsec.graph.chartRendering.SkyRenderElement.MILKY_WAY_TEXTURE;
import jparsec.graph.chartRendering.SkyRenderElement.REALISTIC_STARS;
import jparsec.math.Constant;
import jparsec.util.JPARSECException;
import net.anc.atlas.Sky;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public class DetailedMap extends Sky {

	public DetailedMap() throws JPARSECException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void init() throws JPARSECException {
		super.init();
//		setTelescope(TelescopeElement.REFRACTOR_10cm_f5);
		setTelescope(TelescopeElement.OBJECTIVE_300mm_f2_8);
		
		setTelescope(
				new TelescopeElement(
						"Objective 250mm f/2.8", 250, 89, 0, 0, 0,
//						"Objective 200mm f/2.8", 200, 71, 0, 0, 0,
				new OcularElement("Human eye", 38, 65 * Constant.DEG_TO_RAD, 43))
						);
		
//		setTelescope(new TelescopeElement("newton", 1000, 200, 0, 0, 1.5f, new OcularElement("Binoculars", 371f, 65 * Constant.DEG_TO_RAD, 7)));
	}
	
	@Override
	protected COORDINATE_SYSTEM getCoordinateSystem() {
		return COORDINATE_SYSTEM.EQUATORIAL;
	}
	
	@Override
	protected void initSkyProperties() throws JPARSECException {
		
		super.initSkyProperties();
		getSkyRender().width = 800;
		getSkyRender().height = 640;
		
		getSkyRender().drawLeyend = LEYEND_POSITION.TOP;
		getSkyRender().drawFastLinesMode = FAST_LINES.GRID_AND_MILKY_WAY_AND_CONSTELLATIONS;
		getSkyRender().overlayDSSimageInNextRendering = true;
		getSkyRender().drawStarsLimitingMagnitude = 8.5f;
		getSkyRender().drawDeepSkyObjects = true;
		getSkyRender().drawDeepSkyObjectsAllMessierAndCaldwell = true;
		getSkyRender().drawObjectsLimitingMagnitude = 15.5f;
		getSkyRender().coordinateSystem = getCoordinateSystem();
		// as per Tomás's tip: keep it false!
		getSkyRender().drawClever = false;

		getSkyRender().drawStarsLabelsLimitingMagnitude = getSkyRender().drawStarsLimitingMagnitude-2;
		getSkyRender().drawSuperNovaAndNovaEvents = false;
		getSkyRender().drawSkyCorrectingLocalHorizon = true;
		getSkyRender().drawConstellationLimits = true;
		getSkyRender().drawStarsRealistic = REALISTIC_STARS.STARRED;

		getSkyRender().drawConstellationNamesFont = Graphics.FONT.SANS_SERIF_ITALIC_18;
		getSkyRender().drawStarsNamesFont = Graphics.FONT.SANS_SERIF_PLAIN_11;
		getSkyRender().drawConstellationStroke = JPARSECStroke.STROKE_DEFAULT_LINE_THIN;
	
		getSkyRender().drawMilkyWayContoursWithTextures = MILKY_WAY_TEXTURE.OPTICAL;
	}
	
	
}
