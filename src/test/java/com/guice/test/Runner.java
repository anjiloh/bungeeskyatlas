package com.guice.test;

import com.google.inject.Guice;
import com.google.inject.Injector;


public class Runner {

	public static void main(String[] args) {
		
		Injector injector = Guice.createInjector(new CarModule());

		CarManagerWithMapBinder test = injector.getInstance(CarManagerWithMapBinder.class);
		
		test.createCarByType("ferrari");
		
		test.createCarByType("mercedes");
		test.createCarByType("toyota");
	}

}
