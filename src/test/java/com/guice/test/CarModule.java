package com.guice.test;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.TypeLiteral;

public class CarModule extends AbstractModule {

	@Override
	protected void configure() {
		
		install(new FactoryModuleBuilder().build(FerrariFactory.class));
		install(new FactoryModuleBuilder().build(MercedesFactory.class));
		install(new FactoryModuleBuilder().build(ToyotaFactory.class));
		
		MapBinder<String, CarFactory<?>> mapbinder = MapBinder.newMapBinder(binder(), new TypeLiteral<String>(){}, new TypeLiteral<CarFactory<?>>(){});

		 mapbinder.addBinding("ferrari").to(FerrariFactory.class);  
         mapbinder.addBinding("mercedes").to(MercedesFactory.class);
         mapbinder.addBinding("toyota").to(ToyotaFactory.class);
	}


}
