package net.anc.atlas.pages;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import com.google.inject.Inject;

import jparsec.io.FileIO;
import jparsec.util.JPARSECException;
import jparsec.util.Translate;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.AtlasProperties;
import net.anc.atlas.Content;
import net.anc.atlas.Index;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public class LegendPage implements Page {

	@Inject
	protected AtlasGenerator generator;

	@Inject
	private PageFactory<Page> pageFactory;

	@Override
	public void setTitle(String... title) {
		if( title.length == 1 ){
			generator.beginSection(Messages.getString(title[0]));
		}
	}
	

	@Override
	public void writeContent(List<Content> content) {
		
		generator.beginSubSection(Messages.getString("Legend.windrose.title"));
		generator.writeParagraph(Messages.getString("Legend.windrose.content.1"));
		
		try {
			String windroseImgName = "/windrose" 
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			generator.writeImageWithCaption("30%", null,null, Paths.get(this.getClass().getResource(windroseImgName).toURI()).toFile().toString().replaceAll("\\\\", "/"), Messages.getString("Legend.windrose.img.caption"), "fig:windrose");
		} catch (URISyntaxException e) {
			throw new RuntimeException("cannot write navigator", e);
		}
		
		generator.writeRawText(Messages.getString("Legend.windrose.content.2")  + " ~\\ref{fig:windrose}:" + FileIO.getLineSeparator());
		
		String[]  descr = {
				Messages.getString("Legend.windrose.descr.C"),
				Messages.getString("Legend.windrose.descr.N"),
				Messages.getString("Legend.windrose.descr.E"),
				Messages.getString("Legend.windrose.descr.S"),
				Messages.getString("Legend.windrose.descr.W"),
		};		
		try {
			generator.writeList(descr);
		} catch (JPARSECException e) {
			throw new RuntimeException("cannot get legend", e);
		}
		
		generator.beginSubSection(Messages.getString("Legend.charts.title"));

		if( AtlasProperties.getProperties().isShowLegend() ) {
			generator.writeParagraph(Messages.getString("Legend.charts.content.legend.in.chart"));
		}
		else {
			generator.writeParagraph(Messages.getString("Legend.charts.content.legend.not.in.chart"));
		}
		
		try {
			String legendImgName = 
					"/legend"  
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			generator.writeImageWithCaption("100%", null,null, Paths.get(this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/"), Messages.getString("Legend.charts.img.caption"), "fig:legend");
	
			generator.writeRawText(Messages.getString("Legend.charts.content.1") + FileIO.getLineSeparator());
	
			StringBuilder text = new StringBuilder();
	
	
			text.append("\\begin{itemize}"+ FileIO.getLineSeparator());
			text.append("\\item "+ FileIO.getLineSeparator());
			text.append('{').append(Messages.getString("Legend.charts.stars.mag")).append(" ~\\ref{fig:legend_stars}}"+ FileIO.getLineSeparator());
	
			text.append("\\begin{figure}[!ht]"+ FileIO.getLineSeparator());
			legendImgName = "/legend_stars" 
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			text.append("\\includegraphics[width=1.0000\\textwidth]{").append(
					Paths.get( this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/") )
				.append('}').append( FileIO.getLineSeparator() );
			text.append("\\caption{").append(Messages.getString("Legend.charts.stars.mag.caption")).append( "}\\label{fig:legend_stars}"+ FileIO.getLineSeparator());
			text.append("\\end{figure}"+ FileIO.getLineSeparator()); 
			
			text.append("\\item "+ FileIO.getLineSeparator());
			text.append('{').append(  Messages.getString("Legend.charts.stars.types.1")).append("~\\ref{fig:legend_types}");
			text.append(Messages.getString("Legend.charts.stars.types.2")).append('}').append( FileIO.getLineSeparator())  ;
			text.append("\\begin{figure}[!ht]"+ FileIO.getLineSeparator());

			legendImgName = "/legend_types" 
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  					
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			text.append("\\includegraphics[width=0.1500\\textwidth]{").append( 
					Paths.get( this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/") )
				.append('}').append( FileIO.getLineSeparator());
			text.append("\\caption{" +  Messages.getString("Legend.charts.stars.types.caption") +"}\\label{fig:legend_types}"+ FileIO.getLineSeparator());
			text.append("\\end{figure}"+ FileIO.getLineSeparator());
	
			text.append("\\item "+ FileIO.getLineSeparator());
			text.append('{').append( Messages.getString("Legend.charts.dso") +"~\\ref{fig:legend_dso}}").append( FileIO.getLineSeparator());
			text.append("\\begin{figure}[!ht]"+ FileIO.getLineSeparator());
			
			legendImgName = "/legend_dso" 
					+ (Translate.getDefaultLanguage() == Translate.LANGUAGE.ITALIAN ? "_ita" : "")  
					+ (AtlasProperties.getProperties().isWhite() ? ".jpg" : "_dark.jpg");
			text.append("\\includegraphics[width=1.0000\\textwidth]{").append( 
					Paths.get(this.getClass().getResource(legendImgName).toURI()).toFile().toString().replaceAll("\\\\", "/") )
					.append('}').append( FileIO.getLineSeparator());
			text.append("\\caption{" +  Messages.getString("Legend.charts.dso.caption") +"}\\label{fig:legend_dso}"+ FileIO.getLineSeparator());
			text.append("\\end{figure}"+ FileIO.getLineSeparator());
	
			text.append("\\end{itemize}	"+ FileIO.getLineSeparator());

			generator.writeRawText(text.toString());

			
			
		} catch (URISyntaxException e) {
			throw new RuntimeException("cannot write legend", e);
		}

		generator.beginSection(Messages.getString("Legend.indexes.title"));
		generator.beginSubSection(Messages.getString("Legend.index.constellations"));
		generator.writeParagraph(Messages.getString("Legend.index.constellations.content"));

		
		generator.writeLongTableHeader(Messages.getString("Legend.index.constellations"), "ll");
		generator.writeHeaderRowInTable(new String[] {Messages.getString("Legend.table.column.name"), Messages.getString("Legend.table.column.description")}, null, null, null);
		generator.writeRowInTable( new String[] {new ConstellationsByChartIndex().getColumnsName()[0],  Messages.getString("Legend.constellations.column.1")}, null, null, null);
		generator.writeRowInTable( new String[] {new ConstellationsByChartIndex().getColumnsName()[1],  Messages.getString("Legend.constellations.column.2")}, null, null, null);
		generator.endLongTable(null);
		

	
		generator.beginSubSection(Messages.getString("Legend.index.dso"));
		generator.writeParagraph(Messages.getString("Legend.index.dso.content"));
		
		generator.writeLongTableHeader(Messages.getString("Legend.index.dso"), "ll");
		generator.writeHeaderRowInTable(new String[] {Messages.getString("Legend.table.column.name"), Messages.getString("Legend.table.column.description")}, null, null, null);
		
		ObjectsByChartIndex dsoIndex = new ObjectsByChartIndex();
		for(int i = 0; i < dsoIndex.getColumnsName().length; ++i  ){
			String msg = Messages.getString("Legend.dso.column." + (i+1));
			generator.writeRowInTable( new String[] {dsoIndex.getColumnsName()[i], msg}, null,  null, null);
			
		}
		
		generator.endLongTable(null);

		generator.beginSubSection(Messages.getString("Legend.index.stars"));
		generator.writeParagraph(Messages.getString("Legend.index.stars.content"));

		
		
		generator.writeLongTableHeader(Messages.getString("Legend.index.stars"), "ll");
		generator.writeHeaderRowInTable(new String[] {Messages.getString("Legend.table.column.name"), Messages.getString("Legend.table.column.description")}, null, null, null);
		
		Index starsIndex = (Index) pageFactory.create("starsByChart");
		for(int i = 0; i < starsIndex.getColumnsName().length; ++i  ){
			String msg = Messages.getString("Legend.stars.column." + (i+1));
			generator.writeRowInTable( new String[] {starsIndex.getColumnsName()[i], msg}, null,  null, null);
			
		}
		
		generator.endLongTable(null);
		

		generator.beginSubSection(Messages.getString("Legend.index.novae"));
		generator.writeParagraph(Messages.getString("Legend.index.novae.content"));

		
		
		generator.writeLongTableHeader(Messages.getString("Legend.index.novae"), "ll");
		generator.writeHeaderRowInTable(new String[] {Messages.getString("Legend.table.column.name"), Messages.getString("Legend.table.column.description")}, null, null, null);
		
		Index novaeIndex = (Index) pageFactory.create("novaeByChart");
		
		for(int i = 0; i < novaeIndex.getColumnsName().length; ++i  ){
			String msg = Messages.getString("Legend.novae.column." + (i+1));
			generator.writeRowInTable( new String[] {novaeIndex.getColumnsName()[i], msg}, null,  null, null);
			
		}
		
		generator.endLongTable(null);
		

	}





}
