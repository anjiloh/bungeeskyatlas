package net.anc.atlas.pages;

import java.util.List;

import com.google.inject.Inject;

import jparsec.io.FileIO;
import net.anc.atlas.AtlasGenerator;
import net.anc.atlas.Content;
import net.anc.atlas.Messages;
import net.anc.atlas.Page;
import net.anc.atlas.Table;

/**
 * 
 * @author Angelo Nicolini
 *
 */
public class GenerationOptionsPage implements Page, Table {

	@Inject
	private AtlasGenerator generator;

	@Override
	public void setTitle(String... title) {
		if (title.length == 1) {
			generator.beginSection(Messages.getString(title[0]));
		}
		generator.beginSubSection(Messages.getString("generation.options.title"));
	}

	@Override
	public void setHeader(String caption, String[] head) {

		// ((Table)legend).setHeader(Messages.getString("Legend.generation.data"),
		// new String[] {Messages.getString("Legend.generation.head.1"),
		// Messages.getString("Legend.generation.head.2")});

		generator.writeLongTableHeader(caption, String.format("%0" + head.length + "d", 0).replace("0", "l"));
		generator.writeRawText("\\\\" + FileIO.getLineSeparator());
		StringBuilder columns = new StringBuilder();
		for (String value : head) {
			columns.append(value).append(" & ");
		}
		columns.delete(columns.length() - 3, columns.length()); // remove last &
		columns.append("\\\\").append(FileIO.getLineSeparator());
		generator.writeRawText(columns.toString());

		generator.writeRawText("\\hline\\hline\\noalign{\\smallskip}");
		generator.writeRawText(FileIO.getLineSeparator());
		generator.writeRawText("\\endfirsthead");
		generator.writeRawText(FileIO.getLineSeparator());

		generator.writeRawText("\\caption{Continue ");
		generator.writeRawText(caption);
		generator.writeRawText("}");
		generator.writeRawText("\\\\" + FileIO.getLineSeparator());
		generator.writeRawText("\\hline\\hline\\noalign{\\smallskip}");
		generator.writeRawText(FileIO.getLineSeparator());
		generator.writeRawText(columns.toString());

		generator.writeRawText("\\hline\\hline\\noalign{\\smallskip}");
		generator.writeRawText(FileIO.getLineSeparator());
		generator.writeRawText("\\endhead");
		generator.writeRawText(FileIO.getLineSeparator());

	}

	@Override
	public void writeContent(List<Content> content) {

		content.stream().forEach(record -> {
			generator.writeRowInTable(
					new String[] { ((LegendDataContent) record).getName(), ((LegendDataContent) record).getValue() },
					null, null, null);
		});

		generator.endLongTable(null);

	}

}
